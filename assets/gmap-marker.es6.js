(({ Mapkit }) => {
  /**
   * Impelments the behavior the default Google maps marker.
   */
  class GMapMarker {
    constructor(index, latLng, label, config) {
      this.label = label || null;
      this.index = index;
      this.latLng = latLng;

      this.labelConfig = config.label;
      this.markerSymbol = config.symbol;

      // Keep track of the marker event listeners, keyed by event name.
      this.listeners = new Map();
    }

    /**
     * Initialize the marker when the map is initialized. This can only occur
     * after the Google Maps API is fully loaded.
     *
     * @param {google.maps.Map} map
     *   Google maps instance to attach this marker instance to.
     */
    init(map) {
      this._map = map;
      this._marker = new google.maps.Marker({
        map: map._gmap,
        position: this.position,
        zIndex: this.index,
        icon: this._getIcon(),
        label: this._getLabel(),
        _mapkitRef: this,
      });

      // If spiderfy is enabled for the map
      if (map.oms) {
        map.oms.trackMarker(this._marker);
      }

      // Register any click listeners which were already registered but
      // where waiting for the map to get initialized.
      this.listeners.forEach((items, event) => {
        if (event === 'click' && this._map.oms) {
          event = 'spider_click';
        }

        items.forEach((data) => {
          data.handle = this._marker.addListener(event, data.listener);
        });
      });
    }

    /**
     * Get the current latLng position of this marker.
     *
     * @return {LatLngLiteral}
     *   Returns and object with 'lat' and 'lng' properties for the latitude
     *   longitude of the represented location.
     */
    get position() {
      return this.latLng;
    }

    /**
     * Change the current marker position. This will also inform the map of the
     * relocation, so the map can react if needed to the change.
     *
     * @param {LatLngLiteral} latLng
     *   The latitude and longitude coordinate for the map marker.
     */
    set position(latLng) {
      const oldPos = this.latLng;
      this.latLng = latLng;

      if (this._marker) {
        this._marker.setPosition(latLng);
        this._map.markerPositionChanged(oldPos, latLng);
      }
    }

    /**
     * Get the marker HTML in a form that can be displayed outside of the map.
     * This is useful placing a marker display next to a result listing so users
     * can correllate the marker on the map to the result row.
     *
     * @return {string|null}
     *   Get the HTML snippet that can be embedded to represent the marker in
     *   a list view or other HTML display. Returns NULL if no label (all markers)
     *   look the same.
     */
    get innerHTML() {
      return this.label ? `<div class="mapkit-marker">${this.label}</div>` : null;
    }

    /**
     * Get the label configurations for rendering the label.
     *
     * @return {Object|null}
     *   Marker label color, text configurations and label text compatible for
     *   Google maps label configurations.
     */
    _getLabel() {
      return this.label ? {
        ...this.labelConfig,
        text: this.label,
      } : null;
    }

    /**
     * Get map icon display configurations compatible with Google maps marker
     * icon property.
     *
     * @return {Object|null}
     *   Map marker display settings which are compatible with Google maps
     *   marker icon configuration. This would be SVG path definition, image
     *   file path, or NULL.
     */
    _getIcon() { // eslint-disable-line class-methods-use-this
      return null;
    }

    /**
     * Bring the marker to the top of the display.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    toFront() {
      if (this._marker) {
        this._marker.setZIndex(10000);
      }
      return this;
    }

    /**
     * Change the Z depth of the marker.
     *
     * @param {int=} z
     *   The z-index value to set the marker to. If no value provided, then
     *   reset the index back to the initial value.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    setZIndex(z) {
      if (this._marker) {
        z = z || this.index;
        this._marker.setZIndex(z);
      }
      return this;
    }

    /**
     * Change the marker icon if changes are supported.
     *
     * @param {Object} changes
     *   Icon configuration changes to apply if supported.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    alterIcon(changes) { // eslint-disable-line no-unused-vars
      // Default map markers cannot be modified, so does nothing here, but
      // should be overridden by subclasses.
      return this;
    }

    /**
     * Resets the marker display back to the original settings.
     *
     * @return {this}
     *   Returns this marker instance for method chaining.
     */
    resetMarker() {
      if (this._marker) {
        this._marker.setIcon(this._getIcon());
      }
      return this;
    }

    /**
     * Attach this marker to be displayed on a map.
     *
     * @param {Mapkit.GMap} map
     *   Map to attach the marker to.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    attach(map) {
      this._map = map;

      if (this._marker && map._gmap) {
        this._marker.setMap(map._gmap);

        if (map.oms) {
          map.oms.trackMarker(this._marker);
        }
      }
      return this;
    }

    /**
     * Detach the marker from its current map display.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    detach() {
      if (this._marker) {
        if (this._map.oms) {
          this._map.oms.forgetMarker(this._marker);
        }

        // Remove all events and disassociate with any map instance.
        google.maps.event.clearInstanceListeners(this._marker);
        this._marker.setMap(null);
        delete this._map;
      }

      return this;
    }

    /**
     * Add click event listeners to the marker. If Google maps have not been
     * initialized yet, the marker object will add the listener when the marker
     * is initialized.
     *
     * @param {string} event
     *   Name of the event to add the new event listener to.
     * @param {function} listener
     *   Event listener function to add the the marker.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    on(event, listener) {
      const data = { listener, handle: null };

      if (this.listeners.has(event)) {
        this.listeners.get(event).push(data);
      }
      else {
        this.listeners.set(event, [data]);
      }

      if (this._marker) {
        // When OverlappingMarkerSpiderfier is enabled, the click event is
        // replaced with the "spider_click" event.
        if (this._map.oms && event === 'click') {
          event = 'spider_click';
        }

        data.handle = this._marker.addListener(event, listener);
      }

      return this;
    }

    /**
     * Remove click event listeners for this map marker.
     *
     * @param {string} event
     *   Name of the
     * @param {function=} listener
     *   If provided, only remove this event listener, otherwise, remove all
     *   click events for this marker.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    off(event, listener) {
      // Remove all click handlers if no specific listener passed in.
      if (!listener) {
        this.listeners.delete(event);

        if (this._marker) {
          // Check for OverlappingMarkerSpiderfier and adjust the event name.
          if (this._map.oms && event === 'click') {
            event = 'spider_click';
          }

          google.maps.event.clearListeners(this._marker, event);
        }
      }
      else {
        const listeners = this.listeners.get(event) || [];

        // Go hunting for the callback to remove.
        for (let i = 0; i < listeners.length; ++i) {
          const data = this.listeners[i];

          if (data.listener === listener || data.handle === listener) {
            if (listeners.length <= 1) {
              this.listeners.delete(event);
            }
            else {
              listeners.splice(i, 1);
            }

            if (this._marker && data.handle) {
              google.maps.event.removeListener(data.handle);
            }
            break;
          }
        }
      }
      return this;
    }
  }

  /**
   * Implements a Google maps marker from a SVG path.
   */
  class GMapSymbolMarker extends GMapMarker {
    constructor(index, latLng, label, config) {
      super(index, latLng, label, config);

      this.markerSymbol = config.symbol;
    }

    /**
     * @inheritdoc
     */
    _getLabel() {
      if (this.label && this.label.length) {
        return super._getLabel();
      }

      return {
        text: '\u2022',
        fontFamily: 'arial, san-serif',
        fontSize: '40px',
        color: '#fff',
      };
    }

    /**
     * @inheritdoc
     */
    _getIcon() {
      return this.markerSymbol;
    }

    /**
     * @inheritdoc
     */
    alterIcon(changes) {
      if (this._marker) {
        const icon = this._marker.getIcon();
        Object.assign(icon, changes);
        this._marker.setIcon(icon);
      }
      return this;
    }

    /**
     * @inheritdoc
     */
    get innerHTML() {
      const icon = this._getIcon();

      let html = `<div class="mapkit-marker" style="position: relative;">
        <svg viewBox="0 0 ${icon.size.width} ${icon.size.height}" style="fill:${icon.fillColor}; stroke:${icon.strokeColor};stroke-width:${icon.strokeWeight};">
          <path d="${icon.path}"/>
        </svg>`;

      // Include a label display if a label is provided.
      if (this.label) {
        const label = this._getLabel();
        const styles = 'position: absolute; transform: translate(-50%, -50%);'
          + `top: ${(100 * icon.labelOrigin.y) / icon.size.height}%; left: ${(100 * icon.labelOrigin.x) / icon.size.width}%;`
          + `color: ${label.color}; font-size: ${label.fontSize};`;

        html += `<span style="${styles}">${this.label}</span>`;
      }

      html += '</div>';
      return html;
    }
  }

  /**
   * Create letter labeling for markers.
   *
   * @param {int} i
   *   The marker index to generate the label for.
   *
   * @return {string}
   *   The letters correllating to the marker index.
   */
  function LetterMarkerLabels(i) {
    let str = '';

    while (i > 26) {
      const r = i % 26;
      i = (i - r) / 26;

      if (r) {
        str = String.fromCharCode(64 + r) + str;
      }
      else {
        str = `Z${str}`;
        i--; // eslint-disable-line no-plusplus
      }
    }

    return String.fromCharCode(64 + i) + str;
  }

  /**
   * Convert the numeric marker index into a string to display as the label.
   *
   * @param {int} i
   *   The marker index to transform into the marker label.
   *
   * @return {string}
   *   The string value of the marker index.
   */
  function NumberMarkerLabels(i) {
    return i.toString();
  }

  /**
   * Get the label generator function to use based on the desired label styles.
   *
   * @param {string|null} style
   *   The name of the label styling to fetch the generator function for.
   *
   * @return {function}
   *   Function which takes the marker index, and returns the string label.
   */
  function getLabelGenerator(style) {
    switch (style || 'numbers') {
      case 'letters':
        return LetterMarkerLabels;

      case 'numbers':
        return NumberMarkerLabels;

      default:
        return (() => null);
    }
  }

  /**
   * Generate Standard Google Maps marker factory method based on the settings.
   *
   * @param {Object} settings
   *   The marker setting the resulting marker factory will use when generating
   *   new markers instances.
   *
   * @return {function}
   *   A marker factory function, for creating map marker instances.
   */
  Mapkit.marker.gmapMarker = function createGmapMarker(settings) {
    const offset = settings.offset || 0;
    const labelGen = getLabelGenerator(settings.label.style);

    let count = settings.start || 1;
    let MarkerClass = GMapMarker;

    delete settings.label.style;
    const config = {
      label: {
        color: '#000',
        fontSize: '16px',
        ...settings.label,
      },
      symbol: null,
    };

    // Setup SVG path settings if Symbol configurations are present.
    if (settings.symbol) {
      MarkerClass = GMapSymbolMarker;

      // Ensure some of the marker sane defaults.
      config.symbol = {
        fillOpacity: 1,
        fillColor: '#ea0000',
        strokeColor: '#fff',
        strokeWeight: 1,
        ...settings.symbol,
      };
    }

    // Generator function for creating markers.
    return function createMarker(data) {
      const index = (data.index || count++) + offset; // eslint-disable-line no-plusplus

      return new MarkerClass(index, data.latLng, labelGen(index), config);
    };
  };
})(Drupal);
