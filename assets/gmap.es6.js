(({ Toolshed: ts, Mapkit, debounce }, loader) => {
  /**
   * Create a Mapkit wrapper for supporting Google Maps.
   */
  class GMap {
    constructor(el, settings, markerBuilders) {
      // Internal Google maps instance, should always start as NULL and
      // get assigned during the map initialization.
      this._gmap = null;

      this.el = el;
      this.markers = new Map();
      this.markerType = settings.defaultMarkerSet;
      this.markerBuilders = markerBuilders;

      // Transfer settings, while ensuring that we have sane defaults.
      this.center = settings.center;
      this.fitBounds = settings.fitBounds || false;
      this.spiderfyOptions = settings.markerSpiderfy;
      this.mapOptions = {
        clickableIcons: false,
        center: settings.center || { lat: 0, lng: 0 },
        zoom: settings.zoom || 11,
        minZoom: settings.minZoom || 4,
        maxZoom: settings.maxZoom || 17,
        mapTypeId: settings.defaultLayer || 'roadmap',
        mapTypeControlOptions: {
          mapTypeIds: settings.layers || null,
        },
        // Don't display "Points of Interest" as they can clutter a map.
        styles: [{
          featureType: 'poi',
          elementType: 'labels',
          stylers: [{ visibility: 'off' }],
        }],
      };

      // Bounds cannot be initialized until Google maps API has been loaded.
      this.bounds = null;

      // Create a delayed callback to refresh the map bounds. This small delay
      // allows the map API to catch-up and consolidate quick viewport changes.
      this.refreshBounds = debounce(() => {
        if (this._gmap) {
          if (this.markers.size > 1) {
            this._gmap.fitBounds(this.bounds);
          }
          else if (this.markers.size === 1) {
            this._gmap.setCenter(this.bounds.getCenter());
            this._gmap.setZoom(this.mapOptions.zoom);
          }
        }
      }, 100);

      // Keep track of the map event listeners, keyed by event name.
      this.listeners = new Map();
    }

    /**
     * Initialize the actual map when Google Maps API has been loaded.
     *
     * @param {google.maps.Map=} gmap
     *   A Google maps instance if reusing an existing map. Otherwise a new map
     *   is created.
     */
    init(gmap = null) {
      if (gmap) {
        gmap.setOptions(this.mapOptions);
        this._gmap = gmap;
      }
      else {
        const wrap = new ts.Element('div', { style: { height: '100%' } });
        this._gmap = new google.maps.Map(wrap.el, this.mapOptions);
      }

      // Attach the map instance to the current Mapkit.GMap wrapper.
      this.el.appendChild(this._gmap.getDiv());
      this.bounds = new google.maps.LatLngBounds();

      if (this.spiderfyOptions && window.OverlappingMarkerSpiderfier) {
        this.oms = new window.OverlappingMarkerSpiderfier(this._gmap, this.spiderfyOptions);
      }

      // If any markers were added before Google maps was ready then build the
      // markers now that the Google API has been loaded.
      if (this.markers.size) {
        this.markers.forEach((m) => {
          m.init(this);
          this.bounds.extend(m.position);
        });
      }

      if (this.fitBounds) {
        const boundsListener = google.maps.event.addListener(this._gmap, 'bounds_changed', () => {
          if (this.el.clientWidth > 0) {
            this.refreshBounds();
            google.maps.event.removeListener(boundsListener);
          }
        });

        // When using bounds fitting, it is possible for Google maps to go
        // beyond our zoom limits.
        if (this.mapOptions.minZoom || this.mapOptions.maxZoom) {
          google.maps.event.addListener(this._gmap, 'zoom_changed', () => {
            const zoom = this._gmap.getZoom();

            if (this.mapOptions.minZoom && zoom < this.mapOptions.minZoom) {
              this._gmap.setZoom(this.mapOptions.minZoom);
            }
            else if (this.mapOptions.maxZoom && zoom > this.mapOptions.maxZoom) {
              this._gmap.setZoom(this.mapOptions.maxZoom);
            }
          });
        }
      }

      // Register any click listeners which were already registered but
      // where waiting for the map to get initialized.
      this.listeners.forEach((items, event) => {
        // Init is a custom Mapkit event, and those listeners are run
        // immediately run since this is the map initialization.
        if (event === 'init') {
          items.forEach((data) => data.listener(this));
        }
        else if (event !== 'destroy') {
          items.forEach((data) => {
            data.handle = this._gmap.addListener(event, data.listener);
          });
        }
      });

      // Init has been handled, remove this set of event listeners.
      this.listeners.delete('init');
    }

    /**
     * Get the map center. If map has not been initialized yet, this value
     * might not be the correct values for where the built map will actually
     * be centered. One case is if the map is set to fit bounds but marker
     * bounds are not calculated yet.
     *
     * @return {Object}
     *   Returns a JSON object with "lat" and "lng" values for the current
     *   map set center position.
     */
    getCenter() {
      if (this._gmap) {
        const c = this._gmap.getCenter();

        return {
          lat: c.lat,
          lng: c.lng,
        };
      }

      // If map has not been initialized, that return the configured center
      // point. This will be different from a bounds center, or the current
      // map center position.
      return this.center;
    }

    /**
     * Recalculate the bounds of the marker.
     */
    calcBounds() {
      this.bounds = new google.maps.LatLngBounds();

      this.markers.forEach((m) => {
        this.bounds.extend(m.position);
      });
    }

    /**
     * A marker position has either been removed or moved.
     *
     * @param {LatLngLiteral} oldLatLng
     *   The original position of the marker.
     * @param {LatLngLiteral=} latLng
     *   If marker has been removed and does not have a new position this should
     *   be left empty.
     */
    markerPositionChanged(oldLatLng, latLng) {
      let refresh = false;

      if (latLng && !this.bounds.contains(latLng)) {
        this.bounds.extend(latLng);
        refresh = true;
      }

      // Bounds only change if the marker location coincides with one of
      // the edges of the bounds.
      const ne = this.bounds.getNorthEast();
      const sw = this.bounds.getSouthWest();

      if ((ne.lat() === oldLatLng.lat || sw.lat() === oldLatLng.lat)
        && (ne.lng() === oldLatLng.lng || sw.lng() === oldLatLng.lng)
      ) {
        this.calcBounds();
        refresh = true;
      }

      // The bounds were changed in some way, check if map needs to be refocused.
      if (refresh) {
        if (this.markers.size <= 1) {
          const c = this.markers.size ? this.bounds.getCenter() : this.center;
          this.setCenter(c);
        }
        else if (this.fitBounds) {
          this.refreshBounds();
        }
      }
    }

    /**
     * Open an info window for this map at the given marker location.
     *
     * @param {Mapkit.Marker} marker
     *   A Mapkit marker instance, which wraps a Google maps marker object.
     * @param {string|HTMLElement} content
     *   Content to populate the info window.
     */
    openMarkerPopup(marker, content) {
      if (!this.infoWin) {
        this.infoWin = new google.maps.InfoWindow();
      }

      this.infoWin.setContent(content);
      this.infoWin.open({
        map: this._gmap,
        anchor: marker._marker,
      });
    }

    /**
     * Set the map center location.
     *
     * @param {Object|google.maps.LatLng} center
     *   LatLng coordinate to center the map on.
     */
    setCenter(center) {
      this.center = center;

      if (this._gmap) {
        this._gmap.setCenter(center);
      }
    }

    /**
     * Add a new map marker based on the marker definition.
     *
     * @param {Object} data
     *   The marker definition, which should at minimum have a "latLng" value.
     * @param {bool} updateFit
     *   Should the bounds be adjusted when this marker is added. This ensures
     *   that newly added map markers are visible when using "fitBounds" option.
     *
     * @return {Object}
     *   Return the created map marker instance.
     */
    createMarker(data, updateFit = true) {
      const mset = data.setId && this.markerBuilders[data.setId]
        ? data.setId : this.markerType;

      // Generate a map marker and add it to this map's markers list.
      if (this.markerBuilders[mset]) {
        const marker = this.markerBuilders[mset](data);

        // Only if the map is initialized can we also initialize the map marker
        // and update the map bounds. Otherwise this is done when the map is
        // initialized itself.
        //
        // @see GMap::init()
        if (this._gmap) {
          marker.init(this);

          // If the marker is not already in the bounds, then we need
          // need check if the bounds and display window need to be updated.
          if (!this.bounds.contains(marker.position)) {
            this.bounds.extend(marker.position);

            if (updateFit && this.fitBounds && this.markers.size) {
              this.refreshBounds();
            }
          }
        }

        this.markers.set(marker, marker);
        return marker;
      }
    }

    /**
     * Remove the marker from the map.
     *
     * @param {Object} marker
     *   The marker data to delete.
     * @param {bool} updateFit
     *   Whether or not to update the map based on the removal of this marker.
     */
    removeMarker(marker, updateFit = true) {
      this.markers.delete(marker);

      // Remove the marker from the map.
      if (this._gmap) {
        marker.detach();

        if (updateFit) {
          // Evaluate if the current position requires a marker bounds update.
          this.markerPositionChanged(marker.position);
        }
      }
    }

    /**
     * Add click event listeners to the marker. If Google maps have not been
     * initialized yet, the marker object will add the listener when the marker
     * is initialized.
     *
     * @param {string} event
     *   Name of the event to add the new event listener to.
     * @param {function} listener
     *   Event listener function to add the the marker.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    on(event, listener) {
      // If Google Maps API has already been initialized, call the 'init' immediately.
      // This map will never init again in this lifecycle (will be recycled) so will
      // not need to keep track of this init for later.
      if (event === 'init' && this._gmap) {
        listener(this);
      }
      else {
        const data = { listener, handle: null };

        if (this.listeners.has(event)) {
          this.listeners.get(event).push(data);
        }
        else {
          this.listeners.set(event, [data]);
        }

        // If map is already initialize, register the event directly.
        if (this._gmap && event !== 'destroy') {
          data.handle = this._gmap.addListener(event, listener);
        }
      }

      return this;
    }

    /**
     * Remove event listeners for this map instance.
     *
     * @param {string} event
     *   The event identifier (click, keypress, etc...)
     * @param {function=} listener
     *   If provided, only remove this event listener, otherwise, remove all
     *   click events for this marker.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    off(event, listener = null) {
      // Remove all handlers if no specific listener passed in.
      if (!listener) {
        if (event !== 'init' || event !== 'destroy') {
          (this.listeners.get(event) || []).forEach((data) => {
            if (data.handle) google.maps.event.removeListener(data.handle);
          });
        }

        this.listeners.delete(event);
      }
      else {
        const listeners = this.listeners.get(event) || [];

        // Go hunting for the callback to remove.
        for (let i = 0; i < listeners.length; ++i) {
          const data = this.listeners[i];

          if (data.listener === listener || data.handle === listener) {
            if (listeners.length <= 1) {
              this.listeners.delete(event);
            }
            else {
              listeners.splice(i, 1);
            }

            if (data.handle) {
              google.maps.event.removeListener(data.handle);
            }
            break;
          }
        }
      }
      return this;
    }

    /**
     * Potential map clean-up.
     */
    destroy() {
      if (this._gmap) {
        // Register any click listeners which were already registered but
        // where waiting for the map to get initialized.
        (this.listeners.get('destroy') || []).forEach((data) => data.listener(this));

        // Remove only the event handlers this instance assigned. Using
        // google.map.event.clearListeners() or google.map.event.clearInstanceListeners()
        // removes some native handlers, and makes the map behave incorrectly on reuse.
        this.listeners.forEach((list, event) => this.off(event));

        // Google maps suggests that the map should be recycled rather than
        // have the instance and data deleted. Detach the map instance, and make
        // it available for other map instances to reuse.
        this.el.removeChild(this._gmap.getDiv());
        GMap.gmapPool.push(this._gmap);
      }
      else if (this.initCallback) {
        // Prevent map initialization because it is unnecessary after this.
        loader.removeInit('gmap', this.initCallback);
      }

      // Remove all markers from the instance.
      this.markers.forEach((marker) => marker.detach());
      this.markers.clear();

      // Remove all event listeners.
      this.listeners.clear();
    }
  }

  /**
   * Maintains a pool of google map instances that can be reused.
   *
   * Google maps suggests that the map should be recycled rather than
   * have the instance and data deleted. This keeps the list of maps ready to
   * be recycled.
   */
  GMap.gmapPool = [];

  /**
   * Add the Mapkit handler for generating a Google Maps instance.
   *
   * @param {HTMLElement} el
   *   The HTML element to attach the map to.
   * @param {Object} settings
   *   The map settings to use when generating the map.
   * @param {Object} markerBuilders
   *   The marker factory functions for creating markers for this map.
   *
   * @return {Object}
   *   The generated Map instance.
   */
  Mapkit.handler.gmap = function createGMap(el, settings, markerBuilders) {
    const instance = new GMap(el, settings, markerBuilders);
    const gmap = GMap.gmapPool.pop();

    if (gmap) {
      instance.init(gmap);
    }
    else {
      // Register this map to get initialized when Google maps loads.
      instance.initCallback = instance.init.bind(instance);
      loader.addInit('gmap', instance.initCallback);
    }
    return instance;
  };
})(Drupal, Mapkit);
