(({ Toolshed: ts }, Mapkit) => {
  /**
   * Generates the Places field and input data bind callback for HTML input
   * elements.
   *
   * @param {string} key
   *   The data key which identifies which part of the address property to bind
   *   to this input element.
   * @param {HTMLInputElement} input
   *   The input element to create the data binding for.
   *
   * @return {*}
   *   An object with a "field" and "callback" method for. The "field" key tells
   *   The Places API what data is requested, and the callback is a method
   *   which is bound to the input element and will populate it with the
   *   value from the requested data property (key).
   */
  function buildInputCallback(key, input) {
    switch (key) {
      case 'country':
      case 'postal_code':
      case 'administrative_area_level_1':
      case 'administrative_area_level_2':
      case 'locality':
      case 'street_address':
        return {
          field: 'address_component',
          callback: (place) => {
            let val = '';
            place.address_components.forEach(({ short_name: name, types }) => {
              if (types.indexOf(key) >= 0) val = name;
            });

            input.value = val;
          },
        };

      case 'latlng':
        return {
          field: 'geometry.location',
          callback: (place) => {
            const loc = (place.geometry || {}).location;
            input.value = loc ? `${loc.lat().toFixed(7)},${loc.lng().toFixed(7)}` : '';
          },
        };

      default:
        return {};
    }
  }

  /**
   * Provide autocomplete suggestions and location resolution through the
   * Google Maps Places API.
   */
  Mapkit.ac.gmapPlaces = class {
    /**
     * Create a new Google Maps Places handler for an autocomplete instance.
     *
     * @param {Mapkit.Autocomplete} ac
     *   Reference to the Mapkit.Autocomplete instance that uses this handler.
     */
    constructor(ac) {
      this.loaderId = 'gmap';
      this.ac = ac;

      // Initialize the request objects.
      this.reqFields = [];
      this.inputs = [];
    }

    /**
     * Init method should be called after the map libraries have been loaded.
     *
     * Sets up the Google Maps Places services and creates the request constants
     * from the configurations.
     */
    init() {
      const attribution = new ts.Element('footer', {}, this.ac.suggestWrap);
      attribution.addClass(['attributions', 'attributions--google']);

      // Create the Google Maps services after the libraries have been loaded.
      this.acService = new google.maps.places.AutocompleteService();
      this.placesAPI = new google.maps.places.PlacesService(attribution.el);

      this.reqOptions = { };

      const {
        matchType: type,
        countries,
        bounds,
        pattern,
      } = this.ac.config;

      if (type === 'address') {
        this.reqOptions.types = ['address'];
      }
      else if (type === 'city') {
        this.reqOptions.types = ['(cities)'];
      }
      else if (type === 'region') {
        this.reqOptions.types = ['(regions)'];
      }

      if (pattern) {
        this.filterRegEx = new RegExp(pattern, 'i');
      }

      if (countries) {
        this.reqOptions.componentRestrictions = { country: countries };
      }

      if (bounds && bounds.bottom && bounds.top) {
        const bKey = bounds.type === 'restriction' ? 'locationRestriction' : 'locationBias';
        this.reqOptions[bKey] = new google.maps.LatLngBounds(
          new google.maps.LatLng(bounds.bottom, bounds.right),
          new google.maps.LatLng(bounds.top, bounds.left),
        );
      }
    }

    /**
     * Is this handler ready to make fetch suggestions and resolve locations?
     *
     * @return {bool}
     *   TRUE if the service is ready to make autocomplete and places API calls.
     */
    isReady() {
      return this.acService && this.placesAPI;
    }

    /**
     * Attached the data callback to populate values from the Places API into
     * input elements. This allows us to fill in the values when calling
     * this.select().
     *
     * @param {string} key
     *   The data key for the location property this input populates.
     * @param {HTMLInputElement} input
     *   The HTML input element to attached the data callback to.
     */
    attachInput(key, input) {
      const { field, callback } = buildInputCallback(key, input);

      // Translates to a valid places value and value update callback.
      if (field && callback) {
        this.inputs.push({
          el: input,
          method: callback,
        });

        if (this.reqFields.indexOf(field) < 0) {
          this.reqFields.push(field);
        }
      }
    }

    /**
     * Clears the values from the managed input elements.
     *
     * This ensures that no residual data is kept if the user changes the text
     * entry without selecting a places suggestion.
     */
    clearInputValues() {
      this.inputs.forEach(({ el }) => {
        el.value = '';
      });
    }

    /**
     * User has selected an autocomplete suggestion, update input values and
     * apply the values from the Places API from the selected item.
     *
     * @param {*} item
     *   The autocomplete suggested item selected by the user.
     */
    select(item) {
      if (!this.reqFields.length) {
        // No fields are requested from places, no need to make the call.
        return;
      }

      const req = {
        placeId: item.value.id,
        sessionToken: this.sessionToken,
        fields: this.placeFields,
      };

      // Token is being used in a place details call, we need a new token
      // for successive autocomplete calls.
      delete this.sessionToken;

      // Get additional places data if additional data fields are configured.
      this.placesAPI.getDetails(req, (place, status) => {
        if (google.maps.places.PlacesServiceStatus.OK === status) {
          this.inputs.forEach((input) => input.method(place));
        }
      });
    }

    /**
     * Format the result from the API response into autocomplete suggest text.
     *
     * @param {*} item
     *   The data item returned by the Places service.
     *
     * @return {string}
     *   The text to display as the autocomplete suggestion.
     */
    formatSuggestText(item) {
      return item.description;
    }

    /**
     * Make request to the Places API to get location suggestions for
     * autocomplete suggestions display.
     *
     * @param {string} text
     *   The text to query for matching places suggestions.
     */
    fetchSuggestions(text) {
      // Create and keep a session token for searches to try to optimize
      // billing. This session is valid until we make a Places Detail API call
      // in the Autocomplete.select() method to get full places results.
      if (!this.sessionToken) {
        this.sessionToken = new google.maps.places.AutocompleteSessionToken();
      }

      const req = {
        input: text,
        sessionToken: this.sessionToken,
        ...this.reqOptions,
      };

      this.acService.getPlacePredictions(req, (predictions, status) => {
        this.ac.pending = false;

        let suggest = null;
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          const data = [];

          predictions.forEach((item) => {
            if (!this.filterRegEx || this.filterRegEx.test(item.description)) {
              data.push({
                text: this.formatSuggestText(item),
                value: {
                  types: item.types,
                  id: item.place_id,
                },
              });
            }
          });

          suggest = { list: data };
        }
        else if (google.maps.places.PlacesServiceStatus.ZERO_RESULTS === status) {
          suggest = null;
        }

        // Let the autocomplete elements know that we are done fetching results.
        this.ac.setFetchedSuggestions(suggest);
      });
    }

    /**
     * Clear any references and get ready to detach this autocomplete handler.
     */
    destroy() {
      delete this.sessionToken;
    }
  };
})(Drupal, Mapkit);
