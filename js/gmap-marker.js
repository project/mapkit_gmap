"use strict";

(({
  Mapkit
}) => {
  /**
   * Impelments the behavior the default Google maps marker.
   */
  class GMapMarker {
    constructor(index, latLng, label, config) {
      this.label = label || null;
      this.index = index;
      this.latLng = latLng;
      this.labelConfig = config.label;
      this.markerSymbol = config.symbol;

      // Keep track of the marker event listeners, keyed by event name.
      this.listeners = new Map();
    }

    /**
     * Initialize the marker when the map is initialized. This can only occur
     * after the Google Maps API is fully loaded.
     *
     * @param {google.maps.Map} map
     *   Google maps instance to attach this marker instance to.
     */
    init(map) {
      this._map = map;
      this._marker = new google.maps.Marker({
        map: map._gmap,
        position: this.position,
        zIndex: this.index,
        icon: this._getIcon(),
        label: this._getLabel(),
        _mapkitRef: this
      });

      // If spiderfy is enabled for the map
      if (map.oms) {
        map.oms.trackMarker(this._marker);
      }

      // Register any click listeners which were already registered but
      // where waiting for the map to get initialized.
      this.listeners.forEach((items, event) => {
        if (event === 'click' && this._map.oms) {
          event = 'spider_click';
        }
        items.forEach(data => {
          data.handle = this._marker.addListener(event, data.listener);
        });
      });
    }

    /**
     * Get the current latLng position of this marker.
     *
     * @return {LatLngLiteral}
     *   Returns and object with 'lat' and 'lng' properties for the latitude
     *   longitude of the represented location.
     */
    get position() {
      return this.latLng;
    }

    /**
     * Change the current marker position. This will also inform the map of the
     * relocation, so the map can react if needed to the change.
     *
     * @param {LatLngLiteral} latLng
     *   The latitude and longitude coordinate for the map marker.
     */
    set position(latLng) {
      const oldPos = this.latLng;
      this.latLng = latLng;
      if (this._marker) {
        this._marker.setPosition(latLng);
        this._map.markerPositionChanged(oldPos, latLng);
      }
    }

    /**
     * Get the marker HTML in a form that can be displayed outside of the map.
     * This is useful placing a marker display next to a result listing so users
     * can correllate the marker on the map to the result row.
     *
     * @return {string|null}
     *   Get the HTML snippet that can be embedded to represent the marker in
     *   a list view or other HTML display. Returns NULL if no label (all markers)
     *   look the same.
     */
    get innerHTML() {
      return this.label ? `<div class="mapkit-marker">${this.label}</div>` : null;
    }

    /**
     * Get the label configurations for rendering the label.
     *
     * @return {Object|null}
     *   Marker label color, text configurations and label text compatible for
     *   Google maps label configurations.
     */
    _getLabel() {
      return this.label ? {
        ...this.labelConfig,
        text: this.label
      } : null;
    }

    /**
     * Get map icon display configurations compatible with Google maps marker
     * icon property.
     *
     * @return {Object|null}
     *   Map marker display settings which are compatible with Google maps
     *   marker icon configuration. This would be SVG path definition, image
     *   file path, or NULL.
     */
    _getIcon() {
      // eslint-disable-line class-methods-use-this
      return null;
    }

    /**
     * Bring the marker to the top of the display.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    toFront() {
      if (this._marker) {
        this._marker.setZIndex(10000);
      }
      return this;
    }

    /**
     * Change the Z depth of the marker.
     *
     * @param {int=} z
     *   The z-index value to set the marker to. If no value provided, then
     *   reset the index back to the initial value.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    setZIndex(z) {
      if (this._marker) {
        z = z || this.index;
        this._marker.setZIndex(z);
      }
      return this;
    }

    /**
     * Change the marker icon if changes are supported.
     *
     * @param {Object} changes
     *   Icon configuration changes to apply if supported.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    alterIcon(changes) {
      // eslint-disable-line no-unused-vars
      // Default map markers cannot be modified, so does nothing here, but
      // should be overridden by subclasses.
      return this;
    }

    /**
     * Resets the marker display back to the original settings.
     *
     * @return {this}
     *   Returns this marker instance for method chaining.
     */
    resetMarker() {
      if (this._marker) {
        this._marker.setIcon(this._getIcon());
      }
      return this;
    }

    /**
     * Attach this marker to be displayed on a map.
     *
     * @param {Mapkit.GMap} map
     *   Map to attach the marker to.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    attach(map) {
      this._map = map;
      if (this._marker && map._gmap) {
        this._marker.setMap(map._gmap);
        if (map.oms) {
          map.oms.trackMarker(this._marker);
        }
      }
      return this;
    }

    /**
     * Detach the marker from its current map display.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    detach() {
      if (this._marker) {
        if (this._map.oms) {
          this._map.oms.forgetMarker(this._marker);
        }

        // Remove all events and disassociate with any map instance.
        google.maps.event.clearInstanceListeners(this._marker);
        this._marker.setMap(null);
        delete this._map;
      }
      return this;
    }

    /**
     * Add click event listeners to the marker. If Google maps have not been
     * initialized yet, the marker object will add the listener when the marker
     * is initialized.
     *
     * @param {string} event
     *   Name of the event to add the new event listener to.
     * @param {function} listener
     *   Event listener function to add the the marker.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    on(event, listener) {
      const data = {
        listener,
        handle: null
      };
      if (this.listeners.has(event)) {
        this.listeners.get(event).push(data);
      } else {
        this.listeners.set(event, [data]);
      }
      if (this._marker) {
        // When OverlappingMarkerSpiderfier is enabled, the click event is
        // replaced with the "spider_click" event.
        if (this._map.oms && event === 'click') {
          event = 'spider_click';
        }
        data.handle = this._marker.addListener(event, listener);
      }
      return this;
    }

    /**
     * Remove click event listeners for this map marker.
     *
     * @param {string} event
     *   Name of the
     * @param {function=} listener
     *   If provided, only remove this event listener, otherwise, remove all
     *   click events for this marker.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    off(event, listener) {
      // Remove all click handlers if no specific listener passed in.
      if (!listener) {
        this.listeners.delete(event);
        if (this._marker) {
          // Check for OverlappingMarkerSpiderfier and adjust the event name.
          if (this._map.oms && event === 'click') {
            event = 'spider_click';
          }
          google.maps.event.clearListeners(this._marker, event);
        }
      } else {
        const listeners = this.listeners.get(event) || [];

        // Go hunting for the callback to remove.
        for (let i = 0; i < listeners.length; ++i) {
          const data = this.listeners[i];
          if (data.listener === listener || data.handle === listener) {
            if (listeners.length <= 1) {
              this.listeners.delete(event);
            } else {
              listeners.splice(i, 1);
            }
            if (this._marker && data.handle) {
              google.maps.event.removeListener(data.handle);
            }
            break;
          }
        }
      }
      return this;
    }
  }

  /**
   * Implements a Google maps marker from a SVG path.
   */
  class GMapSymbolMarker extends GMapMarker {
    constructor(index, latLng, label, config) {
      super(index, latLng, label, config);
      this.markerSymbol = config.symbol;
    }

    /**
     * @inheritdoc
     */
    _getLabel() {
      if (this.label && this.label.length) {
        return super._getLabel();
      }
      return {
        text: '\u2022',
        fontFamily: 'arial, san-serif',
        fontSize: '40px',
        color: '#fff'
      };
    }

    /**
     * @inheritdoc
     */
    _getIcon() {
      return this.markerSymbol;
    }

    /**
     * @inheritdoc
     */
    alterIcon(changes) {
      if (this._marker) {
        const icon = this._marker.getIcon();
        Object.assign(icon, changes);
        this._marker.setIcon(icon);
      }
      return this;
    }

    /**
     * @inheritdoc
     */
    get innerHTML() {
      const icon = this._getIcon();
      let html = `<div class="mapkit-marker" style="position: relative;">
        <svg viewBox="0 0 ${icon.size.width} ${icon.size.height}" style="fill:${icon.fillColor}; stroke:${icon.strokeColor};stroke-width:${icon.strokeWeight};">
          <path d="${icon.path}"/>
        </svg>`;

      // Include a label display if a label is provided.
      if (this.label) {
        const label = this._getLabel();
        const styles = 'position: absolute; transform: translate(-50%, -50%);' + `top: ${100 * icon.labelOrigin.y / icon.size.height}%; left: ${100 * icon.labelOrigin.x / icon.size.width}%;` + `color: ${label.color}; font-size: ${label.fontSize};`;
        html += `<span style="${styles}">${this.label}</span>`;
      }
      html += '</div>';
      return html;
    }
  }

  /**
   * Create letter labeling for markers.
   *
   * @param {int} i
   *   The marker index to generate the label for.
   *
   * @return {string}
   *   The letters correllating to the marker index.
   */
  function LetterMarkerLabels(i) {
    let str = '';
    while (i > 26) {
      const r = i % 26;
      i = (i - r) / 26;
      if (r) {
        str = String.fromCharCode(64 + r) + str;
      } else {
        str = `Z${str}`;
        i--; // eslint-disable-line no-plusplus
      }
    }

    return String.fromCharCode(64 + i) + str;
  }

  /**
   * Convert the numeric marker index into a string to display as the label.
   *
   * @param {int} i
   *   The marker index to transform into the marker label.
   *
   * @return {string}
   *   The string value of the marker index.
   */
  function NumberMarkerLabels(i) {
    return i.toString();
  }

  /**
   * Get the label generator function to use based on the desired label styles.
   *
   * @param {string|null} style
   *   The name of the label styling to fetch the generator function for.
   *
   * @return {function}
   *   Function which takes the marker index, and returns the string label.
   */
  function getLabelGenerator(style) {
    switch (style || 'numbers') {
      case 'letters':
        return LetterMarkerLabels;
      case 'numbers':
        return NumberMarkerLabels;
      default:
        return () => null;
    }
  }

  /**
   * Generate Standard Google Maps marker factory method based on the settings.
   *
   * @param {Object} settings
   *   The marker setting the resulting marker factory will use when generating
   *   new markers instances.
   *
   * @return {function}
   *   A marker factory function, for creating map marker instances.
   */
  Mapkit.marker.gmapMarker = function createGmapMarker(settings) {
    const offset = settings.offset || 0;
    const labelGen = getLabelGenerator(settings.label.style);
    let count = settings.start || 1;
    let MarkerClass = GMapMarker;
    delete settings.label.style;
    const config = {
      label: {
        color: '#000',
        fontSize: '16px',
        ...settings.label
      },
      symbol: null
    };

    // Setup SVG path settings if Symbol configurations are present.
    if (settings.symbol) {
      MarkerClass = GMapSymbolMarker;

      // Ensure some of the marker sane defaults.
      config.symbol = {
        fillOpacity: 1,
        fillColor: '#ea0000',
        strokeColor: '#fff',
        strokeWeight: 1,
        ...settings.symbol
      };
    }

    // Generator function for creating markers.
    return function createMarker(data) {
      const index = (data.index || count++) + offset; // eslint-disable-line no-plusplus

      return new MarkerClass(index, data.latLng, labelGen(index), config);
    };
  };
})(Drupal);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ21hcC1tYXJrZXIuanMiLCJuYW1lcyI6WyJNYXBraXQiLCJHTWFwTWFya2VyIiwiY29uc3RydWN0b3IiLCJpbmRleCIsImxhdExuZyIsImxhYmVsIiwiY29uZmlnIiwibGFiZWxDb25maWciLCJtYXJrZXJTeW1ib2wiLCJzeW1ib2wiLCJsaXN0ZW5lcnMiLCJNYXAiLCJpbml0IiwibWFwIiwiX21hcCIsIl9tYXJrZXIiLCJnb29nbGUiLCJtYXBzIiwiTWFya2VyIiwiX2dtYXAiLCJwb3NpdGlvbiIsInpJbmRleCIsImljb24iLCJfZ2V0SWNvbiIsIl9nZXRMYWJlbCIsIl9tYXBraXRSZWYiLCJvbXMiLCJ0cmFja01hcmtlciIsImZvckVhY2giLCJpdGVtcyIsImV2ZW50IiwiZGF0YSIsImhhbmRsZSIsImFkZExpc3RlbmVyIiwibGlzdGVuZXIiLCJvbGRQb3MiLCJzZXRQb3NpdGlvbiIsIm1hcmtlclBvc2l0aW9uQ2hhbmdlZCIsImlubmVySFRNTCIsInRleHQiLCJ0b0Zyb250Iiwic2V0WkluZGV4IiwieiIsImFsdGVySWNvbiIsImNoYW5nZXMiLCJyZXNldE1hcmtlciIsInNldEljb24iLCJhdHRhY2giLCJzZXRNYXAiLCJkZXRhY2giLCJmb3JnZXRNYXJrZXIiLCJjbGVhckluc3RhbmNlTGlzdGVuZXJzIiwib24iLCJoYXMiLCJnZXQiLCJwdXNoIiwic2V0Iiwib2ZmIiwiZGVsZXRlIiwiY2xlYXJMaXN0ZW5lcnMiLCJpIiwibGVuZ3RoIiwic3BsaWNlIiwicmVtb3ZlTGlzdGVuZXIiLCJHTWFwU3ltYm9sTWFya2VyIiwiZm9udEZhbWlseSIsImZvbnRTaXplIiwiY29sb3IiLCJnZXRJY29uIiwiT2JqZWN0IiwiYXNzaWduIiwiaHRtbCIsInNpemUiLCJ3aWR0aCIsImhlaWdodCIsImZpbGxDb2xvciIsInN0cm9rZUNvbG9yIiwic3Ryb2tlV2VpZ2h0IiwicGF0aCIsInN0eWxlcyIsImxhYmVsT3JpZ2luIiwieSIsIngiLCJMZXR0ZXJNYXJrZXJMYWJlbHMiLCJzdHIiLCJyIiwiU3RyaW5nIiwiZnJvbUNoYXJDb2RlIiwiTnVtYmVyTWFya2VyTGFiZWxzIiwidG9TdHJpbmciLCJnZXRMYWJlbEdlbmVyYXRvciIsInN0eWxlIiwibWFya2VyIiwiZ21hcE1hcmtlciIsImNyZWF0ZUdtYXBNYXJrZXIiLCJzZXR0aW5ncyIsIm9mZnNldCIsImxhYmVsR2VuIiwiY291bnQiLCJzdGFydCIsIk1hcmtlckNsYXNzIiwiZmlsbE9wYWNpdHkiLCJjcmVhdGVNYXJrZXIiLCJEcnVwYWwiXSwic291cmNlcyI6WyJnbWFwLW1hcmtlci5lczYuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKCh7IE1hcGtpdCB9KSA9PiB7XG4gIC8qKlxuICAgKiBJbXBlbG1lbnRzIHRoZSBiZWhhdmlvciB0aGUgZGVmYXVsdCBHb29nbGUgbWFwcyBtYXJrZXIuXG4gICAqL1xuICBjbGFzcyBHTWFwTWFya2VyIHtcbiAgICBjb25zdHJ1Y3RvcihpbmRleCwgbGF0TG5nLCBsYWJlbCwgY29uZmlnKSB7XG4gICAgICB0aGlzLmxhYmVsID0gbGFiZWwgfHwgbnVsbDtcbiAgICAgIHRoaXMuaW5kZXggPSBpbmRleDtcbiAgICAgIHRoaXMubGF0TG5nID0gbGF0TG5nO1xuXG4gICAgICB0aGlzLmxhYmVsQ29uZmlnID0gY29uZmlnLmxhYmVsO1xuICAgICAgdGhpcy5tYXJrZXJTeW1ib2wgPSBjb25maWcuc3ltYm9sO1xuXG4gICAgICAvLyBLZWVwIHRyYWNrIG9mIHRoZSBtYXJrZXIgZXZlbnQgbGlzdGVuZXJzLCBrZXllZCBieSBldmVudCBuYW1lLlxuICAgICAgdGhpcy5saXN0ZW5lcnMgPSBuZXcgTWFwKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSW5pdGlhbGl6ZSB0aGUgbWFya2VyIHdoZW4gdGhlIG1hcCBpcyBpbml0aWFsaXplZC4gVGhpcyBjYW4gb25seSBvY2N1clxuICAgICAqIGFmdGVyIHRoZSBHb29nbGUgTWFwcyBBUEkgaXMgZnVsbHkgbG9hZGVkLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtnb29nbGUubWFwcy5NYXB9IG1hcFxuICAgICAqICAgR29vZ2xlIG1hcHMgaW5zdGFuY2UgdG8gYXR0YWNoIHRoaXMgbWFya2VyIGluc3RhbmNlIHRvLlxuICAgICAqL1xuICAgIGluaXQobWFwKSB7XG4gICAgICB0aGlzLl9tYXAgPSBtYXA7XG4gICAgICB0aGlzLl9tYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtcbiAgICAgICAgbWFwOiBtYXAuX2dtYXAsXG4gICAgICAgIHBvc2l0aW9uOiB0aGlzLnBvc2l0aW9uLFxuICAgICAgICB6SW5kZXg6IHRoaXMuaW5kZXgsXG4gICAgICAgIGljb246IHRoaXMuX2dldEljb24oKSxcbiAgICAgICAgbGFiZWw6IHRoaXMuX2dldExhYmVsKCksXG4gICAgICAgIF9tYXBraXRSZWY6IHRoaXMsXG4gICAgICB9KTtcblxuICAgICAgLy8gSWYgc3BpZGVyZnkgaXMgZW5hYmxlZCBmb3IgdGhlIG1hcFxuICAgICAgaWYgKG1hcC5vbXMpIHtcbiAgICAgICAgbWFwLm9tcy50cmFja01hcmtlcih0aGlzLl9tYXJrZXIpO1xuICAgICAgfVxuXG4gICAgICAvLyBSZWdpc3RlciBhbnkgY2xpY2sgbGlzdGVuZXJzIHdoaWNoIHdlcmUgYWxyZWFkeSByZWdpc3RlcmVkIGJ1dFxuICAgICAgLy8gd2hlcmUgd2FpdGluZyBmb3IgdGhlIG1hcCB0byBnZXQgaW5pdGlhbGl6ZWQuXG4gICAgICB0aGlzLmxpc3RlbmVycy5mb3JFYWNoKChpdGVtcywgZXZlbnQpID0+IHtcbiAgICAgICAgaWYgKGV2ZW50ID09PSAnY2xpY2snICYmIHRoaXMuX21hcC5vbXMpIHtcbiAgICAgICAgICBldmVudCA9ICdzcGlkZXJfY2xpY2snO1xuICAgICAgICB9XG5cbiAgICAgICAgaXRlbXMuZm9yRWFjaCgoZGF0YSkgPT4ge1xuICAgICAgICAgIGRhdGEuaGFuZGxlID0gdGhpcy5fbWFya2VyLmFkZExpc3RlbmVyKGV2ZW50LCBkYXRhLmxpc3RlbmVyKTtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXQgdGhlIGN1cnJlbnQgbGF0TG5nIHBvc2l0aW9uIG9mIHRoaXMgbWFya2VyLlxuICAgICAqXG4gICAgICogQHJldHVybiB7TGF0TG5nTGl0ZXJhbH1cbiAgICAgKiAgIFJldHVybnMgYW5kIG9iamVjdCB3aXRoICdsYXQnIGFuZCAnbG5nJyBwcm9wZXJ0aWVzIGZvciB0aGUgbGF0aXR1ZGVcbiAgICAgKiAgIGxvbmdpdHVkZSBvZiB0aGUgcmVwcmVzZW50ZWQgbG9jYXRpb24uXG4gICAgICovXG4gICAgZ2V0IHBvc2l0aW9uKCkge1xuICAgICAgcmV0dXJuIHRoaXMubGF0TG5nO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENoYW5nZSB0aGUgY3VycmVudCBtYXJrZXIgcG9zaXRpb24uIFRoaXMgd2lsbCBhbHNvIGluZm9ybSB0aGUgbWFwIG9mIHRoZVxuICAgICAqIHJlbG9jYXRpb24sIHNvIHRoZSBtYXAgY2FuIHJlYWN0IGlmIG5lZWRlZCB0byB0aGUgY2hhbmdlLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtMYXRMbmdMaXRlcmFsfSBsYXRMbmdcbiAgICAgKiAgIFRoZSBsYXRpdHVkZSBhbmQgbG9uZ2l0dWRlIGNvb3JkaW5hdGUgZm9yIHRoZSBtYXAgbWFya2VyLlxuICAgICAqL1xuICAgIHNldCBwb3NpdGlvbihsYXRMbmcpIHtcbiAgICAgIGNvbnN0IG9sZFBvcyA9IHRoaXMubGF0TG5nO1xuICAgICAgdGhpcy5sYXRMbmcgPSBsYXRMbmc7XG5cbiAgICAgIGlmICh0aGlzLl9tYXJrZXIpIHtcbiAgICAgICAgdGhpcy5fbWFya2VyLnNldFBvc2l0aW9uKGxhdExuZyk7XG4gICAgICAgIHRoaXMuX21hcC5tYXJrZXJQb3NpdGlvbkNoYW5nZWQob2xkUG9zLCBsYXRMbmcpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldCB0aGUgbWFya2VyIEhUTUwgaW4gYSBmb3JtIHRoYXQgY2FuIGJlIGRpc3BsYXllZCBvdXRzaWRlIG9mIHRoZSBtYXAuXG4gICAgICogVGhpcyBpcyB1c2VmdWwgcGxhY2luZyBhIG1hcmtlciBkaXNwbGF5IG5leHQgdG8gYSByZXN1bHQgbGlzdGluZyBzbyB1c2Vyc1xuICAgICAqIGNhbiBjb3JyZWxsYXRlIHRoZSBtYXJrZXIgb24gdGhlIG1hcCB0byB0aGUgcmVzdWx0IHJvdy5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge3N0cmluZ3xudWxsfVxuICAgICAqICAgR2V0IHRoZSBIVE1MIHNuaXBwZXQgdGhhdCBjYW4gYmUgZW1iZWRkZWQgdG8gcmVwcmVzZW50IHRoZSBtYXJrZXIgaW5cbiAgICAgKiAgIGEgbGlzdCB2aWV3IG9yIG90aGVyIEhUTUwgZGlzcGxheS4gUmV0dXJucyBOVUxMIGlmIG5vIGxhYmVsIChhbGwgbWFya2VycylcbiAgICAgKiAgIGxvb2sgdGhlIHNhbWUuXG4gICAgICovXG4gICAgZ2V0IGlubmVySFRNTCgpIHtcbiAgICAgIHJldHVybiB0aGlzLmxhYmVsID8gYDxkaXYgY2xhc3M9XCJtYXBraXQtbWFya2VyXCI+JHt0aGlzLmxhYmVsfTwvZGl2PmAgOiBudWxsO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldCB0aGUgbGFiZWwgY29uZmlndXJhdGlvbnMgZm9yIHJlbmRlcmluZyB0aGUgbGFiZWwuXG4gICAgICpcbiAgICAgKiBAcmV0dXJuIHtPYmplY3R8bnVsbH1cbiAgICAgKiAgIE1hcmtlciBsYWJlbCBjb2xvciwgdGV4dCBjb25maWd1cmF0aW9ucyBhbmQgbGFiZWwgdGV4dCBjb21wYXRpYmxlIGZvclxuICAgICAqICAgR29vZ2xlIG1hcHMgbGFiZWwgY29uZmlndXJhdGlvbnMuXG4gICAgICovXG4gICAgX2dldExhYmVsKCkge1xuICAgICAgcmV0dXJuIHRoaXMubGFiZWwgPyB7XG4gICAgICAgIC4uLnRoaXMubGFiZWxDb25maWcsXG4gICAgICAgIHRleHQ6IHRoaXMubGFiZWwsXG4gICAgICB9IDogbnVsbDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXQgbWFwIGljb24gZGlzcGxheSBjb25maWd1cmF0aW9ucyBjb21wYXRpYmxlIHdpdGggR29vZ2xlIG1hcHMgbWFya2VyXG4gICAgICogaWNvbiBwcm9wZXJ0eS5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge09iamVjdHxudWxsfVxuICAgICAqICAgTWFwIG1hcmtlciBkaXNwbGF5IHNldHRpbmdzIHdoaWNoIGFyZSBjb21wYXRpYmxlIHdpdGggR29vZ2xlIG1hcHNcbiAgICAgKiAgIG1hcmtlciBpY29uIGNvbmZpZ3VyYXRpb24uIFRoaXMgd291bGQgYmUgU1ZHIHBhdGggZGVmaW5pdGlvbiwgaW1hZ2VcbiAgICAgKiAgIGZpbGUgcGF0aCwgb3IgTlVMTC5cbiAgICAgKi9cbiAgICBfZ2V0SWNvbigpIHsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBjbGFzcy1tZXRob2RzLXVzZS10aGlzXG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBCcmluZyB0aGUgbWFya2VyIHRvIHRoZSB0b3Agb2YgdGhlIGRpc3BsYXkuXG4gICAgICpcbiAgICAgKiBAcmV0dXJuIHt0aGlzfVxuICAgICAqICAgVGhpcyBtYXJrZXIgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cbiAgICAgKi9cbiAgICB0b0Zyb250KCkge1xuICAgICAgaWYgKHRoaXMuX21hcmtlcikge1xuICAgICAgICB0aGlzLl9tYXJrZXIuc2V0WkluZGV4KDEwMDAwKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENoYW5nZSB0aGUgWiBkZXB0aCBvZiB0aGUgbWFya2VyLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtpbnQ9fSB6XG4gICAgICogICBUaGUgei1pbmRleCB2YWx1ZSB0byBzZXQgdGhlIG1hcmtlciB0by4gSWYgbm8gdmFsdWUgcHJvdmlkZWQsIHRoZW5cbiAgICAgKiAgIHJlc2V0IHRoZSBpbmRleCBiYWNrIHRvIHRoZSBpbml0aWFsIHZhbHVlLlxuICAgICAqXG4gICAgICogQHJldHVybiB7dGhpc31cbiAgICAgKiAgIFRoaXMgbWFya2VyIGluc3RhbmNlIGZvciBtZXRob2QgY2hhaW5pbmcuXG4gICAgICovXG4gICAgc2V0WkluZGV4KHopIHtcbiAgICAgIGlmICh0aGlzLl9tYXJrZXIpIHtcbiAgICAgICAgeiA9IHogfHwgdGhpcy5pbmRleDtcbiAgICAgICAgdGhpcy5fbWFya2VyLnNldFpJbmRleCh6KTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENoYW5nZSB0aGUgbWFya2VyIGljb24gaWYgY2hhbmdlcyBhcmUgc3VwcG9ydGVkLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtPYmplY3R9IGNoYW5nZXNcbiAgICAgKiAgIEljb24gY29uZmlndXJhdGlvbiBjaGFuZ2VzIHRvIGFwcGx5IGlmIHN1cHBvcnRlZC5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge3RoaXN9XG4gICAgICogICBUaGlzIG1hcmtlciBpbnN0YW5jZSBmb3IgbWV0aG9kIGNoYWluaW5nLlxuICAgICAqL1xuICAgIGFsdGVySWNvbihjaGFuZ2VzKSB7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdW51c2VkLXZhcnNcbiAgICAgIC8vIERlZmF1bHQgbWFwIG1hcmtlcnMgY2Fubm90IGJlIG1vZGlmaWVkLCBzbyBkb2VzIG5vdGhpbmcgaGVyZSwgYnV0XG4gICAgICAvLyBzaG91bGQgYmUgb3ZlcnJpZGRlbiBieSBzdWJjbGFzc2VzLlxuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmVzZXRzIHRoZSBtYXJrZXIgZGlzcGxheSBiYWNrIHRvIHRoZSBvcmlnaW5hbCBzZXR0aW5ncy5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge3RoaXN9XG4gICAgICogICBSZXR1cm5zIHRoaXMgbWFya2VyIGluc3RhbmNlIGZvciBtZXRob2QgY2hhaW5pbmcuXG4gICAgICovXG4gICAgcmVzZXRNYXJrZXIoKSB7XG4gICAgICBpZiAodGhpcy5fbWFya2VyKSB7XG4gICAgICAgIHRoaXMuX21hcmtlci5zZXRJY29uKHRoaXMuX2dldEljb24oKSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBBdHRhY2ggdGhpcyBtYXJrZXIgdG8gYmUgZGlzcGxheWVkIG9uIGEgbWFwLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtNYXBraXQuR01hcH0gbWFwXG4gICAgICogICBNYXAgdG8gYXR0YWNoIHRoZSBtYXJrZXIgdG8uXG4gICAgICpcbiAgICAgKiBAcmV0dXJuIHt0aGlzfVxuICAgICAqICAgVGhpcyBtYXJrZXIgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cbiAgICAgKi9cbiAgICBhdHRhY2gobWFwKSB7XG4gICAgICB0aGlzLl9tYXAgPSBtYXA7XG5cbiAgICAgIGlmICh0aGlzLl9tYXJrZXIgJiYgbWFwLl9nbWFwKSB7XG4gICAgICAgIHRoaXMuX21hcmtlci5zZXRNYXAobWFwLl9nbWFwKTtcblxuICAgICAgICBpZiAobWFwLm9tcykge1xuICAgICAgICAgIG1hcC5vbXMudHJhY2tNYXJrZXIodGhpcy5fbWFya2VyKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRGV0YWNoIHRoZSBtYXJrZXIgZnJvbSBpdHMgY3VycmVudCBtYXAgZGlzcGxheS5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge3RoaXN9XG4gICAgICogICBUaGlzIG1hcmtlciBpbnN0YW5jZSBmb3IgbWV0aG9kIGNoYWluaW5nLlxuICAgICAqL1xuICAgIGRldGFjaCgpIHtcbiAgICAgIGlmICh0aGlzLl9tYXJrZXIpIHtcbiAgICAgICAgaWYgKHRoaXMuX21hcC5vbXMpIHtcbiAgICAgICAgICB0aGlzLl9tYXAub21zLmZvcmdldE1hcmtlcih0aGlzLl9tYXJrZXIpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gUmVtb3ZlIGFsbCBldmVudHMgYW5kIGRpc2Fzc29jaWF0ZSB3aXRoIGFueSBtYXAgaW5zdGFuY2UuXG4gICAgICAgIGdvb2dsZS5tYXBzLmV2ZW50LmNsZWFySW5zdGFuY2VMaXN0ZW5lcnModGhpcy5fbWFya2VyKTtcbiAgICAgICAgdGhpcy5fbWFya2VyLnNldE1hcChudWxsKTtcbiAgICAgICAgZGVsZXRlIHRoaXMuX21hcDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQWRkIGNsaWNrIGV2ZW50IGxpc3RlbmVycyB0byB0aGUgbWFya2VyLiBJZiBHb29nbGUgbWFwcyBoYXZlIG5vdCBiZWVuXG4gICAgICogaW5pdGlhbGl6ZWQgeWV0LCB0aGUgbWFya2VyIG9iamVjdCB3aWxsIGFkZCB0aGUgbGlzdGVuZXIgd2hlbiB0aGUgbWFya2VyXG4gICAgICogaXMgaW5pdGlhbGl6ZWQuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gZXZlbnRcbiAgICAgKiAgIE5hbWUgb2YgdGhlIGV2ZW50IHRvIGFkZCB0aGUgbmV3IGV2ZW50IGxpc3RlbmVyIHRvLlxuICAgICAqIEBwYXJhbSB7ZnVuY3Rpb259IGxpc3RlbmVyXG4gICAgICogICBFdmVudCBsaXN0ZW5lciBmdW5jdGlvbiB0byBhZGQgdGhlIHRoZSBtYXJrZXIuXG4gICAgICpcbiAgICAgKiBAcmV0dXJuIHt0aGlzfVxuICAgICAqICAgVGhpcyBtYXJrZXIgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cbiAgICAgKi9cbiAgICBvbihldmVudCwgbGlzdGVuZXIpIHtcbiAgICAgIGNvbnN0IGRhdGEgPSB7IGxpc3RlbmVyLCBoYW5kbGU6IG51bGwgfTtcblxuICAgICAgaWYgKHRoaXMubGlzdGVuZXJzLmhhcyhldmVudCkpIHtcbiAgICAgICAgdGhpcy5saXN0ZW5lcnMuZ2V0KGV2ZW50KS5wdXNoKGRhdGEpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIHRoaXMubGlzdGVuZXJzLnNldChldmVudCwgW2RhdGFdKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuX21hcmtlcikge1xuICAgICAgICAvLyBXaGVuIE92ZXJsYXBwaW5nTWFya2VyU3BpZGVyZmllciBpcyBlbmFibGVkLCB0aGUgY2xpY2sgZXZlbnQgaXNcbiAgICAgICAgLy8gcmVwbGFjZWQgd2l0aCB0aGUgXCJzcGlkZXJfY2xpY2tcIiBldmVudC5cbiAgICAgICAgaWYgKHRoaXMuX21hcC5vbXMgJiYgZXZlbnQgPT09ICdjbGljaycpIHtcbiAgICAgICAgICBldmVudCA9ICdzcGlkZXJfY2xpY2snO1xuICAgICAgICB9XG5cbiAgICAgICAgZGF0YS5oYW5kbGUgPSB0aGlzLl9tYXJrZXIuYWRkTGlzdGVuZXIoZXZlbnQsIGxpc3RlbmVyKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmVtb3ZlIGNsaWNrIGV2ZW50IGxpc3RlbmVycyBmb3IgdGhpcyBtYXAgbWFya2VyLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGV2ZW50XG4gICAgICogICBOYW1lIG9mIHRoZVxuICAgICAqIEBwYXJhbSB7ZnVuY3Rpb249fSBsaXN0ZW5lclxuICAgICAqICAgSWYgcHJvdmlkZWQsIG9ubHkgcmVtb3ZlIHRoaXMgZXZlbnQgbGlzdGVuZXIsIG90aGVyd2lzZSwgcmVtb3ZlIGFsbFxuICAgICAqICAgY2xpY2sgZXZlbnRzIGZvciB0aGlzIG1hcmtlci5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge3RoaXN9XG4gICAgICogICBUaGlzIG1hcmtlciBpbnN0YW5jZSBmb3IgbWV0aG9kIGNoYWluaW5nLlxuICAgICAqL1xuICAgIG9mZihldmVudCwgbGlzdGVuZXIpIHtcbiAgICAgIC8vIFJlbW92ZSBhbGwgY2xpY2sgaGFuZGxlcnMgaWYgbm8gc3BlY2lmaWMgbGlzdGVuZXIgcGFzc2VkIGluLlxuICAgICAgaWYgKCFsaXN0ZW5lcikge1xuICAgICAgICB0aGlzLmxpc3RlbmVycy5kZWxldGUoZXZlbnQpO1xuXG4gICAgICAgIGlmICh0aGlzLl9tYXJrZXIpIHtcbiAgICAgICAgICAvLyBDaGVjayBmb3IgT3ZlcmxhcHBpbmdNYXJrZXJTcGlkZXJmaWVyIGFuZCBhZGp1c3QgdGhlIGV2ZW50IG5hbWUuXG4gICAgICAgICAgaWYgKHRoaXMuX21hcC5vbXMgJiYgZXZlbnQgPT09ICdjbGljaycpIHtcbiAgICAgICAgICAgIGV2ZW50ID0gJ3NwaWRlcl9jbGljayc7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgZ29vZ2xlLm1hcHMuZXZlbnQuY2xlYXJMaXN0ZW5lcnModGhpcy5fbWFya2VyLCBldmVudCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBjb25zdCBsaXN0ZW5lcnMgPSB0aGlzLmxpc3RlbmVycy5nZXQoZXZlbnQpIHx8IFtdO1xuXG4gICAgICAgIC8vIEdvIGh1bnRpbmcgZm9yIHRoZSBjYWxsYmFjayB0byByZW1vdmUuXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbGlzdGVuZXJzLmxlbmd0aDsgKytpKSB7XG4gICAgICAgICAgY29uc3QgZGF0YSA9IHRoaXMubGlzdGVuZXJzW2ldO1xuXG4gICAgICAgICAgaWYgKGRhdGEubGlzdGVuZXIgPT09IGxpc3RlbmVyIHx8IGRhdGEuaGFuZGxlID09PSBsaXN0ZW5lcikge1xuICAgICAgICAgICAgaWYgKGxpc3RlbmVycy5sZW5ndGggPD0gMSkge1xuICAgICAgICAgICAgICB0aGlzLmxpc3RlbmVycy5kZWxldGUoZXZlbnQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIGxpc3RlbmVycy5zcGxpY2UoaSwgMSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLl9tYXJrZXIgJiYgZGF0YS5oYW5kbGUpIHtcbiAgICAgICAgICAgICAgZ29vZ2xlLm1hcHMuZXZlbnQucmVtb3ZlTGlzdGVuZXIoZGF0YS5oYW5kbGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogSW1wbGVtZW50cyBhIEdvb2dsZSBtYXBzIG1hcmtlciBmcm9tIGEgU1ZHIHBhdGguXG4gICAqL1xuICBjbGFzcyBHTWFwU3ltYm9sTWFya2VyIGV4dGVuZHMgR01hcE1hcmtlciB7XG4gICAgY29uc3RydWN0b3IoaW5kZXgsIGxhdExuZywgbGFiZWwsIGNvbmZpZykge1xuICAgICAgc3VwZXIoaW5kZXgsIGxhdExuZywgbGFiZWwsIGNvbmZpZyk7XG5cbiAgICAgIHRoaXMubWFya2VyU3ltYm9sID0gY29uZmlnLnN5bWJvbDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdGRvY1xuICAgICAqL1xuICAgIF9nZXRMYWJlbCgpIHtcbiAgICAgIGlmICh0aGlzLmxhYmVsICYmIHRoaXMubGFiZWwubGVuZ3RoKSB7XG4gICAgICAgIHJldHVybiBzdXBlci5fZ2V0TGFiZWwoKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgdGV4dDogJ1xcdTIwMjInLFxuICAgICAgICBmb250RmFtaWx5OiAnYXJpYWwsIHNhbi1zZXJpZicsXG4gICAgICAgIGZvbnRTaXplOiAnNDBweCcsXG4gICAgICAgIGNvbG9yOiAnI2ZmZicsXG4gICAgICB9O1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBpbmhlcml0ZG9jXG4gICAgICovXG4gICAgX2dldEljb24oKSB7XG4gICAgICByZXR1cm4gdGhpcy5tYXJrZXJTeW1ib2w7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQGluaGVyaXRkb2NcbiAgICAgKi9cbiAgICBhbHRlckljb24oY2hhbmdlcykge1xuICAgICAgaWYgKHRoaXMuX21hcmtlcikge1xuICAgICAgICBjb25zdCBpY29uID0gdGhpcy5fbWFya2VyLmdldEljb24oKTtcbiAgICAgICAgT2JqZWN0LmFzc2lnbihpY29uLCBjaGFuZ2VzKTtcbiAgICAgICAgdGhpcy5fbWFya2VyLnNldEljb24oaWNvbik7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdGRvY1xuICAgICAqL1xuICAgIGdldCBpbm5lckhUTUwoKSB7XG4gICAgICBjb25zdCBpY29uID0gdGhpcy5fZ2V0SWNvbigpO1xuXG4gICAgICBsZXQgaHRtbCA9IGA8ZGl2IGNsYXNzPVwibWFwa2l0LW1hcmtlclwiIHN0eWxlPVwicG9zaXRpb246IHJlbGF0aXZlO1wiPlxuICAgICAgICA8c3ZnIHZpZXdCb3g9XCIwIDAgJHtpY29uLnNpemUud2lkdGh9ICR7aWNvbi5zaXplLmhlaWdodH1cIiBzdHlsZT1cImZpbGw6JHtpY29uLmZpbGxDb2xvcn07IHN0cm9rZToke2ljb24uc3Ryb2tlQ29sb3J9O3N0cm9rZS13aWR0aDoke2ljb24uc3Ryb2tlV2VpZ2h0fTtcIj5cbiAgICAgICAgICA8cGF0aCBkPVwiJHtpY29uLnBhdGh9XCIvPlxuICAgICAgICA8L3N2Zz5gO1xuXG4gICAgICAvLyBJbmNsdWRlIGEgbGFiZWwgZGlzcGxheSBpZiBhIGxhYmVsIGlzIHByb3ZpZGVkLlxuICAgICAgaWYgKHRoaXMubGFiZWwpIHtcbiAgICAgICAgY29uc3QgbGFiZWwgPSB0aGlzLl9nZXRMYWJlbCgpO1xuICAgICAgICBjb25zdCBzdHlsZXMgPSAncG9zaXRpb246IGFic29sdXRlOyB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTsnXG4gICAgICAgICAgKyBgdG9wOiAkeygxMDAgKiBpY29uLmxhYmVsT3JpZ2luLnkpIC8gaWNvbi5zaXplLmhlaWdodH0lOyBsZWZ0OiAkeygxMDAgKiBpY29uLmxhYmVsT3JpZ2luLngpIC8gaWNvbi5zaXplLndpZHRofSU7YFxuICAgICAgICAgICsgYGNvbG9yOiAke2xhYmVsLmNvbG9yfTsgZm9udC1zaXplOiAke2xhYmVsLmZvbnRTaXplfTtgO1xuXG4gICAgICAgIGh0bWwgKz0gYDxzcGFuIHN0eWxlPVwiJHtzdHlsZXN9XCI+JHt0aGlzLmxhYmVsfTwvc3Bhbj5gO1xuICAgICAgfVxuXG4gICAgICBodG1sICs9ICc8L2Rpdj4nO1xuICAgICAgcmV0dXJuIGh0bWw7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZSBsZXR0ZXIgbGFiZWxpbmcgZm9yIG1hcmtlcnMuXG4gICAqXG4gICAqIEBwYXJhbSB7aW50fSBpXG4gICAqICAgVGhlIG1hcmtlciBpbmRleCB0byBnZW5lcmF0ZSB0aGUgbGFiZWwgZm9yLlxuICAgKlxuICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAqICAgVGhlIGxldHRlcnMgY29ycmVsbGF0aW5nIHRvIHRoZSBtYXJrZXIgaW5kZXguXG4gICAqL1xuICBmdW5jdGlvbiBMZXR0ZXJNYXJrZXJMYWJlbHMoaSkge1xuICAgIGxldCBzdHIgPSAnJztcblxuICAgIHdoaWxlIChpID4gMjYpIHtcbiAgICAgIGNvbnN0IHIgPSBpICUgMjY7XG4gICAgICBpID0gKGkgLSByKSAvIDI2O1xuXG4gICAgICBpZiAocikge1xuICAgICAgICBzdHIgPSBTdHJpbmcuZnJvbUNoYXJDb2RlKDY0ICsgcikgKyBzdHI7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgc3RyID0gYFoke3N0cn1gO1xuICAgICAgICBpLS07IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tcGx1c3BsdXNcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gU3RyaW5nLmZyb21DaGFyQ29kZSg2NCArIGkpICsgc3RyO1xuICB9XG5cbiAgLyoqXG4gICAqIENvbnZlcnQgdGhlIG51bWVyaWMgbWFya2VyIGluZGV4IGludG8gYSBzdHJpbmcgdG8gZGlzcGxheSBhcyB0aGUgbGFiZWwuXG4gICAqXG4gICAqIEBwYXJhbSB7aW50fSBpXG4gICAqICAgVGhlIG1hcmtlciBpbmRleCB0byB0cmFuc2Zvcm0gaW50byB0aGUgbWFya2VyIGxhYmVsLlxuICAgKlxuICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAqICAgVGhlIHN0cmluZyB2YWx1ZSBvZiB0aGUgbWFya2VyIGluZGV4LlxuICAgKi9cbiAgZnVuY3Rpb24gTnVtYmVyTWFya2VyTGFiZWxzKGkpIHtcbiAgICByZXR1cm4gaS50b1N0cmluZygpO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgbGFiZWwgZ2VuZXJhdG9yIGZ1bmN0aW9uIHRvIHVzZSBiYXNlZCBvbiB0aGUgZGVzaXJlZCBsYWJlbCBzdHlsZXMuXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfG51bGx9IHN0eWxlXG4gICAqICAgVGhlIG5hbWUgb2YgdGhlIGxhYmVsIHN0eWxpbmcgdG8gZmV0Y2ggdGhlIGdlbmVyYXRvciBmdW5jdGlvbiBmb3IuXG4gICAqXG4gICAqIEByZXR1cm4ge2Z1bmN0aW9ufVxuICAgKiAgIEZ1bmN0aW9uIHdoaWNoIHRha2VzIHRoZSBtYXJrZXIgaW5kZXgsIGFuZCByZXR1cm5zIHRoZSBzdHJpbmcgbGFiZWwuXG4gICAqL1xuICBmdW5jdGlvbiBnZXRMYWJlbEdlbmVyYXRvcihzdHlsZSkge1xuICAgIHN3aXRjaCAoc3R5bGUgfHwgJ251bWJlcnMnKSB7XG4gICAgICBjYXNlICdsZXR0ZXJzJzpcbiAgICAgICAgcmV0dXJuIExldHRlck1hcmtlckxhYmVscztcblxuICAgICAgY2FzZSAnbnVtYmVycyc6XG4gICAgICAgIHJldHVybiBOdW1iZXJNYXJrZXJMYWJlbHM7XG5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHJldHVybiAoKCkgPT4gbnVsbCk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEdlbmVyYXRlIFN0YW5kYXJkIEdvb2dsZSBNYXBzIG1hcmtlciBmYWN0b3J5IG1ldGhvZCBiYXNlZCBvbiB0aGUgc2V0dGluZ3MuXG4gICAqXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBzZXR0aW5nc1xuICAgKiAgIFRoZSBtYXJrZXIgc2V0dGluZyB0aGUgcmVzdWx0aW5nIG1hcmtlciBmYWN0b3J5IHdpbGwgdXNlIHdoZW4gZ2VuZXJhdGluZ1xuICAgKiAgIG5ldyBtYXJrZXJzIGluc3RhbmNlcy5cbiAgICpcbiAgICogQHJldHVybiB7ZnVuY3Rpb259XG4gICAqICAgQSBtYXJrZXIgZmFjdG9yeSBmdW5jdGlvbiwgZm9yIGNyZWF0aW5nIG1hcCBtYXJrZXIgaW5zdGFuY2VzLlxuICAgKi9cbiAgTWFwa2l0Lm1hcmtlci5nbWFwTWFya2VyID0gZnVuY3Rpb24gY3JlYXRlR21hcE1hcmtlcihzZXR0aW5ncykge1xuICAgIGNvbnN0IG9mZnNldCA9IHNldHRpbmdzLm9mZnNldCB8fCAwO1xuICAgIGNvbnN0IGxhYmVsR2VuID0gZ2V0TGFiZWxHZW5lcmF0b3Ioc2V0dGluZ3MubGFiZWwuc3R5bGUpO1xuXG4gICAgbGV0IGNvdW50ID0gc2V0dGluZ3Muc3RhcnQgfHwgMTtcbiAgICBsZXQgTWFya2VyQ2xhc3MgPSBHTWFwTWFya2VyO1xuXG4gICAgZGVsZXRlIHNldHRpbmdzLmxhYmVsLnN0eWxlO1xuICAgIGNvbnN0IGNvbmZpZyA9IHtcbiAgICAgIGxhYmVsOiB7XG4gICAgICAgIGNvbG9yOiAnIzAwMCcsXG4gICAgICAgIGZvbnRTaXplOiAnMTZweCcsXG4gICAgICAgIC4uLnNldHRpbmdzLmxhYmVsLFxuICAgICAgfSxcbiAgICAgIHN5bWJvbDogbnVsbCxcbiAgICB9O1xuXG4gICAgLy8gU2V0dXAgU1ZHIHBhdGggc2V0dGluZ3MgaWYgU3ltYm9sIGNvbmZpZ3VyYXRpb25zIGFyZSBwcmVzZW50LlxuICAgIGlmIChzZXR0aW5ncy5zeW1ib2wpIHtcbiAgICAgIE1hcmtlckNsYXNzID0gR01hcFN5bWJvbE1hcmtlcjtcblxuICAgICAgLy8gRW5zdXJlIHNvbWUgb2YgdGhlIG1hcmtlciBzYW5lIGRlZmF1bHRzLlxuICAgICAgY29uZmlnLnN5bWJvbCA9IHtcbiAgICAgICAgZmlsbE9wYWNpdHk6IDEsXG4gICAgICAgIGZpbGxDb2xvcjogJyNlYTAwMDAnLFxuICAgICAgICBzdHJva2VDb2xvcjogJyNmZmYnLFxuICAgICAgICBzdHJva2VXZWlnaHQ6IDEsXG4gICAgICAgIC4uLnNldHRpbmdzLnN5bWJvbCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgLy8gR2VuZXJhdG9yIGZ1bmN0aW9uIGZvciBjcmVhdGluZyBtYXJrZXJzLlxuICAgIHJldHVybiBmdW5jdGlvbiBjcmVhdGVNYXJrZXIoZGF0YSkge1xuICAgICAgY29uc3QgaW5kZXggPSAoZGF0YS5pbmRleCB8fCBjb3VudCsrKSArIG9mZnNldDsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby1wbHVzcGx1c1xuXG4gICAgICByZXR1cm4gbmV3IE1hcmtlckNsYXNzKGluZGV4LCBkYXRhLmxhdExuZywgbGFiZWxHZW4oaW5kZXgpLCBjb25maWcpO1xuICAgIH07XG4gIH07XG59KShEcnVwYWwpO1xuIl0sIm1hcHBpbmdzIjoiOztBQUFBLENBQUMsQ0FBQztFQUFFQTtBQUFPLENBQUMsS0FBSztFQUNmO0FBQ0Y7QUFDQTtFQUNFLE1BQU1DLFVBQVUsQ0FBQztJQUNmQyxXQUFXLENBQUNDLEtBQUssRUFBRUMsTUFBTSxFQUFFQyxLQUFLLEVBQUVDLE1BQU0sRUFBRTtNQUN4QyxJQUFJLENBQUNELEtBQUssR0FBR0EsS0FBSyxJQUFJLElBQUk7TUFDMUIsSUFBSSxDQUFDRixLQUFLLEdBQUdBLEtBQUs7TUFDbEIsSUFBSSxDQUFDQyxNQUFNLEdBQUdBLE1BQU07TUFFcEIsSUFBSSxDQUFDRyxXQUFXLEdBQUdELE1BQU0sQ0FBQ0QsS0FBSztNQUMvQixJQUFJLENBQUNHLFlBQVksR0FBR0YsTUFBTSxDQUFDRyxNQUFNOztNQUVqQztNQUNBLElBQUksQ0FBQ0MsU0FBUyxHQUFHLElBQUlDLEdBQUcsRUFBRTtJQUM1Qjs7SUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJQyxJQUFJLENBQUNDLEdBQUcsRUFBRTtNQUNSLElBQUksQ0FBQ0MsSUFBSSxHQUFHRCxHQUFHO01BQ2YsSUFBSSxDQUFDRSxPQUFPLEdBQUcsSUFBSUMsTUFBTSxDQUFDQyxJQUFJLENBQUNDLE1BQU0sQ0FBQztRQUNwQ0wsR0FBRyxFQUFFQSxHQUFHLENBQUNNLEtBQUs7UUFDZEMsUUFBUSxFQUFFLElBQUksQ0FBQ0EsUUFBUTtRQUN2QkMsTUFBTSxFQUFFLElBQUksQ0FBQ2xCLEtBQUs7UUFDbEJtQixJQUFJLEVBQUUsSUFBSSxDQUFDQyxRQUFRLEVBQUU7UUFDckJsQixLQUFLLEVBQUUsSUFBSSxDQUFDbUIsU0FBUyxFQUFFO1FBQ3ZCQyxVQUFVLEVBQUU7TUFDZCxDQUFDLENBQUM7O01BRUY7TUFDQSxJQUFJWixHQUFHLENBQUNhLEdBQUcsRUFBRTtRQUNYYixHQUFHLENBQUNhLEdBQUcsQ0FBQ0MsV0FBVyxDQUFDLElBQUksQ0FBQ1osT0FBTyxDQUFDO01BQ25DOztNQUVBO01BQ0E7TUFDQSxJQUFJLENBQUNMLFNBQVMsQ0FBQ2tCLE9BQU8sQ0FBQyxDQUFDQyxLQUFLLEVBQUVDLEtBQUssS0FBSztRQUN2QyxJQUFJQSxLQUFLLEtBQUssT0FBTyxJQUFJLElBQUksQ0FBQ2hCLElBQUksQ0FBQ1ksR0FBRyxFQUFFO1VBQ3RDSSxLQUFLLEdBQUcsY0FBYztRQUN4QjtRQUVBRCxLQUFLLENBQUNELE9BQU8sQ0FBRUcsSUFBSSxJQUFLO1VBQ3RCQSxJQUFJLENBQUNDLE1BQU0sR0FBRyxJQUFJLENBQUNqQixPQUFPLENBQUNrQixXQUFXLENBQUNILEtBQUssRUFBRUMsSUFBSSxDQUFDRyxRQUFRLENBQUM7UUFDOUQsQ0FBQyxDQUFDO01BQ0osQ0FBQyxDQUFDO0lBQ0o7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSSxJQUFJZCxRQUFRLEdBQUc7TUFDYixPQUFPLElBQUksQ0FBQ2hCLE1BQU07SUFDcEI7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSSxJQUFJZ0IsUUFBUSxDQUFDaEIsTUFBTSxFQUFFO01BQ25CLE1BQU0rQixNQUFNLEdBQUcsSUFBSSxDQUFDL0IsTUFBTTtNQUMxQixJQUFJLENBQUNBLE1BQU0sR0FBR0EsTUFBTTtNQUVwQixJQUFJLElBQUksQ0FBQ1csT0FBTyxFQUFFO1FBQ2hCLElBQUksQ0FBQ0EsT0FBTyxDQUFDcUIsV0FBVyxDQUFDaEMsTUFBTSxDQUFDO1FBQ2hDLElBQUksQ0FBQ1UsSUFBSSxDQUFDdUIscUJBQXFCLENBQUNGLE1BQU0sRUFBRS9CLE1BQU0sQ0FBQztNQUNqRDtJQUNGOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0ksSUFBSWtDLFNBQVMsR0FBRztNQUNkLE9BQU8sSUFBSSxDQUFDakMsS0FBSyxHQUFJLDhCQUE2QixJQUFJLENBQUNBLEtBQU0sUUFBTyxHQUFHLElBQUk7SUFDN0U7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSW1CLFNBQVMsR0FBRztNQUNWLE9BQU8sSUFBSSxDQUFDbkIsS0FBSyxHQUFHO1FBQ2xCLEdBQUcsSUFBSSxDQUFDRSxXQUFXO1FBQ25CZ0MsSUFBSSxFQUFFLElBQUksQ0FBQ2xDO01BQ2IsQ0FBQyxHQUFHLElBQUk7SUFDVjs7SUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSWtCLFFBQVEsR0FBRztNQUFFO01BQ1gsT0FBTyxJQUFJO0lBQ2I7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lpQixPQUFPLEdBQUc7TUFDUixJQUFJLElBQUksQ0FBQ3pCLE9BQU8sRUFBRTtRQUNoQixJQUFJLENBQUNBLE9BQU8sQ0FBQzBCLFNBQVMsQ0FBQyxLQUFLLENBQUM7TUFDL0I7TUFDQSxPQUFPLElBQUk7SUFDYjs7SUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJQSxTQUFTLENBQUNDLENBQUMsRUFBRTtNQUNYLElBQUksSUFBSSxDQUFDM0IsT0FBTyxFQUFFO1FBQ2hCMkIsQ0FBQyxHQUFHQSxDQUFDLElBQUksSUFBSSxDQUFDdkMsS0FBSztRQUNuQixJQUFJLENBQUNZLE9BQU8sQ0FBQzBCLFNBQVMsQ0FBQ0MsQ0FBQyxDQUFDO01BQzNCO01BQ0EsT0FBTyxJQUFJO0lBQ2I7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lDLFNBQVMsQ0FBQ0MsT0FBTyxFQUFFO01BQUU7TUFDbkI7TUFDQTtNQUNBLE9BQU8sSUFBSTtJQUNiOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJQyxXQUFXLEdBQUc7TUFDWixJQUFJLElBQUksQ0FBQzlCLE9BQU8sRUFBRTtRQUNoQixJQUFJLENBQUNBLE9BQU8sQ0FBQytCLE9BQU8sQ0FBQyxJQUFJLENBQUN2QixRQUFRLEVBQUUsQ0FBQztNQUN2QztNQUNBLE9BQU8sSUFBSTtJQUNiOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJd0IsTUFBTSxDQUFDbEMsR0FBRyxFQUFFO01BQ1YsSUFBSSxDQUFDQyxJQUFJLEdBQUdELEdBQUc7TUFFZixJQUFJLElBQUksQ0FBQ0UsT0FBTyxJQUFJRixHQUFHLENBQUNNLEtBQUssRUFBRTtRQUM3QixJQUFJLENBQUNKLE9BQU8sQ0FBQ2lDLE1BQU0sQ0FBQ25DLEdBQUcsQ0FBQ00sS0FBSyxDQUFDO1FBRTlCLElBQUlOLEdBQUcsQ0FBQ2EsR0FBRyxFQUFFO1VBQ1hiLEdBQUcsQ0FBQ2EsR0FBRyxDQUFDQyxXQUFXLENBQUMsSUFBSSxDQUFDWixPQUFPLENBQUM7UUFDbkM7TUFDRjtNQUNBLE9BQU8sSUFBSTtJQUNiOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJa0MsTUFBTSxHQUFHO01BQ1AsSUFBSSxJQUFJLENBQUNsQyxPQUFPLEVBQUU7UUFDaEIsSUFBSSxJQUFJLENBQUNELElBQUksQ0FBQ1ksR0FBRyxFQUFFO1VBQ2pCLElBQUksQ0FBQ1osSUFBSSxDQUFDWSxHQUFHLENBQUN3QixZQUFZLENBQUMsSUFBSSxDQUFDbkMsT0FBTyxDQUFDO1FBQzFDOztRQUVBO1FBQ0FDLE1BQU0sQ0FBQ0MsSUFBSSxDQUFDYSxLQUFLLENBQUNxQixzQkFBc0IsQ0FBQyxJQUFJLENBQUNwQyxPQUFPLENBQUM7UUFDdEQsSUFBSSxDQUFDQSxPQUFPLENBQUNpQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ3pCLE9BQU8sSUFBSSxDQUFDbEMsSUFBSTtNQUNsQjtNQUVBLE9BQU8sSUFBSTtJQUNiOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lzQyxFQUFFLENBQUN0QixLQUFLLEVBQUVJLFFBQVEsRUFBRTtNQUNsQixNQUFNSCxJQUFJLEdBQUc7UUFBRUcsUUFBUTtRQUFFRixNQUFNLEVBQUU7TUFBSyxDQUFDO01BRXZDLElBQUksSUFBSSxDQUFDdEIsU0FBUyxDQUFDMkMsR0FBRyxDQUFDdkIsS0FBSyxDQUFDLEVBQUU7UUFDN0IsSUFBSSxDQUFDcEIsU0FBUyxDQUFDNEMsR0FBRyxDQUFDeEIsS0FBSyxDQUFDLENBQUN5QixJQUFJLENBQUN4QixJQUFJLENBQUM7TUFDdEMsQ0FBQyxNQUNJO1FBQ0gsSUFBSSxDQUFDckIsU0FBUyxDQUFDOEMsR0FBRyxDQUFDMUIsS0FBSyxFQUFFLENBQUNDLElBQUksQ0FBQyxDQUFDO01BQ25DO01BRUEsSUFBSSxJQUFJLENBQUNoQixPQUFPLEVBQUU7UUFDaEI7UUFDQTtRQUNBLElBQUksSUFBSSxDQUFDRCxJQUFJLENBQUNZLEdBQUcsSUFBSUksS0FBSyxLQUFLLE9BQU8sRUFBRTtVQUN0Q0EsS0FBSyxHQUFHLGNBQWM7UUFDeEI7UUFFQUMsSUFBSSxDQUFDQyxNQUFNLEdBQUcsSUFBSSxDQUFDakIsT0FBTyxDQUFDa0IsV0FBVyxDQUFDSCxLQUFLLEVBQUVJLFFBQVEsQ0FBQztNQUN6RDtNQUVBLE9BQU8sSUFBSTtJQUNiOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJdUIsR0FBRyxDQUFDM0IsS0FBSyxFQUFFSSxRQUFRLEVBQUU7TUFDbkI7TUFDQSxJQUFJLENBQUNBLFFBQVEsRUFBRTtRQUNiLElBQUksQ0FBQ3hCLFNBQVMsQ0FBQ2dELE1BQU0sQ0FBQzVCLEtBQUssQ0FBQztRQUU1QixJQUFJLElBQUksQ0FBQ2YsT0FBTyxFQUFFO1VBQ2hCO1VBQ0EsSUFBSSxJQUFJLENBQUNELElBQUksQ0FBQ1ksR0FBRyxJQUFJSSxLQUFLLEtBQUssT0FBTyxFQUFFO1lBQ3RDQSxLQUFLLEdBQUcsY0FBYztVQUN4QjtVQUVBZCxNQUFNLENBQUNDLElBQUksQ0FBQ2EsS0FBSyxDQUFDNkIsY0FBYyxDQUFDLElBQUksQ0FBQzVDLE9BQU8sRUFBRWUsS0FBSyxDQUFDO1FBQ3ZEO01BQ0YsQ0FBQyxNQUNJO1FBQ0gsTUFBTXBCLFNBQVMsR0FBRyxJQUFJLENBQUNBLFNBQVMsQ0FBQzRDLEdBQUcsQ0FBQ3hCLEtBQUssQ0FBQyxJQUFJLEVBQUU7O1FBRWpEO1FBQ0EsS0FBSyxJQUFJOEIsQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHbEQsU0FBUyxDQUFDbUQsTUFBTSxFQUFFLEVBQUVELENBQUMsRUFBRTtVQUN6QyxNQUFNN0IsSUFBSSxHQUFHLElBQUksQ0FBQ3JCLFNBQVMsQ0FBQ2tELENBQUMsQ0FBQztVQUU5QixJQUFJN0IsSUFBSSxDQUFDRyxRQUFRLEtBQUtBLFFBQVEsSUFBSUgsSUFBSSxDQUFDQyxNQUFNLEtBQUtFLFFBQVEsRUFBRTtZQUMxRCxJQUFJeEIsU0FBUyxDQUFDbUQsTUFBTSxJQUFJLENBQUMsRUFBRTtjQUN6QixJQUFJLENBQUNuRCxTQUFTLENBQUNnRCxNQUFNLENBQUM1QixLQUFLLENBQUM7WUFDOUIsQ0FBQyxNQUNJO2NBQ0hwQixTQUFTLENBQUNvRCxNQUFNLENBQUNGLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDeEI7WUFFQSxJQUFJLElBQUksQ0FBQzdDLE9BQU8sSUFBSWdCLElBQUksQ0FBQ0MsTUFBTSxFQUFFO2NBQy9CaEIsTUFBTSxDQUFDQyxJQUFJLENBQUNhLEtBQUssQ0FBQ2lDLGNBQWMsQ0FBQ2hDLElBQUksQ0FBQ0MsTUFBTSxDQUFDO1lBQy9DO1lBQ0E7VUFDRjtRQUNGO01BQ0Y7TUFDQSxPQUFPLElBQUk7SUFDYjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtFQUNFLE1BQU1nQyxnQkFBZ0IsU0FBUy9ELFVBQVUsQ0FBQztJQUN4Q0MsV0FBVyxDQUFDQyxLQUFLLEVBQUVDLE1BQU0sRUFBRUMsS0FBSyxFQUFFQyxNQUFNLEVBQUU7TUFDeEMsS0FBSyxDQUFDSCxLQUFLLEVBQUVDLE1BQU0sRUFBRUMsS0FBSyxFQUFFQyxNQUFNLENBQUM7TUFFbkMsSUFBSSxDQUFDRSxZQUFZLEdBQUdGLE1BQU0sQ0FBQ0csTUFBTTtJQUNuQzs7SUFFQTtBQUNKO0FBQ0E7SUFDSWUsU0FBUyxHQUFHO01BQ1YsSUFBSSxJQUFJLENBQUNuQixLQUFLLElBQUksSUFBSSxDQUFDQSxLQUFLLENBQUN3RCxNQUFNLEVBQUU7UUFDbkMsT0FBTyxLQUFLLENBQUNyQyxTQUFTLEVBQUU7TUFDMUI7TUFFQSxPQUFPO1FBQ0xlLElBQUksRUFBRSxRQUFRO1FBQ2QwQixVQUFVLEVBQUUsa0JBQWtCO1FBQzlCQyxRQUFRLEVBQUUsTUFBTTtRQUNoQkMsS0FBSyxFQUFFO01BQ1QsQ0FBQztJQUNIOztJQUVBO0FBQ0o7QUFDQTtJQUNJNUMsUUFBUSxHQUFHO01BQ1QsT0FBTyxJQUFJLENBQUNmLFlBQVk7SUFDMUI7O0lBRUE7QUFDSjtBQUNBO0lBQ0ltQyxTQUFTLENBQUNDLE9BQU8sRUFBRTtNQUNqQixJQUFJLElBQUksQ0FBQzdCLE9BQU8sRUFBRTtRQUNoQixNQUFNTyxJQUFJLEdBQUcsSUFBSSxDQUFDUCxPQUFPLENBQUNxRCxPQUFPLEVBQUU7UUFDbkNDLE1BQU0sQ0FBQ0MsTUFBTSxDQUFDaEQsSUFBSSxFQUFFc0IsT0FBTyxDQUFDO1FBQzVCLElBQUksQ0FBQzdCLE9BQU8sQ0FBQytCLE9BQU8sQ0FBQ3hCLElBQUksQ0FBQztNQUM1QjtNQUNBLE9BQU8sSUFBSTtJQUNiOztJQUVBO0FBQ0o7QUFDQTtJQUNJLElBQUlnQixTQUFTLEdBQUc7TUFDZCxNQUFNaEIsSUFBSSxHQUFHLElBQUksQ0FBQ0MsUUFBUSxFQUFFO01BRTVCLElBQUlnRCxJQUFJLEdBQUk7QUFDbEIsNEJBQTRCakQsSUFBSSxDQUFDa0QsSUFBSSxDQUFDQyxLQUFNLElBQUduRCxJQUFJLENBQUNrRCxJQUFJLENBQUNFLE1BQU8saUJBQWdCcEQsSUFBSSxDQUFDcUQsU0FBVSxZQUFXckQsSUFBSSxDQUFDc0QsV0FBWSxpQkFBZ0J0RCxJQUFJLENBQUN1RCxZQUFhO0FBQzdKLHFCQUFxQnZELElBQUksQ0FBQ3dELElBQUs7QUFDL0IsZUFBZTs7TUFFVDtNQUNBLElBQUksSUFBSSxDQUFDekUsS0FBSyxFQUFFO1FBQ2QsTUFBTUEsS0FBSyxHQUFHLElBQUksQ0FBQ21CLFNBQVMsRUFBRTtRQUM5QixNQUFNdUQsTUFBTSxHQUFHLHVEQUF1RCxHQUNqRSxRQUFRLEdBQUcsR0FBR3pELElBQUksQ0FBQzBELFdBQVcsQ0FBQ0MsQ0FBQyxHQUFJM0QsSUFBSSxDQUFDa0QsSUFBSSxDQUFDRSxNQUFPLFlBQVksR0FBRyxHQUFHcEQsSUFBSSxDQUFDMEQsV0FBVyxDQUFDRSxDQUFDLEdBQUk1RCxJQUFJLENBQUNrRCxJQUFJLENBQUNDLEtBQU0sSUFBRyxHQUNoSCxVQUFTcEUsS0FBSyxDQUFDOEQsS0FBTSxnQkFBZTlELEtBQUssQ0FBQzZELFFBQVMsR0FBRTtRQUUxREssSUFBSSxJQUFLLGdCQUFlUSxNQUFPLEtBQUksSUFBSSxDQUFDMUUsS0FBTSxTQUFRO01BQ3hEO01BRUFrRSxJQUFJLElBQUksUUFBUTtNQUNoQixPQUFPQSxJQUFJO0lBQ2I7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTWSxrQkFBa0IsQ0FBQ3ZCLENBQUMsRUFBRTtJQUM3QixJQUFJd0IsR0FBRyxHQUFHLEVBQUU7SUFFWixPQUFPeEIsQ0FBQyxHQUFHLEVBQUUsRUFBRTtNQUNiLE1BQU15QixDQUFDLEdBQUd6QixDQUFDLEdBQUcsRUFBRTtNQUNoQkEsQ0FBQyxHQUFHLENBQUNBLENBQUMsR0FBR3lCLENBQUMsSUFBSSxFQUFFO01BRWhCLElBQUlBLENBQUMsRUFBRTtRQUNMRCxHQUFHLEdBQUdFLE1BQU0sQ0FBQ0MsWUFBWSxDQUFDLEVBQUUsR0FBR0YsQ0FBQyxDQUFDLEdBQUdELEdBQUc7TUFDekMsQ0FBQyxNQUNJO1FBQ0hBLEdBQUcsR0FBSSxJQUFHQSxHQUFJLEVBQUM7UUFDZnhCLENBQUMsRUFBRSxDQUFDLENBQUM7TUFDUDtJQUNGOztJQUVBLE9BQU8wQixNQUFNLENBQUNDLFlBQVksQ0FBQyxFQUFFLEdBQUczQixDQUFDLENBQUMsR0FBR3dCLEdBQUc7RUFDMUM7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU0ksa0JBQWtCLENBQUM1QixDQUFDLEVBQUU7SUFDN0IsT0FBT0EsQ0FBQyxDQUFDNkIsUUFBUSxFQUFFO0VBQ3JCOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNDLGlCQUFpQixDQUFDQyxLQUFLLEVBQUU7SUFDaEMsUUFBUUEsS0FBSyxJQUFJLFNBQVM7TUFDeEIsS0FBSyxTQUFTO1FBQ1osT0FBT1Isa0JBQWtCO01BRTNCLEtBQUssU0FBUztRQUNaLE9BQU9LLGtCQUFrQjtNQUUzQjtRQUNFLE9BQVEsTUFBTSxJQUFJO0lBQUU7RUFFMUI7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRXhGLE1BQU0sQ0FBQzRGLE1BQU0sQ0FBQ0MsVUFBVSxHQUFHLFNBQVNDLGdCQUFnQixDQUFDQyxRQUFRLEVBQUU7SUFDN0QsTUFBTUMsTUFBTSxHQUFHRCxRQUFRLENBQUNDLE1BQU0sSUFBSSxDQUFDO0lBQ25DLE1BQU1DLFFBQVEsR0FBR1AsaUJBQWlCLENBQUNLLFFBQVEsQ0FBQzFGLEtBQUssQ0FBQ3NGLEtBQUssQ0FBQztJQUV4RCxJQUFJTyxLQUFLLEdBQUdILFFBQVEsQ0FBQ0ksS0FBSyxJQUFJLENBQUM7SUFDL0IsSUFBSUMsV0FBVyxHQUFHbkcsVUFBVTtJQUU1QixPQUFPOEYsUUFBUSxDQUFDMUYsS0FBSyxDQUFDc0YsS0FBSztJQUMzQixNQUFNckYsTUFBTSxHQUFHO01BQ2JELEtBQUssRUFBRTtRQUNMOEQsS0FBSyxFQUFFLE1BQU07UUFDYkQsUUFBUSxFQUFFLE1BQU07UUFDaEIsR0FBRzZCLFFBQVEsQ0FBQzFGO01BQ2QsQ0FBQztNQUNESSxNQUFNLEVBQUU7SUFDVixDQUFDOztJQUVEO0lBQ0EsSUFBSXNGLFFBQVEsQ0FBQ3RGLE1BQU0sRUFBRTtNQUNuQjJGLFdBQVcsR0FBR3BDLGdCQUFnQjs7TUFFOUI7TUFDQTFELE1BQU0sQ0FBQ0csTUFBTSxHQUFHO1FBQ2Q0RixXQUFXLEVBQUUsQ0FBQztRQUNkMUIsU0FBUyxFQUFFLFNBQVM7UUFDcEJDLFdBQVcsRUFBRSxNQUFNO1FBQ25CQyxZQUFZLEVBQUUsQ0FBQztRQUNmLEdBQUdrQixRQUFRLENBQUN0RjtNQUNkLENBQUM7SUFDSDs7SUFFQTtJQUNBLE9BQU8sU0FBUzZGLFlBQVksQ0FBQ3ZFLElBQUksRUFBRTtNQUNqQyxNQUFNNUIsS0FBSyxHQUFHLENBQUM0QixJQUFJLENBQUM1QixLQUFLLElBQUkrRixLQUFLLEVBQUUsSUFBSUYsTUFBTSxDQUFDLENBQUM7O01BRWhELE9BQU8sSUFBSUksV0FBVyxDQUFDakcsS0FBSyxFQUFFNEIsSUFBSSxDQUFDM0IsTUFBTSxFQUFFNkYsUUFBUSxDQUFDOUYsS0FBSyxDQUFDLEVBQUVHLE1BQU0sQ0FBQztJQUNyRSxDQUFDO0VBQ0gsQ0FBQztBQUNILENBQUMsRUFBRWlHLE1BQU0sQ0FBQyJ9
