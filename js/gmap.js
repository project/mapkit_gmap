"use strict";

(({
  Toolshed: ts,
  Mapkit,
  debounce
}, loader) => {
  /**
   * Create a Mapkit wrapper for supporting Google Maps.
   */
  class GMap {
    constructor(el, settings, markerBuilders) {
      // Internal Google maps instance, should always start as NULL and
      // get assigned during the map initialization.
      this._gmap = null;
      this.el = el;
      this.markers = new Map();
      this.markerType = settings.defaultMarkerSet;
      this.markerBuilders = markerBuilders;

      // Transfer settings, while ensuring that we have sane defaults.
      this.center = settings.center;
      this.fitBounds = settings.fitBounds || false;
      this.spiderfyOptions = settings.markerSpiderfy;
      this.mapOptions = {
        clickableIcons: false,
        center: settings.center || {
          lat: 0,
          lng: 0
        },
        zoom: settings.zoom || 11,
        minZoom: settings.minZoom || 4,
        maxZoom: settings.maxZoom || 17,
        mapTypeId: settings.defaultLayer || 'roadmap',
        mapTypeControlOptions: {
          mapTypeIds: settings.layers || null
        },
        // Don't display "Points of Interest" as they can clutter a map.
        styles: [{
          featureType: 'poi',
          elementType: 'labels',
          stylers: [{
            visibility: 'off'
          }]
        }]
      };

      // Bounds cannot be initialized until Google maps API has been loaded.
      this.bounds = null;

      // Create a delayed callback to refresh the map bounds. This small delay
      // allows the map API to catch-up and consolidate quick viewport changes.
      this.refreshBounds = debounce(() => {
        if (this._gmap) {
          if (this.markers.size > 1) {
            this._gmap.fitBounds(this.bounds);
          } else if (this.markers.size === 1) {
            this._gmap.setCenter(this.bounds.getCenter());
            this._gmap.setZoom(this.mapOptions.zoom);
          }
        }
      }, 100);

      // Keep track of the map event listeners, keyed by event name.
      this.listeners = new Map();
    }

    /**
     * Initialize the actual map when Google Maps API has been loaded.
     *
     * @param {google.maps.Map=} gmap
     *   A Google maps instance if reusing an existing map. Otherwise a new map
     *   is created.
     */
    init(gmap = null) {
      if (gmap) {
        gmap.setOptions(this.mapOptions);
        this._gmap = gmap;
      } else {
        const wrap = new ts.Element('div', {
          style: {
            height: '100%'
          }
        });
        this._gmap = new google.maps.Map(wrap.el, this.mapOptions);
      }

      // Attach the map instance to the current Mapkit.GMap wrapper.
      this.el.appendChild(this._gmap.getDiv());
      this.bounds = new google.maps.LatLngBounds();
      if (this.spiderfyOptions && window.OverlappingMarkerSpiderfier) {
        this.oms = new window.OverlappingMarkerSpiderfier(this._gmap, this.spiderfyOptions);
      }

      // If any markers were added before Google maps was ready then build the
      // markers now that the Google API has been loaded.
      if (this.markers.size) {
        this.markers.forEach(m => {
          m.init(this);
          this.bounds.extend(m.position);
        });
      }
      if (this.fitBounds) {
        const boundsListener = google.maps.event.addListener(this._gmap, 'bounds_changed', () => {
          if (this.el.clientWidth > 0) {
            this.refreshBounds();
            google.maps.event.removeListener(boundsListener);
          }
        });

        // When using bounds fitting, it is possible for Google maps to go
        // beyond our zoom limits.
        if (this.mapOptions.minZoom || this.mapOptions.maxZoom) {
          google.maps.event.addListener(this._gmap, 'zoom_changed', () => {
            const zoom = this._gmap.getZoom();
            if (this.mapOptions.minZoom && zoom < this.mapOptions.minZoom) {
              this._gmap.setZoom(this.mapOptions.minZoom);
            } else if (this.mapOptions.maxZoom && zoom > this.mapOptions.maxZoom) {
              this._gmap.setZoom(this.mapOptions.maxZoom);
            }
          });
        }
      }

      // Register any click listeners which were already registered but
      // where waiting for the map to get initialized.
      this.listeners.forEach((items, event) => {
        // Init is a custom Mapkit event, and those listeners are run
        // immediately run since this is the map initialization.
        if (event === 'init') {
          items.forEach(data => data.listener(this));
        } else if (event !== 'destroy') {
          items.forEach(data => {
            data.handle = this._gmap.addListener(event, data.listener);
          });
        }
      });

      // Init has been handled, remove this set of event listeners.
      this.listeners.delete('init');
    }

    /**
     * Get the map center. If map has not been initialized yet, this value
     * might not be the correct values for where the built map will actually
     * be centered. One case is if the map is set to fit bounds but marker
     * bounds are not calculated yet.
     *
     * @return {Object}
     *   Returns a JSON object with "lat" and "lng" values for the current
     *   map set center position.
     */
    getCenter() {
      if (this._gmap) {
        const c = this._gmap.getCenter();
        return {
          lat: c.lat,
          lng: c.lng
        };
      }

      // If map has not been initialized, that return the configured center
      // point. This will be different from a bounds center, or the current
      // map center position.
      return this.center;
    }

    /**
     * Recalculate the bounds of the marker.
     */
    calcBounds() {
      this.bounds = new google.maps.LatLngBounds();
      this.markers.forEach(m => {
        this.bounds.extend(m.position);
      });
    }

    /**
     * A marker position has either been removed or moved.
     *
     * @param {LatLngLiteral} oldLatLng
     *   The original position of the marker.
     * @param {LatLngLiteral=} latLng
     *   If marker has been removed and does not have a new position this should
     *   be left empty.
     */
    markerPositionChanged(oldLatLng, latLng) {
      let refresh = false;
      if (latLng && !this.bounds.contains(latLng)) {
        this.bounds.extend(latLng);
        refresh = true;
      }

      // Bounds only change if the marker location coincides with one of
      // the edges of the bounds.
      const ne = this.bounds.getNorthEast();
      const sw = this.bounds.getSouthWest();
      if ((ne.lat() === oldLatLng.lat || sw.lat() === oldLatLng.lat) && (ne.lng() === oldLatLng.lng || sw.lng() === oldLatLng.lng)) {
        this.calcBounds();
        refresh = true;
      }

      // The bounds were changed in some way, check if map needs to be refocused.
      if (refresh) {
        if (this.markers.size <= 1) {
          const c = this.markers.size ? this.bounds.getCenter() : this.center;
          this.setCenter(c);
        } else if (this.fitBounds) {
          this.refreshBounds();
        }
      }
    }

    /**
     * Open an info window for this map at the given marker location.
     *
     * @param {Mapkit.Marker} marker
     *   A Mapkit marker instance, which wraps a Google maps marker object.
     * @param {string|HTMLElement} content
     *   Content to populate the info window.
     */
    openMarkerPopup(marker, content) {
      if (!this.infoWin) {
        this.infoWin = new google.maps.InfoWindow();
      }
      this.infoWin.setContent(content);
      this.infoWin.open({
        map: this._gmap,
        anchor: marker._marker
      });
    }

    /**
     * Set the map center location.
     *
     * @param {Object|google.maps.LatLng} center
     *   LatLng coordinate to center the map on.
     */
    setCenter(center) {
      this.center = center;
      if (this._gmap) {
        this._gmap.setCenter(center);
      }
    }

    /**
     * Add a new map marker based on the marker definition.
     *
     * @param {Object} data
     *   The marker definition, which should at minimum have a "latLng" value.
     * @param {bool} updateFit
     *   Should the bounds be adjusted when this marker is added. This ensures
     *   that newly added map markers are visible when using "fitBounds" option.
     *
     * @return {Object}
     *   Return the created map marker instance.
     */
    createMarker(data, updateFit = true) {
      const mset = data.setId && this.markerBuilders[data.setId] ? data.setId : this.markerType;

      // Generate a map marker and add it to this map's markers list.
      if (this.markerBuilders[mset]) {
        const marker = this.markerBuilders[mset](data);

        // Only if the map is initialized can we also initialize the map marker
        // and update the map bounds. Otherwise this is done when the map is
        // initialized itself.
        //
        // @see GMap::init()
        if (this._gmap) {
          marker.init(this);

          // If the marker is not already in the bounds, then we need
          // need check if the bounds and display window need to be updated.
          if (!this.bounds.contains(marker.position)) {
            this.bounds.extend(marker.position);
            if (updateFit && this.fitBounds && this.markers.size) {
              this.refreshBounds();
            }
          }
        }
        this.markers.set(marker, marker);
        return marker;
      }
    }

    /**
     * Remove the marker from the map.
     *
     * @param {Object} marker
     *   The marker data to delete.
     * @param {bool} updateFit
     *   Whether or not to update the map based on the removal of this marker.
     */
    removeMarker(marker, updateFit = true) {
      this.markers.delete(marker);

      // Remove the marker from the map.
      if (this._gmap) {
        marker.detach();
        if (updateFit) {
          // Evaluate if the current position requires a marker bounds update.
          this.markerPositionChanged(marker.position);
        }
      }
    }

    /**
     * Add click event listeners to the marker. If Google maps have not been
     * initialized yet, the marker object will add the listener when the marker
     * is initialized.
     *
     * @param {string} event
     *   Name of the event to add the new event listener to.
     * @param {function} listener
     *   Event listener function to add the the marker.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    on(event, listener) {
      // If Google Maps API has already been initialized, call the 'init' immediately.
      // This map will never init again in this lifecycle (will be recycled) so will
      // not need to keep track of this init for later.
      if (event === 'init' && this._gmap) {
        listener(this);
      } else {
        const data = {
          listener,
          handle: null
        };
        if (this.listeners.has(event)) {
          this.listeners.get(event).push(data);
        } else {
          this.listeners.set(event, [data]);
        }

        // If map is already initialize, register the event directly.
        if (this._gmap && event !== 'destroy') {
          data.handle = this._gmap.addListener(event, listener);
        }
      }
      return this;
    }

    /**
     * Remove event listeners for this map instance.
     *
     * @param {string} event
     *   The event identifier (click, keypress, etc...)
     * @param {function=} listener
     *   If provided, only remove this event listener, otherwise, remove all
     *   click events for this marker.
     *
     * @return {this}
     *   This marker instance for method chaining.
     */
    off(event, listener = null) {
      // Remove all handlers if no specific listener passed in.
      if (!listener) {
        if (event !== 'init' || event !== 'destroy') {
          (this.listeners.get(event) || []).forEach(data => {
            if (data.handle) google.maps.event.removeListener(data.handle);
          });
        }
        this.listeners.delete(event);
      } else {
        const listeners = this.listeners.get(event) || [];

        // Go hunting for the callback to remove.
        for (let i = 0; i < listeners.length; ++i) {
          const data = this.listeners[i];
          if (data.listener === listener || data.handle === listener) {
            if (listeners.length <= 1) {
              this.listeners.delete(event);
            } else {
              listeners.splice(i, 1);
            }
            if (data.handle) {
              google.maps.event.removeListener(data.handle);
            }
            break;
          }
        }
      }
      return this;
    }

    /**
     * Potential map clean-up.
     */
    destroy() {
      if (this._gmap) {
        // Register any click listeners which were already registered but
        // where waiting for the map to get initialized.
        (this.listeners.get('destroy') || []).forEach(data => data.listener(this));

        // Remove only the event handlers this instance assigned. Using
        // google.map.event.clearListeners() or google.map.event.clearInstanceListeners()
        // removes some native handlers, and makes the map behave incorrectly on reuse.
        this.listeners.forEach((list, event) => this.off(event));

        // Google maps suggests that the map should be recycled rather than
        // have the instance and data deleted. Detach the map instance, and make
        // it available for other map instances to reuse.
        this.el.removeChild(this._gmap.getDiv());
        GMap.gmapPool.push(this._gmap);
      } else if (this.initCallback) {
        // Prevent map initialization because it is unnecessary after this.
        loader.removeInit('gmap', this.initCallback);
      }

      // Remove all markers from the instance.
      this.markers.forEach(marker => marker.detach());
      this.markers.clear();

      // Remove all event listeners.
      this.listeners.clear();
    }
  }

  /**
   * Maintains a pool of google map instances that can be reused.
   *
   * Google maps suggests that the map should be recycled rather than
   * have the instance and data deleted. This keeps the list of maps ready to
   * be recycled.
   */
  GMap.gmapPool = [];

  /**
   * Add the Mapkit handler for generating a Google Maps instance.
   *
   * @param {HTMLElement} el
   *   The HTML element to attach the map to.
   * @param {Object} settings
   *   The map settings to use when generating the map.
   * @param {Object} markerBuilders
   *   The marker factory functions for creating markers for this map.
   *
   * @return {Object}
   *   The generated Map instance.
   */
  Mapkit.handler.gmap = function createGMap(el, settings, markerBuilders) {
    const instance = new GMap(el, settings, markerBuilders);
    const gmap = GMap.gmapPool.pop();
    if (gmap) {
      instance.init(gmap);
    } else {
      // Register this map to get initialized when Google maps loads.
      instance.initCallback = instance.init.bind(instance);
      loader.addInit('gmap', instance.initCallback);
    }
    return instance;
  };
})(Drupal, Mapkit);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ21hcC5qcyIsIm5hbWVzIjpbIlRvb2xzaGVkIiwidHMiLCJNYXBraXQiLCJkZWJvdW5jZSIsImxvYWRlciIsIkdNYXAiLCJjb25zdHJ1Y3RvciIsImVsIiwic2V0dGluZ3MiLCJtYXJrZXJCdWlsZGVycyIsIl9nbWFwIiwibWFya2VycyIsIk1hcCIsIm1hcmtlclR5cGUiLCJkZWZhdWx0TWFya2VyU2V0IiwiY2VudGVyIiwiZml0Qm91bmRzIiwic3BpZGVyZnlPcHRpb25zIiwibWFya2VyU3BpZGVyZnkiLCJtYXBPcHRpb25zIiwiY2xpY2thYmxlSWNvbnMiLCJsYXQiLCJsbmciLCJ6b29tIiwibWluWm9vbSIsIm1heFpvb20iLCJtYXBUeXBlSWQiLCJkZWZhdWx0TGF5ZXIiLCJtYXBUeXBlQ29udHJvbE9wdGlvbnMiLCJtYXBUeXBlSWRzIiwibGF5ZXJzIiwic3R5bGVzIiwiZmVhdHVyZVR5cGUiLCJlbGVtZW50VHlwZSIsInN0eWxlcnMiLCJ2aXNpYmlsaXR5IiwiYm91bmRzIiwicmVmcmVzaEJvdW5kcyIsInNpemUiLCJzZXRDZW50ZXIiLCJnZXRDZW50ZXIiLCJzZXRab29tIiwibGlzdGVuZXJzIiwiaW5pdCIsImdtYXAiLCJzZXRPcHRpb25zIiwid3JhcCIsIkVsZW1lbnQiLCJzdHlsZSIsImhlaWdodCIsImdvb2dsZSIsIm1hcHMiLCJhcHBlbmRDaGlsZCIsImdldERpdiIsIkxhdExuZ0JvdW5kcyIsIndpbmRvdyIsIk92ZXJsYXBwaW5nTWFya2VyU3BpZGVyZmllciIsIm9tcyIsImZvckVhY2giLCJtIiwiZXh0ZW5kIiwicG9zaXRpb24iLCJib3VuZHNMaXN0ZW5lciIsImV2ZW50IiwiYWRkTGlzdGVuZXIiLCJjbGllbnRXaWR0aCIsInJlbW92ZUxpc3RlbmVyIiwiZ2V0Wm9vbSIsIml0ZW1zIiwiZGF0YSIsImxpc3RlbmVyIiwiaGFuZGxlIiwiZGVsZXRlIiwiYyIsImNhbGNCb3VuZHMiLCJtYXJrZXJQb3NpdGlvbkNoYW5nZWQiLCJvbGRMYXRMbmciLCJsYXRMbmciLCJyZWZyZXNoIiwiY29udGFpbnMiLCJuZSIsImdldE5vcnRoRWFzdCIsInN3IiwiZ2V0U291dGhXZXN0Iiwib3Blbk1hcmtlclBvcHVwIiwibWFya2VyIiwiY29udGVudCIsImluZm9XaW4iLCJJbmZvV2luZG93Iiwic2V0Q29udGVudCIsIm9wZW4iLCJtYXAiLCJhbmNob3IiLCJfbWFya2VyIiwiY3JlYXRlTWFya2VyIiwidXBkYXRlRml0IiwibXNldCIsInNldElkIiwic2V0IiwicmVtb3ZlTWFya2VyIiwiZGV0YWNoIiwib24iLCJoYXMiLCJnZXQiLCJwdXNoIiwib2ZmIiwiaSIsImxlbmd0aCIsInNwbGljZSIsImRlc3Ryb3kiLCJsaXN0IiwicmVtb3ZlQ2hpbGQiLCJnbWFwUG9vbCIsImluaXRDYWxsYmFjayIsInJlbW92ZUluaXQiLCJjbGVhciIsImhhbmRsZXIiLCJjcmVhdGVHTWFwIiwiaW5zdGFuY2UiLCJwb3AiLCJiaW5kIiwiYWRkSW5pdCIsIkRydXBhbCJdLCJzb3VyY2VzIjpbImdtYXAuZXM2LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIigoeyBUb29sc2hlZDogdHMsIE1hcGtpdCwgZGVib3VuY2UgfSwgbG9hZGVyKSA9PiB7XG4gIC8qKlxuICAgKiBDcmVhdGUgYSBNYXBraXQgd3JhcHBlciBmb3Igc3VwcG9ydGluZyBHb29nbGUgTWFwcy5cbiAgICovXG4gIGNsYXNzIEdNYXAge1xuICAgIGNvbnN0cnVjdG9yKGVsLCBzZXR0aW5ncywgbWFya2VyQnVpbGRlcnMpIHtcbiAgICAgIC8vIEludGVybmFsIEdvb2dsZSBtYXBzIGluc3RhbmNlLCBzaG91bGQgYWx3YXlzIHN0YXJ0IGFzIE5VTEwgYW5kXG4gICAgICAvLyBnZXQgYXNzaWduZWQgZHVyaW5nIHRoZSBtYXAgaW5pdGlhbGl6YXRpb24uXG4gICAgICB0aGlzLl9nbWFwID0gbnVsbDtcblxuICAgICAgdGhpcy5lbCA9IGVsO1xuICAgICAgdGhpcy5tYXJrZXJzID0gbmV3IE1hcCgpO1xuICAgICAgdGhpcy5tYXJrZXJUeXBlID0gc2V0dGluZ3MuZGVmYXVsdE1hcmtlclNldDtcbiAgICAgIHRoaXMubWFya2VyQnVpbGRlcnMgPSBtYXJrZXJCdWlsZGVycztcblxuICAgICAgLy8gVHJhbnNmZXIgc2V0dGluZ3MsIHdoaWxlIGVuc3VyaW5nIHRoYXQgd2UgaGF2ZSBzYW5lIGRlZmF1bHRzLlxuICAgICAgdGhpcy5jZW50ZXIgPSBzZXR0aW5ncy5jZW50ZXI7XG4gICAgICB0aGlzLmZpdEJvdW5kcyA9IHNldHRpbmdzLmZpdEJvdW5kcyB8fCBmYWxzZTtcbiAgICAgIHRoaXMuc3BpZGVyZnlPcHRpb25zID0gc2V0dGluZ3MubWFya2VyU3BpZGVyZnk7XG4gICAgICB0aGlzLm1hcE9wdGlvbnMgPSB7XG4gICAgICAgIGNsaWNrYWJsZUljb25zOiBmYWxzZSxcbiAgICAgICAgY2VudGVyOiBzZXR0aW5ncy5jZW50ZXIgfHwgeyBsYXQ6IDAsIGxuZzogMCB9LFxuICAgICAgICB6b29tOiBzZXR0aW5ncy56b29tIHx8IDExLFxuICAgICAgICBtaW5ab29tOiBzZXR0aW5ncy5taW5ab29tIHx8IDQsXG4gICAgICAgIG1heFpvb206IHNldHRpbmdzLm1heFpvb20gfHwgMTcsXG4gICAgICAgIG1hcFR5cGVJZDogc2V0dGluZ3MuZGVmYXVsdExheWVyIHx8ICdyb2FkbWFwJyxcbiAgICAgICAgbWFwVHlwZUNvbnRyb2xPcHRpb25zOiB7XG4gICAgICAgICAgbWFwVHlwZUlkczogc2V0dGluZ3MubGF5ZXJzIHx8IG51bGwsXG4gICAgICAgIH0sXG4gICAgICAgIC8vIERvbid0IGRpc3BsYXkgXCJQb2ludHMgb2YgSW50ZXJlc3RcIiBhcyB0aGV5IGNhbiBjbHV0dGVyIGEgbWFwLlxuICAgICAgICBzdHlsZXM6IFt7XG4gICAgICAgICAgZmVhdHVyZVR5cGU6ICdwb2knLFxuICAgICAgICAgIGVsZW1lbnRUeXBlOiAnbGFiZWxzJyxcbiAgICAgICAgICBzdHlsZXJzOiBbeyB2aXNpYmlsaXR5OiAnb2ZmJyB9XSxcbiAgICAgICAgfV0sXG4gICAgICB9O1xuXG4gICAgICAvLyBCb3VuZHMgY2Fubm90IGJlIGluaXRpYWxpemVkIHVudGlsIEdvb2dsZSBtYXBzIEFQSSBoYXMgYmVlbiBsb2FkZWQuXG4gICAgICB0aGlzLmJvdW5kcyA9IG51bGw7XG5cbiAgICAgIC8vIENyZWF0ZSBhIGRlbGF5ZWQgY2FsbGJhY2sgdG8gcmVmcmVzaCB0aGUgbWFwIGJvdW5kcy4gVGhpcyBzbWFsbCBkZWxheVxuICAgICAgLy8gYWxsb3dzIHRoZSBtYXAgQVBJIHRvIGNhdGNoLXVwIGFuZCBjb25zb2xpZGF0ZSBxdWljayB2aWV3cG9ydCBjaGFuZ2VzLlxuICAgICAgdGhpcy5yZWZyZXNoQm91bmRzID0gZGVib3VuY2UoKCkgPT4ge1xuICAgICAgICBpZiAodGhpcy5fZ21hcCkge1xuICAgICAgICAgIGlmICh0aGlzLm1hcmtlcnMuc2l6ZSA+IDEpIHtcbiAgICAgICAgICAgIHRoaXMuX2dtYXAuZml0Qm91bmRzKHRoaXMuYm91bmRzKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZiAodGhpcy5tYXJrZXJzLnNpemUgPT09IDEpIHtcbiAgICAgICAgICAgIHRoaXMuX2dtYXAuc2V0Q2VudGVyKHRoaXMuYm91bmRzLmdldENlbnRlcigpKTtcbiAgICAgICAgICAgIHRoaXMuX2dtYXAuc2V0Wm9vbSh0aGlzLm1hcE9wdGlvbnMuem9vbSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9LCAxMDApO1xuXG4gICAgICAvLyBLZWVwIHRyYWNrIG9mIHRoZSBtYXAgZXZlbnQgbGlzdGVuZXJzLCBrZXllZCBieSBldmVudCBuYW1lLlxuICAgICAgdGhpcy5saXN0ZW5lcnMgPSBuZXcgTWFwKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSW5pdGlhbGl6ZSB0aGUgYWN0dWFsIG1hcCB3aGVuIEdvb2dsZSBNYXBzIEFQSSBoYXMgYmVlbiBsb2FkZWQuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge2dvb2dsZS5tYXBzLk1hcD19IGdtYXBcbiAgICAgKiAgIEEgR29vZ2xlIG1hcHMgaW5zdGFuY2UgaWYgcmV1c2luZyBhbiBleGlzdGluZyBtYXAuIE90aGVyd2lzZSBhIG5ldyBtYXBcbiAgICAgKiAgIGlzIGNyZWF0ZWQuXG4gICAgICovXG4gICAgaW5pdChnbWFwID0gbnVsbCkge1xuICAgICAgaWYgKGdtYXApIHtcbiAgICAgICAgZ21hcC5zZXRPcHRpb25zKHRoaXMubWFwT3B0aW9ucyk7XG4gICAgICAgIHRoaXMuX2dtYXAgPSBnbWFwO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGNvbnN0IHdyYXAgPSBuZXcgdHMuRWxlbWVudCgnZGl2JywgeyBzdHlsZTogeyBoZWlnaHQ6ICcxMDAlJyB9IH0pO1xuICAgICAgICB0aGlzLl9nbWFwID0gbmV3IGdvb2dsZS5tYXBzLk1hcCh3cmFwLmVsLCB0aGlzLm1hcE9wdGlvbnMpO1xuICAgICAgfVxuXG4gICAgICAvLyBBdHRhY2ggdGhlIG1hcCBpbnN0YW5jZSB0byB0aGUgY3VycmVudCBNYXBraXQuR01hcCB3cmFwcGVyLlxuICAgICAgdGhpcy5lbC5hcHBlbmRDaGlsZCh0aGlzLl9nbWFwLmdldERpdigpKTtcbiAgICAgIHRoaXMuYm91bmRzID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZ0JvdW5kcygpO1xuXG4gICAgICBpZiAodGhpcy5zcGlkZXJmeU9wdGlvbnMgJiYgd2luZG93Lk92ZXJsYXBwaW5nTWFya2VyU3BpZGVyZmllcikge1xuICAgICAgICB0aGlzLm9tcyA9IG5ldyB3aW5kb3cuT3ZlcmxhcHBpbmdNYXJrZXJTcGlkZXJmaWVyKHRoaXMuX2dtYXAsIHRoaXMuc3BpZGVyZnlPcHRpb25zKTtcbiAgICAgIH1cblxuICAgICAgLy8gSWYgYW55IG1hcmtlcnMgd2VyZSBhZGRlZCBiZWZvcmUgR29vZ2xlIG1hcHMgd2FzIHJlYWR5IHRoZW4gYnVpbGQgdGhlXG4gICAgICAvLyBtYXJrZXJzIG5vdyB0aGF0IHRoZSBHb29nbGUgQVBJIGhhcyBiZWVuIGxvYWRlZC5cbiAgICAgIGlmICh0aGlzLm1hcmtlcnMuc2l6ZSkge1xuICAgICAgICB0aGlzLm1hcmtlcnMuZm9yRWFjaCgobSkgPT4ge1xuICAgICAgICAgIG0uaW5pdCh0aGlzKTtcbiAgICAgICAgICB0aGlzLmJvdW5kcy5leHRlbmQobS5wb3NpdGlvbik7XG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5maXRCb3VuZHMpIHtcbiAgICAgICAgY29uc3QgYm91bmRzTGlzdGVuZXIgPSBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLl9nbWFwLCAnYm91bmRzX2NoYW5nZWQnLCAoKSA9PiB7XG4gICAgICAgICAgaWYgKHRoaXMuZWwuY2xpZW50V2lkdGggPiAwKSB7XG4gICAgICAgICAgICB0aGlzLnJlZnJlc2hCb3VuZHMoKTtcbiAgICAgICAgICAgIGdvb2dsZS5tYXBzLmV2ZW50LnJlbW92ZUxpc3RlbmVyKGJvdW5kc0xpc3RlbmVyKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIFdoZW4gdXNpbmcgYm91bmRzIGZpdHRpbmcsIGl0IGlzIHBvc3NpYmxlIGZvciBHb29nbGUgbWFwcyB0byBnb1xuICAgICAgICAvLyBiZXlvbmQgb3VyIHpvb20gbGltaXRzLlxuICAgICAgICBpZiAodGhpcy5tYXBPcHRpb25zLm1pblpvb20gfHwgdGhpcy5tYXBPcHRpb25zLm1heFpvb20pIHtcbiAgICAgICAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLl9nbWFwLCAnem9vbV9jaGFuZ2VkJywgKCkgPT4ge1xuICAgICAgICAgICAgY29uc3Qgem9vbSA9IHRoaXMuX2dtYXAuZ2V0Wm9vbSgpO1xuXG4gICAgICAgICAgICBpZiAodGhpcy5tYXBPcHRpb25zLm1pblpvb20gJiYgem9vbSA8IHRoaXMubWFwT3B0aW9ucy5taW5ab29tKSB7XG4gICAgICAgICAgICAgIHRoaXMuX2dtYXAuc2V0Wm9vbSh0aGlzLm1hcE9wdGlvbnMubWluWm9vbSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmICh0aGlzLm1hcE9wdGlvbnMubWF4Wm9vbSAmJiB6b29tID4gdGhpcy5tYXBPcHRpb25zLm1heFpvb20pIHtcbiAgICAgICAgICAgICAgdGhpcy5fZ21hcC5zZXRab29tKHRoaXMubWFwT3B0aW9ucy5tYXhab29tKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAvLyBSZWdpc3RlciBhbnkgY2xpY2sgbGlzdGVuZXJzIHdoaWNoIHdlcmUgYWxyZWFkeSByZWdpc3RlcmVkIGJ1dFxuICAgICAgLy8gd2hlcmUgd2FpdGluZyBmb3IgdGhlIG1hcCB0byBnZXQgaW5pdGlhbGl6ZWQuXG4gICAgICB0aGlzLmxpc3RlbmVycy5mb3JFYWNoKChpdGVtcywgZXZlbnQpID0+IHtcbiAgICAgICAgLy8gSW5pdCBpcyBhIGN1c3RvbSBNYXBraXQgZXZlbnQsIGFuZCB0aG9zZSBsaXN0ZW5lcnMgYXJlIHJ1blxuICAgICAgICAvLyBpbW1lZGlhdGVseSBydW4gc2luY2UgdGhpcyBpcyB0aGUgbWFwIGluaXRpYWxpemF0aW9uLlxuICAgICAgICBpZiAoZXZlbnQgPT09ICdpbml0Jykge1xuICAgICAgICAgIGl0ZW1zLmZvckVhY2goKGRhdGEpID0+IGRhdGEubGlzdGVuZXIodGhpcykpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKGV2ZW50ICE9PSAnZGVzdHJveScpIHtcbiAgICAgICAgICBpdGVtcy5mb3JFYWNoKChkYXRhKSA9PiB7XG4gICAgICAgICAgICBkYXRhLmhhbmRsZSA9IHRoaXMuX2dtYXAuYWRkTGlzdGVuZXIoZXZlbnQsIGRhdGEubGlzdGVuZXIpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9KTtcblxuICAgICAgLy8gSW5pdCBoYXMgYmVlbiBoYW5kbGVkLCByZW1vdmUgdGhpcyBzZXQgb2YgZXZlbnQgbGlzdGVuZXJzLlxuICAgICAgdGhpcy5saXN0ZW5lcnMuZGVsZXRlKCdpbml0Jyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0IHRoZSBtYXAgY2VudGVyLiBJZiBtYXAgaGFzIG5vdCBiZWVuIGluaXRpYWxpemVkIHlldCwgdGhpcyB2YWx1ZVxuICAgICAqIG1pZ2h0IG5vdCBiZSB0aGUgY29ycmVjdCB2YWx1ZXMgZm9yIHdoZXJlIHRoZSBidWlsdCBtYXAgd2lsbCBhY3R1YWxseVxuICAgICAqIGJlIGNlbnRlcmVkLiBPbmUgY2FzZSBpcyBpZiB0aGUgbWFwIGlzIHNldCB0byBmaXQgYm91bmRzIGJ1dCBtYXJrZXJcbiAgICAgKiBib3VuZHMgYXJlIG5vdCBjYWxjdWxhdGVkIHlldC5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge09iamVjdH1cbiAgICAgKiAgIFJldHVybnMgYSBKU09OIG9iamVjdCB3aXRoIFwibGF0XCIgYW5kIFwibG5nXCIgdmFsdWVzIGZvciB0aGUgY3VycmVudFxuICAgICAqICAgbWFwIHNldCBjZW50ZXIgcG9zaXRpb24uXG4gICAgICovXG4gICAgZ2V0Q2VudGVyKCkge1xuICAgICAgaWYgKHRoaXMuX2dtYXApIHtcbiAgICAgICAgY29uc3QgYyA9IHRoaXMuX2dtYXAuZ2V0Q2VudGVyKCk7XG5cbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBsYXQ6IGMubGF0LFxuICAgICAgICAgIGxuZzogYy5sbmcsXG4gICAgICAgIH07XG4gICAgICB9XG5cbiAgICAgIC8vIElmIG1hcCBoYXMgbm90IGJlZW4gaW5pdGlhbGl6ZWQsIHRoYXQgcmV0dXJuIHRoZSBjb25maWd1cmVkIGNlbnRlclxuICAgICAgLy8gcG9pbnQuIFRoaXMgd2lsbCBiZSBkaWZmZXJlbnQgZnJvbSBhIGJvdW5kcyBjZW50ZXIsIG9yIHRoZSBjdXJyZW50XG4gICAgICAvLyBtYXAgY2VudGVyIHBvc2l0aW9uLlxuICAgICAgcmV0dXJuIHRoaXMuY2VudGVyO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJlY2FsY3VsYXRlIHRoZSBib3VuZHMgb2YgdGhlIG1hcmtlci5cbiAgICAgKi9cbiAgICBjYWxjQm91bmRzKCkge1xuICAgICAgdGhpcy5ib3VuZHMgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nQm91bmRzKCk7XG5cbiAgICAgIHRoaXMubWFya2Vycy5mb3JFYWNoKChtKSA9PiB7XG4gICAgICAgIHRoaXMuYm91bmRzLmV4dGVuZChtLnBvc2l0aW9uKTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEEgbWFya2VyIHBvc2l0aW9uIGhhcyBlaXRoZXIgYmVlbiByZW1vdmVkIG9yIG1vdmVkLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtMYXRMbmdMaXRlcmFsfSBvbGRMYXRMbmdcbiAgICAgKiAgIFRoZSBvcmlnaW5hbCBwb3NpdGlvbiBvZiB0aGUgbWFya2VyLlxuICAgICAqIEBwYXJhbSB7TGF0TG5nTGl0ZXJhbD19IGxhdExuZ1xuICAgICAqICAgSWYgbWFya2VyIGhhcyBiZWVuIHJlbW92ZWQgYW5kIGRvZXMgbm90IGhhdmUgYSBuZXcgcG9zaXRpb24gdGhpcyBzaG91bGRcbiAgICAgKiAgIGJlIGxlZnQgZW1wdHkuXG4gICAgICovXG4gICAgbWFya2VyUG9zaXRpb25DaGFuZ2VkKG9sZExhdExuZywgbGF0TG5nKSB7XG4gICAgICBsZXQgcmVmcmVzaCA9IGZhbHNlO1xuXG4gICAgICBpZiAobGF0TG5nICYmICF0aGlzLmJvdW5kcy5jb250YWlucyhsYXRMbmcpKSB7XG4gICAgICAgIHRoaXMuYm91bmRzLmV4dGVuZChsYXRMbmcpO1xuICAgICAgICByZWZyZXNoID0gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgLy8gQm91bmRzIG9ubHkgY2hhbmdlIGlmIHRoZSBtYXJrZXIgbG9jYXRpb24gY29pbmNpZGVzIHdpdGggb25lIG9mXG4gICAgICAvLyB0aGUgZWRnZXMgb2YgdGhlIGJvdW5kcy5cbiAgICAgIGNvbnN0IG5lID0gdGhpcy5ib3VuZHMuZ2V0Tm9ydGhFYXN0KCk7XG4gICAgICBjb25zdCBzdyA9IHRoaXMuYm91bmRzLmdldFNvdXRoV2VzdCgpO1xuXG4gICAgICBpZiAoKG5lLmxhdCgpID09PSBvbGRMYXRMbmcubGF0IHx8IHN3LmxhdCgpID09PSBvbGRMYXRMbmcubGF0KVxuICAgICAgICAmJiAobmUubG5nKCkgPT09IG9sZExhdExuZy5sbmcgfHwgc3cubG5nKCkgPT09IG9sZExhdExuZy5sbmcpXG4gICAgICApIHtcbiAgICAgICAgdGhpcy5jYWxjQm91bmRzKCk7XG4gICAgICAgIHJlZnJlc2ggPSB0cnVlO1xuICAgICAgfVxuXG4gICAgICAvLyBUaGUgYm91bmRzIHdlcmUgY2hhbmdlZCBpbiBzb21lIHdheSwgY2hlY2sgaWYgbWFwIG5lZWRzIHRvIGJlIHJlZm9jdXNlZC5cbiAgICAgIGlmIChyZWZyZXNoKSB7XG4gICAgICAgIGlmICh0aGlzLm1hcmtlcnMuc2l6ZSA8PSAxKSB7XG4gICAgICAgICAgY29uc3QgYyA9IHRoaXMubWFya2Vycy5zaXplID8gdGhpcy5ib3VuZHMuZ2V0Q2VudGVyKCkgOiB0aGlzLmNlbnRlcjtcbiAgICAgICAgICB0aGlzLnNldENlbnRlcihjKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICh0aGlzLmZpdEJvdW5kcykge1xuICAgICAgICAgIHRoaXMucmVmcmVzaEJvdW5kcygpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogT3BlbiBhbiBpbmZvIHdpbmRvdyBmb3IgdGhpcyBtYXAgYXQgdGhlIGdpdmVuIG1hcmtlciBsb2NhdGlvbi5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7TWFwa2l0Lk1hcmtlcn0gbWFya2VyXG4gICAgICogICBBIE1hcGtpdCBtYXJrZXIgaW5zdGFuY2UsIHdoaWNoIHdyYXBzIGEgR29vZ2xlIG1hcHMgbWFya2VyIG9iamVjdC5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ3xIVE1MRWxlbWVudH0gY29udGVudFxuICAgICAqICAgQ29udGVudCB0byBwb3B1bGF0ZSB0aGUgaW5mbyB3aW5kb3cuXG4gICAgICovXG4gICAgb3Blbk1hcmtlclBvcHVwKG1hcmtlciwgY29udGVudCkge1xuICAgICAgaWYgKCF0aGlzLmluZm9XaW4pIHtcbiAgICAgICAgdGhpcy5pbmZvV2luID0gbmV3IGdvb2dsZS5tYXBzLkluZm9XaW5kb3coKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5pbmZvV2luLnNldENvbnRlbnQoY29udGVudCk7XG4gICAgICB0aGlzLmluZm9XaW4ub3Blbih7XG4gICAgICAgIG1hcDogdGhpcy5fZ21hcCxcbiAgICAgICAgYW5jaG9yOiBtYXJrZXIuX21hcmtlcixcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldCB0aGUgbWFwIGNlbnRlciBsb2NhdGlvbi5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7T2JqZWN0fGdvb2dsZS5tYXBzLkxhdExuZ30gY2VudGVyXG4gICAgICogICBMYXRMbmcgY29vcmRpbmF0ZSB0byBjZW50ZXIgdGhlIG1hcCBvbi5cbiAgICAgKi9cbiAgICBzZXRDZW50ZXIoY2VudGVyKSB7XG4gICAgICB0aGlzLmNlbnRlciA9IGNlbnRlcjtcblxuICAgICAgaWYgKHRoaXMuX2dtYXApIHtcbiAgICAgICAgdGhpcy5fZ21hcC5zZXRDZW50ZXIoY2VudGVyKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBBZGQgYSBuZXcgbWFwIG1hcmtlciBiYXNlZCBvbiB0aGUgbWFya2VyIGRlZmluaXRpb24uXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gZGF0YVxuICAgICAqICAgVGhlIG1hcmtlciBkZWZpbml0aW9uLCB3aGljaCBzaG91bGQgYXQgbWluaW11bSBoYXZlIGEgXCJsYXRMbmdcIiB2YWx1ZS5cbiAgICAgKiBAcGFyYW0ge2Jvb2x9IHVwZGF0ZUZpdFxuICAgICAqICAgU2hvdWxkIHRoZSBib3VuZHMgYmUgYWRqdXN0ZWQgd2hlbiB0aGlzIG1hcmtlciBpcyBhZGRlZC4gVGhpcyBlbnN1cmVzXG4gICAgICogICB0aGF0IG5ld2x5IGFkZGVkIG1hcCBtYXJrZXJzIGFyZSB2aXNpYmxlIHdoZW4gdXNpbmcgXCJmaXRCb3VuZHNcIiBvcHRpb24uXG4gICAgICpcbiAgICAgKiBAcmV0dXJuIHtPYmplY3R9XG4gICAgICogICBSZXR1cm4gdGhlIGNyZWF0ZWQgbWFwIG1hcmtlciBpbnN0YW5jZS5cbiAgICAgKi9cbiAgICBjcmVhdGVNYXJrZXIoZGF0YSwgdXBkYXRlRml0ID0gdHJ1ZSkge1xuICAgICAgY29uc3QgbXNldCA9IGRhdGEuc2V0SWQgJiYgdGhpcy5tYXJrZXJCdWlsZGVyc1tkYXRhLnNldElkXVxuICAgICAgICA/IGRhdGEuc2V0SWQgOiB0aGlzLm1hcmtlclR5cGU7XG5cbiAgICAgIC8vIEdlbmVyYXRlIGEgbWFwIG1hcmtlciBhbmQgYWRkIGl0IHRvIHRoaXMgbWFwJ3MgbWFya2VycyBsaXN0LlxuICAgICAgaWYgKHRoaXMubWFya2VyQnVpbGRlcnNbbXNldF0pIHtcbiAgICAgICAgY29uc3QgbWFya2VyID0gdGhpcy5tYXJrZXJCdWlsZGVyc1ttc2V0XShkYXRhKTtcblxuICAgICAgICAvLyBPbmx5IGlmIHRoZSBtYXAgaXMgaW5pdGlhbGl6ZWQgY2FuIHdlIGFsc28gaW5pdGlhbGl6ZSB0aGUgbWFwIG1hcmtlclxuICAgICAgICAvLyBhbmQgdXBkYXRlIHRoZSBtYXAgYm91bmRzLiBPdGhlcndpc2UgdGhpcyBpcyBkb25lIHdoZW4gdGhlIG1hcCBpc1xuICAgICAgICAvLyBpbml0aWFsaXplZCBpdHNlbGYuXG4gICAgICAgIC8vXG4gICAgICAgIC8vIEBzZWUgR01hcDo6aW5pdCgpXG4gICAgICAgIGlmICh0aGlzLl9nbWFwKSB7XG4gICAgICAgICAgbWFya2VyLmluaXQodGhpcyk7XG5cbiAgICAgICAgICAvLyBJZiB0aGUgbWFya2VyIGlzIG5vdCBhbHJlYWR5IGluIHRoZSBib3VuZHMsIHRoZW4gd2UgbmVlZFxuICAgICAgICAgIC8vIG5lZWQgY2hlY2sgaWYgdGhlIGJvdW5kcyBhbmQgZGlzcGxheSB3aW5kb3cgbmVlZCB0byBiZSB1cGRhdGVkLlxuICAgICAgICAgIGlmICghdGhpcy5ib3VuZHMuY29udGFpbnMobWFya2VyLnBvc2l0aW9uKSkge1xuICAgICAgICAgICAgdGhpcy5ib3VuZHMuZXh0ZW5kKG1hcmtlci5wb3NpdGlvbik7XG5cbiAgICAgICAgICAgIGlmICh1cGRhdGVGaXQgJiYgdGhpcy5maXRCb3VuZHMgJiYgdGhpcy5tYXJrZXJzLnNpemUpIHtcbiAgICAgICAgICAgICAgdGhpcy5yZWZyZXNoQm91bmRzKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5tYXJrZXJzLnNldChtYXJrZXIsIG1hcmtlcik7XG4gICAgICAgIHJldHVybiBtYXJrZXI7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmVtb3ZlIHRoZSBtYXJrZXIgZnJvbSB0aGUgbWFwLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG1hcmtlclxuICAgICAqICAgVGhlIG1hcmtlciBkYXRhIHRvIGRlbGV0ZS5cbiAgICAgKiBAcGFyYW0ge2Jvb2x9IHVwZGF0ZUZpdFxuICAgICAqICAgV2hldGhlciBvciBub3QgdG8gdXBkYXRlIHRoZSBtYXAgYmFzZWQgb24gdGhlIHJlbW92YWwgb2YgdGhpcyBtYXJrZXIuXG4gICAgICovXG4gICAgcmVtb3ZlTWFya2VyKG1hcmtlciwgdXBkYXRlRml0ID0gdHJ1ZSkge1xuICAgICAgdGhpcy5tYXJrZXJzLmRlbGV0ZShtYXJrZXIpO1xuXG4gICAgICAvLyBSZW1vdmUgdGhlIG1hcmtlciBmcm9tIHRoZSBtYXAuXG4gICAgICBpZiAodGhpcy5fZ21hcCkge1xuICAgICAgICBtYXJrZXIuZGV0YWNoKCk7XG5cbiAgICAgICAgaWYgKHVwZGF0ZUZpdCkge1xuICAgICAgICAgIC8vIEV2YWx1YXRlIGlmIHRoZSBjdXJyZW50IHBvc2l0aW9uIHJlcXVpcmVzIGEgbWFya2VyIGJvdW5kcyB1cGRhdGUuXG4gICAgICAgICAgdGhpcy5tYXJrZXJQb3NpdGlvbkNoYW5nZWQobWFya2VyLnBvc2l0aW9uKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEFkZCBjbGljayBldmVudCBsaXN0ZW5lcnMgdG8gdGhlIG1hcmtlci4gSWYgR29vZ2xlIG1hcHMgaGF2ZSBub3QgYmVlblxuICAgICAqIGluaXRpYWxpemVkIHlldCwgdGhlIG1hcmtlciBvYmplY3Qgd2lsbCBhZGQgdGhlIGxpc3RlbmVyIHdoZW4gdGhlIG1hcmtlclxuICAgICAqIGlzIGluaXRpYWxpemVkLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGV2ZW50XG4gICAgICogICBOYW1lIG9mIHRoZSBldmVudCB0byBhZGQgdGhlIG5ldyBldmVudCBsaXN0ZW5lciB0by5cbiAgICAgKiBAcGFyYW0ge2Z1bmN0aW9ufSBsaXN0ZW5lclxuICAgICAqICAgRXZlbnQgbGlzdGVuZXIgZnVuY3Rpb24gdG8gYWRkIHRoZSB0aGUgbWFya2VyLlxuICAgICAqXG4gICAgICogQHJldHVybiB7dGhpc31cbiAgICAgKiAgIFRoaXMgbWFya2VyIGluc3RhbmNlIGZvciBtZXRob2QgY2hhaW5pbmcuXG4gICAgICovXG4gICAgb24oZXZlbnQsIGxpc3RlbmVyKSB7XG4gICAgICAvLyBJZiBHb29nbGUgTWFwcyBBUEkgaGFzIGFscmVhZHkgYmVlbiBpbml0aWFsaXplZCwgY2FsbCB0aGUgJ2luaXQnIGltbWVkaWF0ZWx5LlxuICAgICAgLy8gVGhpcyBtYXAgd2lsbCBuZXZlciBpbml0IGFnYWluIGluIHRoaXMgbGlmZWN5Y2xlICh3aWxsIGJlIHJlY3ljbGVkKSBzbyB3aWxsXG4gICAgICAvLyBub3QgbmVlZCB0byBrZWVwIHRyYWNrIG9mIHRoaXMgaW5pdCBmb3IgbGF0ZXIuXG4gICAgICBpZiAoZXZlbnQgPT09ICdpbml0JyAmJiB0aGlzLl9nbWFwKSB7XG4gICAgICAgIGxpc3RlbmVyKHRoaXMpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGNvbnN0IGRhdGEgPSB7IGxpc3RlbmVyLCBoYW5kbGU6IG51bGwgfTtcblxuICAgICAgICBpZiAodGhpcy5saXN0ZW5lcnMuaGFzKGV2ZW50KSkge1xuICAgICAgICAgIHRoaXMubGlzdGVuZXJzLmdldChldmVudCkucHVzaChkYXRhKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICB0aGlzLmxpc3RlbmVycy5zZXQoZXZlbnQsIFtkYXRhXSk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBJZiBtYXAgaXMgYWxyZWFkeSBpbml0aWFsaXplLCByZWdpc3RlciB0aGUgZXZlbnQgZGlyZWN0bHkuXG4gICAgICAgIGlmICh0aGlzLl9nbWFwICYmIGV2ZW50ICE9PSAnZGVzdHJveScpIHtcbiAgICAgICAgICBkYXRhLmhhbmRsZSA9IHRoaXMuX2dtYXAuYWRkTGlzdGVuZXIoZXZlbnQsIGxpc3RlbmVyKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZW1vdmUgZXZlbnQgbGlzdGVuZXJzIGZvciB0aGlzIG1hcCBpbnN0YW5jZS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBldmVudFxuICAgICAqICAgVGhlIGV2ZW50IGlkZW50aWZpZXIgKGNsaWNrLCBrZXlwcmVzcywgZXRjLi4uKVxuICAgICAqIEBwYXJhbSB7ZnVuY3Rpb249fSBsaXN0ZW5lclxuICAgICAqICAgSWYgcHJvdmlkZWQsIG9ubHkgcmVtb3ZlIHRoaXMgZXZlbnQgbGlzdGVuZXIsIG90aGVyd2lzZSwgcmVtb3ZlIGFsbFxuICAgICAqICAgY2xpY2sgZXZlbnRzIGZvciB0aGlzIG1hcmtlci5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge3RoaXN9XG4gICAgICogICBUaGlzIG1hcmtlciBpbnN0YW5jZSBmb3IgbWV0aG9kIGNoYWluaW5nLlxuICAgICAqL1xuICAgIG9mZihldmVudCwgbGlzdGVuZXIgPSBudWxsKSB7XG4gICAgICAvLyBSZW1vdmUgYWxsIGhhbmRsZXJzIGlmIG5vIHNwZWNpZmljIGxpc3RlbmVyIHBhc3NlZCBpbi5cbiAgICAgIGlmICghbGlzdGVuZXIpIHtcbiAgICAgICAgaWYgKGV2ZW50ICE9PSAnaW5pdCcgfHwgZXZlbnQgIT09ICdkZXN0cm95Jykge1xuICAgICAgICAgICh0aGlzLmxpc3RlbmVycy5nZXQoZXZlbnQpIHx8IFtdKS5mb3JFYWNoKChkYXRhKSA9PiB7XG4gICAgICAgICAgICBpZiAoZGF0YS5oYW5kbGUpIGdvb2dsZS5tYXBzLmV2ZW50LnJlbW92ZUxpc3RlbmVyKGRhdGEuaGFuZGxlKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMubGlzdGVuZXJzLmRlbGV0ZShldmVudCk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgY29uc3QgbGlzdGVuZXJzID0gdGhpcy5saXN0ZW5lcnMuZ2V0KGV2ZW50KSB8fCBbXTtcblxuICAgICAgICAvLyBHbyBodW50aW5nIGZvciB0aGUgY2FsbGJhY2sgdG8gcmVtb3ZlLlxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGxpc3RlbmVycy5sZW5ndGg7ICsraSkge1xuICAgICAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmxpc3RlbmVyc1tpXTtcblxuICAgICAgICAgIGlmIChkYXRhLmxpc3RlbmVyID09PSBsaXN0ZW5lciB8fCBkYXRhLmhhbmRsZSA9PT0gbGlzdGVuZXIpIHtcbiAgICAgICAgICAgIGlmIChsaXN0ZW5lcnMubGVuZ3RoIDw9IDEpIHtcbiAgICAgICAgICAgICAgdGhpcy5saXN0ZW5lcnMuZGVsZXRlKGV2ZW50KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBsaXN0ZW5lcnMuc3BsaWNlKGksIDEpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoZGF0YS5oYW5kbGUpIHtcbiAgICAgICAgICAgICAgZ29vZ2xlLm1hcHMuZXZlbnQucmVtb3ZlTGlzdGVuZXIoZGF0YS5oYW5kbGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBQb3RlbnRpYWwgbWFwIGNsZWFuLXVwLlxuICAgICAqL1xuICAgIGRlc3Ryb3koKSB7XG4gICAgICBpZiAodGhpcy5fZ21hcCkge1xuICAgICAgICAvLyBSZWdpc3RlciBhbnkgY2xpY2sgbGlzdGVuZXJzIHdoaWNoIHdlcmUgYWxyZWFkeSByZWdpc3RlcmVkIGJ1dFxuICAgICAgICAvLyB3aGVyZSB3YWl0aW5nIGZvciB0aGUgbWFwIHRvIGdldCBpbml0aWFsaXplZC5cbiAgICAgICAgKHRoaXMubGlzdGVuZXJzLmdldCgnZGVzdHJveScpIHx8IFtdKS5mb3JFYWNoKChkYXRhKSA9PiBkYXRhLmxpc3RlbmVyKHRoaXMpKTtcblxuICAgICAgICAvLyBSZW1vdmUgb25seSB0aGUgZXZlbnQgaGFuZGxlcnMgdGhpcyBpbnN0YW5jZSBhc3NpZ25lZC4gVXNpbmdcbiAgICAgICAgLy8gZ29vZ2xlLm1hcC5ldmVudC5jbGVhckxpc3RlbmVycygpIG9yIGdvb2dsZS5tYXAuZXZlbnQuY2xlYXJJbnN0YW5jZUxpc3RlbmVycygpXG4gICAgICAgIC8vIHJlbW92ZXMgc29tZSBuYXRpdmUgaGFuZGxlcnMsIGFuZCBtYWtlcyB0aGUgbWFwIGJlaGF2ZSBpbmNvcnJlY3RseSBvbiByZXVzZS5cbiAgICAgICAgdGhpcy5saXN0ZW5lcnMuZm9yRWFjaCgobGlzdCwgZXZlbnQpID0+IHRoaXMub2ZmKGV2ZW50KSk7XG5cbiAgICAgICAgLy8gR29vZ2xlIG1hcHMgc3VnZ2VzdHMgdGhhdCB0aGUgbWFwIHNob3VsZCBiZSByZWN5Y2xlZCByYXRoZXIgdGhhblxuICAgICAgICAvLyBoYXZlIHRoZSBpbnN0YW5jZSBhbmQgZGF0YSBkZWxldGVkLiBEZXRhY2ggdGhlIG1hcCBpbnN0YW5jZSwgYW5kIG1ha2VcbiAgICAgICAgLy8gaXQgYXZhaWxhYmxlIGZvciBvdGhlciBtYXAgaW5zdGFuY2VzIHRvIHJldXNlLlxuICAgICAgICB0aGlzLmVsLnJlbW92ZUNoaWxkKHRoaXMuX2dtYXAuZ2V0RGl2KCkpO1xuICAgICAgICBHTWFwLmdtYXBQb29sLnB1c2godGhpcy5fZ21hcCk7XG4gICAgICB9XG4gICAgICBlbHNlIGlmICh0aGlzLmluaXRDYWxsYmFjaykge1xuICAgICAgICAvLyBQcmV2ZW50IG1hcCBpbml0aWFsaXphdGlvbiBiZWNhdXNlIGl0IGlzIHVubmVjZXNzYXJ5IGFmdGVyIHRoaXMuXG4gICAgICAgIGxvYWRlci5yZW1vdmVJbml0KCdnbWFwJywgdGhpcy5pbml0Q2FsbGJhY2spO1xuICAgICAgfVxuXG4gICAgICAvLyBSZW1vdmUgYWxsIG1hcmtlcnMgZnJvbSB0aGUgaW5zdGFuY2UuXG4gICAgICB0aGlzLm1hcmtlcnMuZm9yRWFjaCgobWFya2VyKSA9PiBtYXJrZXIuZGV0YWNoKCkpO1xuICAgICAgdGhpcy5tYXJrZXJzLmNsZWFyKCk7XG5cbiAgICAgIC8vIFJlbW92ZSBhbGwgZXZlbnQgbGlzdGVuZXJzLlxuICAgICAgdGhpcy5saXN0ZW5lcnMuY2xlYXIoKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogTWFpbnRhaW5zIGEgcG9vbCBvZiBnb29nbGUgbWFwIGluc3RhbmNlcyB0aGF0IGNhbiBiZSByZXVzZWQuXG4gICAqXG4gICAqIEdvb2dsZSBtYXBzIHN1Z2dlc3RzIHRoYXQgdGhlIG1hcCBzaG91bGQgYmUgcmVjeWNsZWQgcmF0aGVyIHRoYW5cbiAgICogaGF2ZSB0aGUgaW5zdGFuY2UgYW5kIGRhdGEgZGVsZXRlZC4gVGhpcyBrZWVwcyB0aGUgbGlzdCBvZiBtYXBzIHJlYWR5IHRvXG4gICAqIGJlIHJlY3ljbGVkLlxuICAgKi9cbiAgR01hcC5nbWFwUG9vbCA9IFtdO1xuXG4gIC8qKlxuICAgKiBBZGQgdGhlIE1hcGtpdCBoYW5kbGVyIGZvciBnZW5lcmF0aW5nIGEgR29vZ2xlIE1hcHMgaW5zdGFuY2UuXG4gICAqXG4gICAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IGVsXG4gICAqICAgVGhlIEhUTUwgZWxlbWVudCB0byBhdHRhY2ggdGhlIG1hcCB0by5cbiAgICogQHBhcmFtIHtPYmplY3R9IHNldHRpbmdzXG4gICAqICAgVGhlIG1hcCBzZXR0aW5ncyB0byB1c2Ugd2hlbiBnZW5lcmF0aW5nIHRoZSBtYXAuXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBtYXJrZXJCdWlsZGVyc1xuICAgKiAgIFRoZSBtYXJrZXIgZmFjdG9yeSBmdW5jdGlvbnMgZm9yIGNyZWF0aW5nIG1hcmtlcnMgZm9yIHRoaXMgbWFwLlxuICAgKlxuICAgKiBAcmV0dXJuIHtPYmplY3R9XG4gICAqICAgVGhlIGdlbmVyYXRlZCBNYXAgaW5zdGFuY2UuXG4gICAqL1xuICBNYXBraXQuaGFuZGxlci5nbWFwID0gZnVuY3Rpb24gY3JlYXRlR01hcChlbCwgc2V0dGluZ3MsIG1hcmtlckJ1aWxkZXJzKSB7XG4gICAgY29uc3QgaW5zdGFuY2UgPSBuZXcgR01hcChlbCwgc2V0dGluZ3MsIG1hcmtlckJ1aWxkZXJzKTtcbiAgICBjb25zdCBnbWFwID0gR01hcC5nbWFwUG9vbC5wb3AoKTtcblxuICAgIGlmIChnbWFwKSB7XG4gICAgICBpbnN0YW5jZS5pbml0KGdtYXApO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIC8vIFJlZ2lzdGVyIHRoaXMgbWFwIHRvIGdldCBpbml0aWFsaXplZCB3aGVuIEdvb2dsZSBtYXBzIGxvYWRzLlxuICAgICAgaW5zdGFuY2UuaW5pdENhbGxiYWNrID0gaW5zdGFuY2UuaW5pdC5iaW5kKGluc3RhbmNlKTtcbiAgICAgIGxvYWRlci5hZGRJbml0KCdnbWFwJywgaW5zdGFuY2UuaW5pdENhbGxiYWNrKTtcbiAgICB9XG4gICAgcmV0dXJuIGluc3RhbmNlO1xuICB9O1xufSkoRHJ1cGFsLCBNYXBraXQpO1xuIl0sIm1hcHBpbmdzIjoiOztBQUFBLENBQUMsQ0FBQztFQUFFQSxRQUFRLEVBQUVDLEVBQUU7RUFBRUMsTUFBTTtFQUFFQztBQUFTLENBQUMsRUFBRUMsTUFBTSxLQUFLO0VBQy9DO0FBQ0Y7QUFDQTtFQUNFLE1BQU1DLElBQUksQ0FBQztJQUNUQyxXQUFXLENBQUNDLEVBQUUsRUFBRUMsUUFBUSxFQUFFQyxjQUFjLEVBQUU7TUFDeEM7TUFDQTtNQUNBLElBQUksQ0FBQ0MsS0FBSyxHQUFHLElBQUk7TUFFakIsSUFBSSxDQUFDSCxFQUFFLEdBQUdBLEVBQUU7TUFDWixJQUFJLENBQUNJLE9BQU8sR0FBRyxJQUFJQyxHQUFHLEVBQUU7TUFDeEIsSUFBSSxDQUFDQyxVQUFVLEdBQUdMLFFBQVEsQ0FBQ00sZ0JBQWdCO01BQzNDLElBQUksQ0FBQ0wsY0FBYyxHQUFHQSxjQUFjOztNQUVwQztNQUNBLElBQUksQ0FBQ00sTUFBTSxHQUFHUCxRQUFRLENBQUNPLE1BQU07TUFDN0IsSUFBSSxDQUFDQyxTQUFTLEdBQUdSLFFBQVEsQ0FBQ1EsU0FBUyxJQUFJLEtBQUs7TUFDNUMsSUFBSSxDQUFDQyxlQUFlLEdBQUdULFFBQVEsQ0FBQ1UsY0FBYztNQUM5QyxJQUFJLENBQUNDLFVBQVUsR0FBRztRQUNoQkMsY0FBYyxFQUFFLEtBQUs7UUFDckJMLE1BQU0sRUFBRVAsUUFBUSxDQUFDTyxNQUFNLElBQUk7VUFBRU0sR0FBRyxFQUFFLENBQUM7VUFBRUMsR0FBRyxFQUFFO1FBQUUsQ0FBQztRQUM3Q0MsSUFBSSxFQUFFZixRQUFRLENBQUNlLElBQUksSUFBSSxFQUFFO1FBQ3pCQyxPQUFPLEVBQUVoQixRQUFRLENBQUNnQixPQUFPLElBQUksQ0FBQztRQUM5QkMsT0FBTyxFQUFFakIsUUFBUSxDQUFDaUIsT0FBTyxJQUFJLEVBQUU7UUFDL0JDLFNBQVMsRUFBRWxCLFFBQVEsQ0FBQ21CLFlBQVksSUFBSSxTQUFTO1FBQzdDQyxxQkFBcUIsRUFBRTtVQUNyQkMsVUFBVSxFQUFFckIsUUFBUSxDQUFDc0IsTUFBTSxJQUFJO1FBQ2pDLENBQUM7UUFDRDtRQUNBQyxNQUFNLEVBQUUsQ0FBQztVQUNQQyxXQUFXLEVBQUUsS0FBSztVQUNsQkMsV0FBVyxFQUFFLFFBQVE7VUFDckJDLE9BQU8sRUFBRSxDQUFDO1lBQUVDLFVBQVUsRUFBRTtVQUFNLENBQUM7UUFDakMsQ0FBQztNQUNILENBQUM7O01BRUQ7TUFDQSxJQUFJLENBQUNDLE1BQU0sR0FBRyxJQUFJOztNQUVsQjtNQUNBO01BQ0EsSUFBSSxDQUFDQyxhQUFhLEdBQUdsQyxRQUFRLENBQUMsTUFBTTtRQUNsQyxJQUFJLElBQUksQ0FBQ08sS0FBSyxFQUFFO1VBQ2QsSUFBSSxJQUFJLENBQUNDLE9BQU8sQ0FBQzJCLElBQUksR0FBRyxDQUFDLEVBQUU7WUFDekIsSUFBSSxDQUFDNUIsS0FBSyxDQUFDTSxTQUFTLENBQUMsSUFBSSxDQUFDb0IsTUFBTSxDQUFDO1VBQ25DLENBQUMsTUFDSSxJQUFJLElBQUksQ0FBQ3pCLE9BQU8sQ0FBQzJCLElBQUksS0FBSyxDQUFDLEVBQUU7WUFDaEMsSUFBSSxDQUFDNUIsS0FBSyxDQUFDNkIsU0FBUyxDQUFDLElBQUksQ0FBQ0gsTUFBTSxDQUFDSSxTQUFTLEVBQUUsQ0FBQztZQUM3QyxJQUFJLENBQUM5QixLQUFLLENBQUMrQixPQUFPLENBQUMsSUFBSSxDQUFDdEIsVUFBVSxDQUFDSSxJQUFJLENBQUM7VUFDMUM7UUFDRjtNQUNGLENBQUMsRUFBRSxHQUFHLENBQUM7O01BRVA7TUFDQSxJQUFJLENBQUNtQixTQUFTLEdBQUcsSUFBSTlCLEdBQUcsRUFBRTtJQUM1Qjs7SUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJK0IsSUFBSSxDQUFDQyxJQUFJLEdBQUcsSUFBSSxFQUFFO01BQ2hCLElBQUlBLElBQUksRUFBRTtRQUNSQSxJQUFJLENBQUNDLFVBQVUsQ0FBQyxJQUFJLENBQUMxQixVQUFVLENBQUM7UUFDaEMsSUFBSSxDQUFDVCxLQUFLLEdBQUdrQyxJQUFJO01BQ25CLENBQUMsTUFDSTtRQUNILE1BQU1FLElBQUksR0FBRyxJQUFJN0MsRUFBRSxDQUFDOEMsT0FBTyxDQUFDLEtBQUssRUFBRTtVQUFFQyxLQUFLLEVBQUU7WUFBRUMsTUFBTSxFQUFFO1VBQU87UUFBRSxDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDdkMsS0FBSyxHQUFHLElBQUl3QyxNQUFNLENBQUNDLElBQUksQ0FBQ3ZDLEdBQUcsQ0FBQ2tDLElBQUksQ0FBQ3ZDLEVBQUUsRUFBRSxJQUFJLENBQUNZLFVBQVUsQ0FBQztNQUM1RDs7TUFFQTtNQUNBLElBQUksQ0FBQ1osRUFBRSxDQUFDNkMsV0FBVyxDQUFDLElBQUksQ0FBQzFDLEtBQUssQ0FBQzJDLE1BQU0sRUFBRSxDQUFDO01BQ3hDLElBQUksQ0FBQ2pCLE1BQU0sR0FBRyxJQUFJYyxNQUFNLENBQUNDLElBQUksQ0FBQ0csWUFBWSxFQUFFO01BRTVDLElBQUksSUFBSSxDQUFDckMsZUFBZSxJQUFJc0MsTUFBTSxDQUFDQywyQkFBMkIsRUFBRTtRQUM5RCxJQUFJLENBQUNDLEdBQUcsR0FBRyxJQUFJRixNQUFNLENBQUNDLDJCQUEyQixDQUFDLElBQUksQ0FBQzlDLEtBQUssRUFBRSxJQUFJLENBQUNPLGVBQWUsQ0FBQztNQUNyRjs7TUFFQTtNQUNBO01BQ0EsSUFBSSxJQUFJLENBQUNOLE9BQU8sQ0FBQzJCLElBQUksRUFBRTtRQUNyQixJQUFJLENBQUMzQixPQUFPLENBQUMrQyxPQUFPLENBQUVDLENBQUMsSUFBSztVQUMxQkEsQ0FBQyxDQUFDaEIsSUFBSSxDQUFDLElBQUksQ0FBQztVQUNaLElBQUksQ0FBQ1AsTUFBTSxDQUFDd0IsTUFBTSxDQUFDRCxDQUFDLENBQUNFLFFBQVEsQ0FBQztRQUNoQyxDQUFDLENBQUM7TUFDSjtNQUVBLElBQUksSUFBSSxDQUFDN0MsU0FBUyxFQUFFO1FBQ2xCLE1BQU04QyxjQUFjLEdBQUdaLE1BQU0sQ0FBQ0MsSUFBSSxDQUFDWSxLQUFLLENBQUNDLFdBQVcsQ0FBQyxJQUFJLENBQUN0RCxLQUFLLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTTtVQUN2RixJQUFJLElBQUksQ0FBQ0gsRUFBRSxDQUFDMEQsV0FBVyxHQUFHLENBQUMsRUFBRTtZQUMzQixJQUFJLENBQUM1QixhQUFhLEVBQUU7WUFDcEJhLE1BQU0sQ0FBQ0MsSUFBSSxDQUFDWSxLQUFLLENBQUNHLGNBQWMsQ0FBQ0osY0FBYyxDQUFDO1VBQ2xEO1FBQ0YsQ0FBQyxDQUFDOztRQUVGO1FBQ0E7UUFDQSxJQUFJLElBQUksQ0FBQzNDLFVBQVUsQ0FBQ0ssT0FBTyxJQUFJLElBQUksQ0FBQ0wsVUFBVSxDQUFDTSxPQUFPLEVBQUU7VUFDdER5QixNQUFNLENBQUNDLElBQUksQ0FBQ1ksS0FBSyxDQUFDQyxXQUFXLENBQUMsSUFBSSxDQUFDdEQsS0FBSyxFQUFFLGNBQWMsRUFBRSxNQUFNO1lBQzlELE1BQU1hLElBQUksR0FBRyxJQUFJLENBQUNiLEtBQUssQ0FBQ3lELE9BQU8sRUFBRTtZQUVqQyxJQUFJLElBQUksQ0FBQ2hELFVBQVUsQ0FBQ0ssT0FBTyxJQUFJRCxJQUFJLEdBQUcsSUFBSSxDQUFDSixVQUFVLENBQUNLLE9BQU8sRUFBRTtjQUM3RCxJQUFJLENBQUNkLEtBQUssQ0FBQytCLE9BQU8sQ0FBQyxJQUFJLENBQUN0QixVQUFVLENBQUNLLE9BQU8sQ0FBQztZQUM3QyxDQUFDLE1BQ0ksSUFBSSxJQUFJLENBQUNMLFVBQVUsQ0FBQ00sT0FBTyxJQUFJRixJQUFJLEdBQUcsSUFBSSxDQUFDSixVQUFVLENBQUNNLE9BQU8sRUFBRTtjQUNsRSxJQUFJLENBQUNmLEtBQUssQ0FBQytCLE9BQU8sQ0FBQyxJQUFJLENBQUN0QixVQUFVLENBQUNNLE9BQU8sQ0FBQztZQUM3QztVQUNGLENBQUMsQ0FBQztRQUNKO01BQ0Y7O01BRUE7TUFDQTtNQUNBLElBQUksQ0FBQ2lCLFNBQVMsQ0FBQ2dCLE9BQU8sQ0FBQyxDQUFDVSxLQUFLLEVBQUVMLEtBQUssS0FBSztRQUN2QztRQUNBO1FBQ0EsSUFBSUEsS0FBSyxLQUFLLE1BQU0sRUFBRTtVQUNwQkssS0FBSyxDQUFDVixPQUFPLENBQUVXLElBQUksSUFBS0EsSUFBSSxDQUFDQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUMsQ0FBQyxNQUNJLElBQUlQLEtBQUssS0FBSyxTQUFTLEVBQUU7VUFDNUJLLEtBQUssQ0FBQ1YsT0FBTyxDQUFFVyxJQUFJLElBQUs7WUFDdEJBLElBQUksQ0FBQ0UsTUFBTSxHQUFHLElBQUksQ0FBQzdELEtBQUssQ0FBQ3NELFdBQVcsQ0FBQ0QsS0FBSyxFQUFFTSxJQUFJLENBQUNDLFFBQVEsQ0FBQztVQUM1RCxDQUFDLENBQUM7UUFDSjtNQUNGLENBQUMsQ0FBQzs7TUFFRjtNQUNBLElBQUksQ0FBQzVCLFNBQVMsQ0FBQzhCLE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDL0I7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSWhDLFNBQVMsR0FBRztNQUNWLElBQUksSUFBSSxDQUFDOUIsS0FBSyxFQUFFO1FBQ2QsTUFBTStELENBQUMsR0FBRyxJQUFJLENBQUMvRCxLQUFLLENBQUM4QixTQUFTLEVBQUU7UUFFaEMsT0FBTztVQUNMbkIsR0FBRyxFQUFFb0QsQ0FBQyxDQUFDcEQsR0FBRztVQUNWQyxHQUFHLEVBQUVtRCxDQUFDLENBQUNuRDtRQUNULENBQUM7TUFDSDs7TUFFQTtNQUNBO01BQ0E7TUFDQSxPQUFPLElBQUksQ0FBQ1AsTUFBTTtJQUNwQjs7SUFFQTtBQUNKO0FBQ0E7SUFDSTJELFVBQVUsR0FBRztNQUNYLElBQUksQ0FBQ3RDLE1BQU0sR0FBRyxJQUFJYyxNQUFNLENBQUNDLElBQUksQ0FBQ0csWUFBWSxFQUFFO01BRTVDLElBQUksQ0FBQzNDLE9BQU8sQ0FBQytDLE9BQU8sQ0FBRUMsQ0FBQyxJQUFLO1FBQzFCLElBQUksQ0FBQ3ZCLE1BQU0sQ0FBQ3dCLE1BQU0sQ0FBQ0QsQ0FBQyxDQUFDRSxRQUFRLENBQUM7TUFDaEMsQ0FBQyxDQUFDO0lBQ0o7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0ljLHFCQUFxQixDQUFDQyxTQUFTLEVBQUVDLE1BQU0sRUFBRTtNQUN2QyxJQUFJQyxPQUFPLEdBQUcsS0FBSztNQUVuQixJQUFJRCxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUN6QyxNQUFNLENBQUMyQyxRQUFRLENBQUNGLE1BQU0sQ0FBQyxFQUFFO1FBQzNDLElBQUksQ0FBQ3pDLE1BQU0sQ0FBQ3dCLE1BQU0sQ0FBQ2lCLE1BQU0sQ0FBQztRQUMxQkMsT0FBTyxHQUFHLElBQUk7TUFDaEI7O01BRUE7TUFDQTtNQUNBLE1BQU1FLEVBQUUsR0FBRyxJQUFJLENBQUM1QyxNQUFNLENBQUM2QyxZQUFZLEVBQUU7TUFDckMsTUFBTUMsRUFBRSxHQUFHLElBQUksQ0FBQzlDLE1BQU0sQ0FBQytDLFlBQVksRUFBRTtNQUVyQyxJQUFJLENBQUNILEVBQUUsQ0FBQzNELEdBQUcsRUFBRSxLQUFLdUQsU0FBUyxDQUFDdkQsR0FBRyxJQUFJNkQsRUFBRSxDQUFDN0QsR0FBRyxFQUFFLEtBQUt1RCxTQUFTLENBQUN2RCxHQUFHLE1BQ3ZEMkQsRUFBRSxDQUFDMUQsR0FBRyxFQUFFLEtBQUtzRCxTQUFTLENBQUN0RCxHQUFHLElBQUk0RCxFQUFFLENBQUM1RCxHQUFHLEVBQUUsS0FBS3NELFNBQVMsQ0FBQ3RELEdBQUcsQ0FBQyxFQUM3RDtRQUNBLElBQUksQ0FBQ29ELFVBQVUsRUFBRTtRQUNqQkksT0FBTyxHQUFHLElBQUk7TUFDaEI7O01BRUE7TUFDQSxJQUFJQSxPQUFPLEVBQUU7UUFDWCxJQUFJLElBQUksQ0FBQ25FLE9BQU8sQ0FBQzJCLElBQUksSUFBSSxDQUFDLEVBQUU7VUFDMUIsTUFBTW1DLENBQUMsR0FBRyxJQUFJLENBQUM5RCxPQUFPLENBQUMyQixJQUFJLEdBQUcsSUFBSSxDQUFDRixNQUFNLENBQUNJLFNBQVMsRUFBRSxHQUFHLElBQUksQ0FBQ3pCLE1BQU07VUFDbkUsSUFBSSxDQUFDd0IsU0FBUyxDQUFDa0MsQ0FBQyxDQUFDO1FBQ25CLENBQUMsTUFDSSxJQUFJLElBQUksQ0FBQ3pELFNBQVMsRUFBRTtVQUN2QixJQUFJLENBQUNxQixhQUFhLEVBQUU7UUFDdEI7TUFDRjtJQUNGOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSStDLGVBQWUsQ0FBQ0MsTUFBTSxFQUFFQyxPQUFPLEVBQUU7TUFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQ0MsT0FBTyxFQUFFO1FBQ2pCLElBQUksQ0FBQ0EsT0FBTyxHQUFHLElBQUlyQyxNQUFNLENBQUNDLElBQUksQ0FBQ3FDLFVBQVUsRUFBRTtNQUM3QztNQUVBLElBQUksQ0FBQ0QsT0FBTyxDQUFDRSxVQUFVLENBQUNILE9BQU8sQ0FBQztNQUNoQyxJQUFJLENBQUNDLE9BQU8sQ0FBQ0csSUFBSSxDQUFDO1FBQ2hCQyxHQUFHLEVBQUUsSUFBSSxDQUFDakYsS0FBSztRQUNma0YsTUFBTSxFQUFFUCxNQUFNLENBQUNRO01BQ2pCLENBQUMsQ0FBQztJQUNKOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJdEQsU0FBUyxDQUFDeEIsTUFBTSxFQUFFO01BQ2hCLElBQUksQ0FBQ0EsTUFBTSxHQUFHQSxNQUFNO01BRXBCLElBQUksSUFBSSxDQUFDTCxLQUFLLEVBQUU7UUFDZCxJQUFJLENBQUNBLEtBQUssQ0FBQzZCLFNBQVMsQ0FBQ3hCLE1BQU0sQ0FBQztNQUM5QjtJQUNGOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJK0UsWUFBWSxDQUFDekIsSUFBSSxFQUFFMEIsU0FBUyxHQUFHLElBQUksRUFBRTtNQUNuQyxNQUFNQyxJQUFJLEdBQUczQixJQUFJLENBQUM0QixLQUFLLElBQUksSUFBSSxDQUFDeEYsY0FBYyxDQUFDNEQsSUFBSSxDQUFDNEIsS0FBSyxDQUFDLEdBQ3RENUIsSUFBSSxDQUFDNEIsS0FBSyxHQUFHLElBQUksQ0FBQ3BGLFVBQVU7O01BRWhDO01BQ0EsSUFBSSxJQUFJLENBQUNKLGNBQWMsQ0FBQ3VGLElBQUksQ0FBQyxFQUFFO1FBQzdCLE1BQU1YLE1BQU0sR0FBRyxJQUFJLENBQUM1RSxjQUFjLENBQUN1RixJQUFJLENBQUMsQ0FBQzNCLElBQUksQ0FBQzs7UUFFOUM7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLElBQUksSUFBSSxDQUFDM0QsS0FBSyxFQUFFO1VBQ2QyRSxNQUFNLENBQUMxQyxJQUFJLENBQUMsSUFBSSxDQUFDOztVQUVqQjtVQUNBO1VBQ0EsSUFBSSxDQUFDLElBQUksQ0FBQ1AsTUFBTSxDQUFDMkMsUUFBUSxDQUFDTSxNQUFNLENBQUN4QixRQUFRLENBQUMsRUFBRTtZQUMxQyxJQUFJLENBQUN6QixNQUFNLENBQUN3QixNQUFNLENBQUN5QixNQUFNLENBQUN4QixRQUFRLENBQUM7WUFFbkMsSUFBSWtDLFNBQVMsSUFBSSxJQUFJLENBQUMvRSxTQUFTLElBQUksSUFBSSxDQUFDTCxPQUFPLENBQUMyQixJQUFJLEVBQUU7Y0FDcEQsSUFBSSxDQUFDRCxhQUFhLEVBQUU7WUFDdEI7VUFDRjtRQUNGO1FBRUEsSUFBSSxDQUFDMUIsT0FBTyxDQUFDdUYsR0FBRyxDQUFDYixNQUFNLEVBQUVBLE1BQU0sQ0FBQztRQUNoQyxPQUFPQSxNQUFNO01BQ2Y7SUFDRjs7SUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0ljLFlBQVksQ0FBQ2QsTUFBTSxFQUFFVSxTQUFTLEdBQUcsSUFBSSxFQUFFO01BQ3JDLElBQUksQ0FBQ3BGLE9BQU8sQ0FBQzZELE1BQU0sQ0FBQ2EsTUFBTSxDQUFDOztNQUUzQjtNQUNBLElBQUksSUFBSSxDQUFDM0UsS0FBSyxFQUFFO1FBQ2QyRSxNQUFNLENBQUNlLE1BQU0sRUFBRTtRQUVmLElBQUlMLFNBQVMsRUFBRTtVQUNiO1VBQ0EsSUFBSSxDQUFDcEIscUJBQXFCLENBQUNVLE1BQU0sQ0FBQ3hCLFFBQVEsQ0FBQztRQUM3QztNQUNGO0lBQ0Y7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSXdDLEVBQUUsQ0FBQ3RDLEtBQUssRUFBRU8sUUFBUSxFQUFFO01BQ2xCO01BQ0E7TUFDQTtNQUNBLElBQUlQLEtBQUssS0FBSyxNQUFNLElBQUksSUFBSSxDQUFDckQsS0FBSyxFQUFFO1FBQ2xDNEQsUUFBUSxDQUFDLElBQUksQ0FBQztNQUNoQixDQUFDLE1BQ0k7UUFDSCxNQUFNRCxJQUFJLEdBQUc7VUFBRUMsUUFBUTtVQUFFQyxNQUFNLEVBQUU7UUFBSyxDQUFDO1FBRXZDLElBQUksSUFBSSxDQUFDN0IsU0FBUyxDQUFDNEQsR0FBRyxDQUFDdkMsS0FBSyxDQUFDLEVBQUU7VUFDN0IsSUFBSSxDQUFDckIsU0FBUyxDQUFDNkQsR0FBRyxDQUFDeEMsS0FBSyxDQUFDLENBQUN5QyxJQUFJLENBQUNuQyxJQUFJLENBQUM7UUFDdEMsQ0FBQyxNQUNJO1VBQ0gsSUFBSSxDQUFDM0IsU0FBUyxDQUFDd0QsR0FBRyxDQUFDbkMsS0FBSyxFQUFFLENBQUNNLElBQUksQ0FBQyxDQUFDO1FBQ25DOztRQUVBO1FBQ0EsSUFBSSxJQUFJLENBQUMzRCxLQUFLLElBQUlxRCxLQUFLLEtBQUssU0FBUyxFQUFFO1VBQ3JDTSxJQUFJLENBQUNFLE1BQU0sR0FBRyxJQUFJLENBQUM3RCxLQUFLLENBQUNzRCxXQUFXLENBQUNELEtBQUssRUFBRU8sUUFBUSxDQUFDO1FBQ3ZEO01BQ0Y7TUFFQSxPQUFPLElBQUk7SUFDYjs7SUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSW1DLEdBQUcsQ0FBQzFDLEtBQUssRUFBRU8sUUFBUSxHQUFHLElBQUksRUFBRTtNQUMxQjtNQUNBLElBQUksQ0FBQ0EsUUFBUSxFQUFFO1FBQ2IsSUFBSVAsS0FBSyxLQUFLLE1BQU0sSUFBSUEsS0FBSyxLQUFLLFNBQVMsRUFBRTtVQUMzQyxDQUFDLElBQUksQ0FBQ3JCLFNBQVMsQ0FBQzZELEdBQUcsQ0FBQ3hDLEtBQUssQ0FBQyxJQUFJLEVBQUUsRUFBRUwsT0FBTyxDQUFFVyxJQUFJLElBQUs7WUFDbEQsSUFBSUEsSUFBSSxDQUFDRSxNQUFNLEVBQUVyQixNQUFNLENBQUNDLElBQUksQ0FBQ1ksS0FBSyxDQUFDRyxjQUFjLENBQUNHLElBQUksQ0FBQ0UsTUFBTSxDQUFDO1VBQ2hFLENBQUMsQ0FBQztRQUNKO1FBRUEsSUFBSSxDQUFDN0IsU0FBUyxDQUFDOEIsTUFBTSxDQUFDVCxLQUFLLENBQUM7TUFDOUIsQ0FBQyxNQUNJO1FBQ0gsTUFBTXJCLFNBQVMsR0FBRyxJQUFJLENBQUNBLFNBQVMsQ0FBQzZELEdBQUcsQ0FBQ3hDLEtBQUssQ0FBQyxJQUFJLEVBQUU7O1FBRWpEO1FBQ0EsS0FBSyxJQUFJMkMsQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHaEUsU0FBUyxDQUFDaUUsTUFBTSxFQUFFLEVBQUVELENBQUMsRUFBRTtVQUN6QyxNQUFNckMsSUFBSSxHQUFHLElBQUksQ0FBQzNCLFNBQVMsQ0FBQ2dFLENBQUMsQ0FBQztVQUU5QixJQUFJckMsSUFBSSxDQUFDQyxRQUFRLEtBQUtBLFFBQVEsSUFBSUQsSUFBSSxDQUFDRSxNQUFNLEtBQUtELFFBQVEsRUFBRTtZQUMxRCxJQUFJNUIsU0FBUyxDQUFDaUUsTUFBTSxJQUFJLENBQUMsRUFBRTtjQUN6QixJQUFJLENBQUNqRSxTQUFTLENBQUM4QixNQUFNLENBQUNULEtBQUssQ0FBQztZQUM5QixDQUFDLE1BQ0k7Y0FDSHJCLFNBQVMsQ0FBQ2tFLE1BQU0sQ0FBQ0YsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN4QjtZQUVBLElBQUlyQyxJQUFJLENBQUNFLE1BQU0sRUFBRTtjQUNmckIsTUFBTSxDQUFDQyxJQUFJLENBQUNZLEtBQUssQ0FBQ0csY0FBYyxDQUFDRyxJQUFJLENBQUNFLE1BQU0sQ0FBQztZQUMvQztZQUNBO1VBQ0Y7UUFDRjtNQUNGO01BQ0EsT0FBTyxJQUFJO0lBQ2I7O0lBRUE7QUFDSjtBQUNBO0lBQ0lzQyxPQUFPLEdBQUc7TUFDUixJQUFJLElBQUksQ0FBQ25HLEtBQUssRUFBRTtRQUNkO1FBQ0E7UUFDQSxDQUFDLElBQUksQ0FBQ2dDLFNBQVMsQ0FBQzZELEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLEVBQUU3QyxPQUFPLENBQUVXLElBQUksSUFBS0EsSUFBSSxDQUFDQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7O1FBRTVFO1FBQ0E7UUFDQTtRQUNBLElBQUksQ0FBQzVCLFNBQVMsQ0FBQ2dCLE9BQU8sQ0FBQyxDQUFDb0QsSUFBSSxFQUFFL0MsS0FBSyxLQUFLLElBQUksQ0FBQzBDLEdBQUcsQ0FBQzFDLEtBQUssQ0FBQyxDQUFDOztRQUV4RDtRQUNBO1FBQ0E7UUFDQSxJQUFJLENBQUN4RCxFQUFFLENBQUN3RyxXQUFXLENBQUMsSUFBSSxDQUFDckcsS0FBSyxDQUFDMkMsTUFBTSxFQUFFLENBQUM7UUFDeENoRCxJQUFJLENBQUMyRyxRQUFRLENBQUNSLElBQUksQ0FBQyxJQUFJLENBQUM5RixLQUFLLENBQUM7TUFDaEMsQ0FBQyxNQUNJLElBQUksSUFBSSxDQUFDdUcsWUFBWSxFQUFFO1FBQzFCO1FBQ0E3RyxNQUFNLENBQUM4RyxVQUFVLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQ0QsWUFBWSxDQUFDO01BQzlDOztNQUVBO01BQ0EsSUFBSSxDQUFDdEcsT0FBTyxDQUFDK0MsT0FBTyxDQUFFMkIsTUFBTSxJQUFLQSxNQUFNLENBQUNlLE1BQU0sRUFBRSxDQUFDO01BQ2pELElBQUksQ0FBQ3pGLE9BQU8sQ0FBQ3dHLEtBQUssRUFBRTs7TUFFcEI7TUFDQSxJQUFJLENBQUN6RSxTQUFTLENBQUN5RSxLQUFLLEVBQUU7SUFDeEI7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFOUcsSUFBSSxDQUFDMkcsUUFBUSxHQUFHLEVBQUU7O0VBRWxCO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0U5RyxNQUFNLENBQUNrSCxPQUFPLENBQUN4RSxJQUFJLEdBQUcsU0FBU3lFLFVBQVUsQ0FBQzlHLEVBQUUsRUFBRUMsUUFBUSxFQUFFQyxjQUFjLEVBQUU7SUFDdEUsTUFBTTZHLFFBQVEsR0FBRyxJQUFJakgsSUFBSSxDQUFDRSxFQUFFLEVBQUVDLFFBQVEsRUFBRUMsY0FBYyxDQUFDO0lBQ3ZELE1BQU1tQyxJQUFJLEdBQUd2QyxJQUFJLENBQUMyRyxRQUFRLENBQUNPLEdBQUcsRUFBRTtJQUVoQyxJQUFJM0UsSUFBSSxFQUFFO01BQ1IwRSxRQUFRLENBQUMzRSxJQUFJLENBQUNDLElBQUksQ0FBQztJQUNyQixDQUFDLE1BQ0k7TUFDSDtNQUNBMEUsUUFBUSxDQUFDTCxZQUFZLEdBQUdLLFFBQVEsQ0FBQzNFLElBQUksQ0FBQzZFLElBQUksQ0FBQ0YsUUFBUSxDQUFDO01BQ3BEbEgsTUFBTSxDQUFDcUgsT0FBTyxDQUFDLE1BQU0sRUFBRUgsUUFBUSxDQUFDTCxZQUFZLENBQUM7SUFDL0M7SUFDQSxPQUFPSyxRQUFRO0VBQ2pCLENBQUM7QUFDSCxDQUFDLEVBQUVJLE1BQU0sRUFBRXhILE1BQU0sQ0FBQyJ9
