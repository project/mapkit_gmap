"use strict";

(({
  Toolshed: ts
}, Mapkit) => {
  /**
   * Generates the Places field and input data bind callback for HTML input
   * elements.
   *
   * @param {string} key
   *   The data key which identifies which part of the address property to bind
   *   to this input element.
   * @param {HTMLInputElement} input
   *   The input element to create the data binding for.
   *
   * @return {*}
   *   An object with a "field" and "callback" method for. The "field" key tells
   *   The Places API what data is requested, and the callback is a method
   *   which is bound to the input element and will populate it with the
   *   value from the requested data property (key).
   */
  function buildInputCallback(key, input) {
    switch (key) {
      case 'country':
      case 'postal_code':
      case 'administrative_area_level_1':
      case 'administrative_area_level_2':
      case 'locality':
      case 'street_address':
        return {
          field: 'address_component',
          callback: place => {
            let val = '';
            place.address_components.forEach(({
              short_name: name,
              types
            }) => {
              if (types.indexOf(key) >= 0) val = name;
            });
            input.value = val;
          }
        };
      case 'latlng':
        return {
          field: 'geometry.location',
          callback: place => {
            const loc = (place.geometry || {}).location;
            input.value = loc ? `${loc.lat().toFixed(7)},${loc.lng().toFixed(7)}` : '';
          }
        };
      default:
        return {};
    }
  }

  /**
   * Provide autocomplete suggestions and location resolution through the
   * Google Maps Places API.
   */
  Mapkit.ac.gmapPlaces = class {
    /**
     * Create a new Google Maps Places handler for an autocomplete instance.
     *
     * @param {Mapkit.Autocomplete} ac
     *   Reference to the Mapkit.Autocomplete instance that uses this handler.
     */
    constructor(ac) {
      this.loaderId = 'gmap';
      this.ac = ac;

      // Initialize the request objects.
      this.reqFields = [];
      this.inputs = [];
    }

    /**
     * Init method should be called after the map libraries have been loaded.
     *
     * Sets up the Google Maps Places services and creates the request constants
     * from the configurations.
     */
    init() {
      const attribution = new ts.Element('footer', {}, this.ac.suggestWrap);
      attribution.addClass(['attributions', 'attributions--google']);

      // Create the Google Maps services after the libraries have been loaded.
      this.acService = new google.maps.places.AutocompleteService();
      this.placesAPI = new google.maps.places.PlacesService(attribution.el);
      this.reqOptions = {};
      const {
        matchType: type,
        countries,
        bounds,
        pattern
      } = this.ac.config;
      if (type === 'address') {
        this.reqOptions.types = ['address'];
      } else if (type === 'city') {
        this.reqOptions.types = ['(cities)'];
      } else if (type === 'region') {
        this.reqOptions.types = ['(regions)'];
      }
      if (pattern) {
        this.filterRegEx = new RegExp(pattern, 'i');
      }
      if (countries) {
        this.reqOptions.componentRestrictions = {
          country: countries
        };
      }
      if (bounds && bounds.bottom && bounds.top) {
        const bKey = bounds.type === 'restriction' ? 'locationRestriction' : 'locationBias';
        this.reqOptions[bKey] = new google.maps.LatLngBounds(new google.maps.LatLng(bounds.bottom, bounds.right), new google.maps.LatLng(bounds.top, bounds.left));
      }
    }

    /**
     * Is this handler ready to make fetch suggestions and resolve locations?
     *
     * @return {bool}
     *   TRUE if the service is ready to make autocomplete and places API calls.
     */
    isReady() {
      return this.acService && this.placesAPI;
    }

    /**
     * Attached the data callback to populate values from the Places API into
     * input elements. This allows us to fill in the values when calling
     * this.select().
     *
     * @param {string} key
     *   The data key for the location property this input populates.
     * @param {HTMLInputElement} input
     *   The HTML input element to attached the data callback to.
     */
    attachInput(key, input) {
      const {
        field,
        callback
      } = buildInputCallback(key, input);

      // Translates to a valid places value and value update callback.
      if (field && callback) {
        this.inputs.push({
          el: input,
          method: callback
        });
        if (this.reqFields.indexOf(field) < 0) {
          this.reqFields.push(field);
        }
      }
    }

    /**
     * Clears the values from the managed input elements.
     *
     * This ensures that no residual data is kept if the user changes the text
     * entry without selecting a places suggestion.
     */
    clearInputValues() {
      this.inputs.forEach(({
        el
      }) => {
        el.value = '';
      });
    }

    /**
     * User has selected an autocomplete suggestion, update input values and
     * apply the values from the Places API from the selected item.
     *
     * @param {*} item
     *   The autocomplete suggested item selected by the user.
     */
    select(item) {
      if (!this.reqFields.length) {
        // No fields are requested from places, no need to make the call.
        return;
      }
      const req = {
        placeId: item.value.id,
        sessionToken: this.sessionToken,
        fields: this.placeFields
      };

      // Token is being used in a place details call, we need a new token
      // for successive autocomplete calls.
      delete this.sessionToken;

      // Get additional places data if additional data fields are configured.
      this.placesAPI.getDetails(req, (place, status) => {
        if (google.maps.places.PlacesServiceStatus.OK === status) {
          this.inputs.forEach(input => input.method(place));
        }
      });
    }

    /**
     * Format the result from the API response into autocomplete suggest text.
     *
     * @param {*} item
     *   The data item returned by the Places service.
     *
     * @return {string}
     *   The text to display as the autocomplete suggestion.
     */
    formatSuggestText(item) {
      return item.description;
    }

    /**
     * Make request to the Places API to get location suggestions for
     * autocomplete suggestions display.
     *
     * @param {string} text
     *   The text to query for matching places suggestions.
     */
    fetchSuggestions(text) {
      // Create and keep a session token for searches to try to optimize
      // billing. This session is valid until we make a Places Detail API call
      // in the Autocomplete.select() method to get full places results.
      if (!this.sessionToken) {
        this.sessionToken = new google.maps.places.AutocompleteSessionToken();
      }
      const req = {
        input: text,
        sessionToken: this.sessionToken,
        ...this.reqOptions
      };
      this.acService.getPlacePredictions(req, (predictions, status) => {
        this.ac.pending = false;
        let suggest = null;
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          const data = [];
          predictions.forEach(item => {
            if (!this.filterRegEx || this.filterRegEx.test(item.description)) {
              data.push({
                text: this.formatSuggestText(item),
                value: {
                  types: item.types,
                  id: item.place_id
                }
              });
            }
          });
          suggest = {
            list: data
          };
        } else if (google.maps.places.PlacesServiceStatus.ZERO_RESULTS === status) {
          suggest = null;
        }

        // Let the autocomplete elements know that we are done fetching results.
        this.ac.setFetchedSuggestions(suggest);
      });
    }

    /**
     * Clear any references and get ready to detach this autocomplete handler.
     */
    destroy() {
      delete this.sessionToken;
    }
  };
})(Drupal, Mapkit);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGxhY2VzLWF1dG9jb21wbGV0ZS5qcyIsIm5hbWVzIjpbIlRvb2xzaGVkIiwidHMiLCJNYXBraXQiLCJidWlsZElucHV0Q2FsbGJhY2siLCJrZXkiLCJpbnB1dCIsImZpZWxkIiwiY2FsbGJhY2siLCJwbGFjZSIsInZhbCIsImFkZHJlc3NfY29tcG9uZW50cyIsImZvckVhY2giLCJzaG9ydF9uYW1lIiwibmFtZSIsInR5cGVzIiwiaW5kZXhPZiIsInZhbHVlIiwibG9jIiwiZ2VvbWV0cnkiLCJsb2NhdGlvbiIsImxhdCIsInRvRml4ZWQiLCJsbmciLCJhYyIsImdtYXBQbGFjZXMiLCJjb25zdHJ1Y3RvciIsImxvYWRlcklkIiwicmVxRmllbGRzIiwiaW5wdXRzIiwiaW5pdCIsImF0dHJpYnV0aW9uIiwiRWxlbWVudCIsInN1Z2dlc3RXcmFwIiwiYWRkQ2xhc3MiLCJhY1NlcnZpY2UiLCJnb29nbGUiLCJtYXBzIiwicGxhY2VzIiwiQXV0b2NvbXBsZXRlU2VydmljZSIsInBsYWNlc0FQSSIsIlBsYWNlc1NlcnZpY2UiLCJlbCIsInJlcU9wdGlvbnMiLCJtYXRjaFR5cGUiLCJ0eXBlIiwiY291bnRyaWVzIiwiYm91bmRzIiwicGF0dGVybiIsImNvbmZpZyIsImZpbHRlclJlZ0V4IiwiUmVnRXhwIiwiY29tcG9uZW50UmVzdHJpY3Rpb25zIiwiY291bnRyeSIsImJvdHRvbSIsInRvcCIsImJLZXkiLCJMYXRMbmdCb3VuZHMiLCJMYXRMbmciLCJyaWdodCIsImxlZnQiLCJpc1JlYWR5IiwiYXR0YWNoSW5wdXQiLCJwdXNoIiwibWV0aG9kIiwiY2xlYXJJbnB1dFZhbHVlcyIsInNlbGVjdCIsIml0ZW0iLCJsZW5ndGgiLCJyZXEiLCJwbGFjZUlkIiwiaWQiLCJzZXNzaW9uVG9rZW4iLCJmaWVsZHMiLCJwbGFjZUZpZWxkcyIsImdldERldGFpbHMiLCJzdGF0dXMiLCJQbGFjZXNTZXJ2aWNlU3RhdHVzIiwiT0siLCJmb3JtYXRTdWdnZXN0VGV4dCIsImRlc2NyaXB0aW9uIiwiZmV0Y2hTdWdnZXN0aW9ucyIsInRleHQiLCJBdXRvY29tcGxldGVTZXNzaW9uVG9rZW4iLCJnZXRQbGFjZVByZWRpY3Rpb25zIiwicHJlZGljdGlvbnMiLCJwZW5kaW5nIiwic3VnZ2VzdCIsImRhdGEiLCJ0ZXN0IiwicGxhY2VfaWQiLCJsaXN0IiwiWkVST19SRVNVTFRTIiwic2V0RmV0Y2hlZFN1Z2dlc3Rpb25zIiwiZGVzdHJveSIsIkRydXBhbCJdLCJzb3VyY2VzIjpbInBsYWNlcy1hdXRvY29tcGxldGUuZXM2LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIigoeyBUb29sc2hlZDogdHMgfSwgTWFwa2l0KSA9PiB7XG4gIC8qKlxuICAgKiBHZW5lcmF0ZXMgdGhlIFBsYWNlcyBmaWVsZCBhbmQgaW5wdXQgZGF0YSBiaW5kIGNhbGxiYWNrIGZvciBIVE1MIGlucHV0XG4gICAqIGVsZW1lbnRzLlxuICAgKlxuICAgKiBAcGFyYW0ge3N0cmluZ30ga2V5XG4gICAqICAgVGhlIGRhdGEga2V5IHdoaWNoIGlkZW50aWZpZXMgd2hpY2ggcGFydCBvZiB0aGUgYWRkcmVzcyBwcm9wZXJ0eSB0byBiaW5kXG4gICAqICAgdG8gdGhpcyBpbnB1dCBlbGVtZW50LlxuICAgKiBAcGFyYW0ge0hUTUxJbnB1dEVsZW1lbnR9IGlucHV0XG4gICAqICAgVGhlIGlucHV0IGVsZW1lbnQgdG8gY3JlYXRlIHRoZSBkYXRhIGJpbmRpbmcgZm9yLlxuICAgKlxuICAgKiBAcmV0dXJuIHsqfVxuICAgKiAgIEFuIG9iamVjdCB3aXRoIGEgXCJmaWVsZFwiIGFuZCBcImNhbGxiYWNrXCIgbWV0aG9kIGZvci4gVGhlIFwiZmllbGRcIiBrZXkgdGVsbHNcbiAgICogICBUaGUgUGxhY2VzIEFQSSB3aGF0IGRhdGEgaXMgcmVxdWVzdGVkLCBhbmQgdGhlIGNhbGxiYWNrIGlzIGEgbWV0aG9kXG4gICAqICAgd2hpY2ggaXMgYm91bmQgdG8gdGhlIGlucHV0IGVsZW1lbnQgYW5kIHdpbGwgcG9wdWxhdGUgaXQgd2l0aCB0aGVcbiAgICogICB2YWx1ZSBmcm9tIHRoZSByZXF1ZXN0ZWQgZGF0YSBwcm9wZXJ0eSAoa2V5KS5cbiAgICovXG4gIGZ1bmN0aW9uIGJ1aWxkSW5wdXRDYWxsYmFjayhrZXksIGlucHV0KSB7XG4gICAgc3dpdGNoIChrZXkpIHtcbiAgICAgIGNhc2UgJ2NvdW50cnknOlxuICAgICAgY2FzZSAncG9zdGFsX2NvZGUnOlxuICAgICAgY2FzZSAnYWRtaW5pc3RyYXRpdmVfYXJlYV9sZXZlbF8xJzpcbiAgICAgIGNhc2UgJ2FkbWluaXN0cmF0aXZlX2FyZWFfbGV2ZWxfMic6XG4gICAgICBjYXNlICdsb2NhbGl0eSc6XG4gICAgICBjYXNlICdzdHJlZXRfYWRkcmVzcyc6XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgZmllbGQ6ICdhZGRyZXNzX2NvbXBvbmVudCcsXG4gICAgICAgICAgY2FsbGJhY2s6IChwbGFjZSkgPT4ge1xuICAgICAgICAgICAgbGV0IHZhbCA9ICcnO1xuICAgICAgICAgICAgcGxhY2UuYWRkcmVzc19jb21wb25lbnRzLmZvckVhY2goKHsgc2hvcnRfbmFtZTogbmFtZSwgdHlwZXMgfSkgPT4ge1xuICAgICAgICAgICAgICBpZiAodHlwZXMuaW5kZXhPZihrZXkpID49IDApIHZhbCA9IG5hbWU7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgaW5wdXQudmFsdWUgPSB2YWw7XG4gICAgICAgICAgfSxcbiAgICAgICAgfTtcblxuICAgICAgY2FzZSAnbGF0bG5nJzpcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBmaWVsZDogJ2dlb21ldHJ5LmxvY2F0aW9uJyxcbiAgICAgICAgICBjYWxsYmFjazogKHBsYWNlKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBsb2MgPSAocGxhY2UuZ2VvbWV0cnkgfHwge30pLmxvY2F0aW9uO1xuICAgICAgICAgICAgaW5wdXQudmFsdWUgPSBsb2MgPyBgJHtsb2MubGF0KCkudG9GaXhlZCg3KX0sJHtsb2MubG5nKCkudG9GaXhlZCg3KX1gIDogJyc7XG4gICAgICAgICAgfSxcbiAgICAgICAgfTtcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuIHt9O1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBQcm92aWRlIGF1dG9jb21wbGV0ZSBzdWdnZXN0aW9ucyBhbmQgbG9jYXRpb24gcmVzb2x1dGlvbiB0aHJvdWdoIHRoZVxuICAgKiBHb29nbGUgTWFwcyBQbGFjZXMgQVBJLlxuICAgKi9cbiAgTWFwa2l0LmFjLmdtYXBQbGFjZXMgPSBjbGFzcyB7XG4gICAgLyoqXG4gICAgICogQ3JlYXRlIGEgbmV3IEdvb2dsZSBNYXBzIFBsYWNlcyBoYW5kbGVyIGZvciBhbiBhdXRvY29tcGxldGUgaW5zdGFuY2UuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge01hcGtpdC5BdXRvY29tcGxldGV9IGFjXG4gICAgICogICBSZWZlcmVuY2UgdG8gdGhlIE1hcGtpdC5BdXRvY29tcGxldGUgaW5zdGFuY2UgdGhhdCB1c2VzIHRoaXMgaGFuZGxlci5cbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcihhYykge1xuICAgICAgdGhpcy5sb2FkZXJJZCA9ICdnbWFwJztcbiAgICAgIHRoaXMuYWMgPSBhYztcblxuICAgICAgLy8gSW5pdGlhbGl6ZSB0aGUgcmVxdWVzdCBvYmplY3RzLlxuICAgICAgdGhpcy5yZXFGaWVsZHMgPSBbXTtcbiAgICAgIHRoaXMuaW5wdXRzID0gW107XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSW5pdCBtZXRob2Qgc2hvdWxkIGJlIGNhbGxlZCBhZnRlciB0aGUgbWFwIGxpYnJhcmllcyBoYXZlIGJlZW4gbG9hZGVkLlxuICAgICAqXG4gICAgICogU2V0cyB1cCB0aGUgR29vZ2xlIE1hcHMgUGxhY2VzIHNlcnZpY2VzIGFuZCBjcmVhdGVzIHRoZSByZXF1ZXN0IGNvbnN0YW50c1xuICAgICAqIGZyb20gdGhlIGNvbmZpZ3VyYXRpb25zLlxuICAgICAqL1xuICAgIGluaXQoKSB7XG4gICAgICBjb25zdCBhdHRyaWJ1dGlvbiA9IG5ldyB0cy5FbGVtZW50KCdmb290ZXInLCB7fSwgdGhpcy5hYy5zdWdnZXN0V3JhcCk7XG4gICAgICBhdHRyaWJ1dGlvbi5hZGRDbGFzcyhbJ2F0dHJpYnV0aW9ucycsICdhdHRyaWJ1dGlvbnMtLWdvb2dsZSddKTtcblxuICAgICAgLy8gQ3JlYXRlIHRoZSBHb29nbGUgTWFwcyBzZXJ2aWNlcyBhZnRlciB0aGUgbGlicmFyaWVzIGhhdmUgYmVlbiBsb2FkZWQuXG4gICAgICB0aGlzLmFjU2VydmljZSA9IG5ldyBnb29nbGUubWFwcy5wbGFjZXMuQXV0b2NvbXBsZXRlU2VydmljZSgpO1xuICAgICAgdGhpcy5wbGFjZXNBUEkgPSBuZXcgZ29vZ2xlLm1hcHMucGxhY2VzLlBsYWNlc1NlcnZpY2UoYXR0cmlidXRpb24uZWwpO1xuXG4gICAgICB0aGlzLnJlcU9wdGlvbnMgPSB7IH07XG5cbiAgICAgIGNvbnN0IHtcbiAgICAgICAgbWF0Y2hUeXBlOiB0eXBlLFxuICAgICAgICBjb3VudHJpZXMsXG4gICAgICAgIGJvdW5kcyxcbiAgICAgICAgcGF0dGVybixcbiAgICAgIH0gPSB0aGlzLmFjLmNvbmZpZztcblxuICAgICAgaWYgKHR5cGUgPT09ICdhZGRyZXNzJykge1xuICAgICAgICB0aGlzLnJlcU9wdGlvbnMudHlwZXMgPSBbJ2FkZHJlc3MnXTtcbiAgICAgIH1cbiAgICAgIGVsc2UgaWYgKHR5cGUgPT09ICdjaXR5Jykge1xuICAgICAgICB0aGlzLnJlcU9wdGlvbnMudHlwZXMgPSBbJyhjaXRpZXMpJ107XG4gICAgICB9XG4gICAgICBlbHNlIGlmICh0eXBlID09PSAncmVnaW9uJykge1xuICAgICAgICB0aGlzLnJlcU9wdGlvbnMudHlwZXMgPSBbJyhyZWdpb25zKSddO1xuICAgICAgfVxuXG4gICAgICBpZiAocGF0dGVybikge1xuICAgICAgICB0aGlzLmZpbHRlclJlZ0V4ID0gbmV3IFJlZ0V4cChwYXR0ZXJuLCAnaScpO1xuICAgICAgfVxuXG4gICAgICBpZiAoY291bnRyaWVzKSB7XG4gICAgICAgIHRoaXMucmVxT3B0aW9ucy5jb21wb25lbnRSZXN0cmljdGlvbnMgPSB7IGNvdW50cnk6IGNvdW50cmllcyB9O1xuICAgICAgfVxuXG4gICAgICBpZiAoYm91bmRzICYmIGJvdW5kcy5ib3R0b20gJiYgYm91bmRzLnRvcCkge1xuICAgICAgICBjb25zdCBiS2V5ID0gYm91bmRzLnR5cGUgPT09ICdyZXN0cmljdGlvbicgPyAnbG9jYXRpb25SZXN0cmljdGlvbicgOiAnbG9jYXRpb25CaWFzJztcbiAgICAgICAgdGhpcy5yZXFPcHRpb25zW2JLZXldID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZ0JvdW5kcyhcbiAgICAgICAgICBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGJvdW5kcy5ib3R0b20sIGJvdW5kcy5yaWdodCksXG4gICAgICAgICAgbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhib3VuZHMudG9wLCBib3VuZHMubGVmdCksXG4gICAgICAgICk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSXMgdGhpcyBoYW5kbGVyIHJlYWR5IHRvIG1ha2UgZmV0Y2ggc3VnZ2VzdGlvbnMgYW5kIHJlc29sdmUgbG9jYXRpb25zP1xuICAgICAqXG4gICAgICogQHJldHVybiB7Ym9vbH1cbiAgICAgKiAgIFRSVUUgaWYgdGhlIHNlcnZpY2UgaXMgcmVhZHkgdG8gbWFrZSBhdXRvY29tcGxldGUgYW5kIHBsYWNlcyBBUEkgY2FsbHMuXG4gICAgICovXG4gICAgaXNSZWFkeSgpIHtcbiAgICAgIHJldHVybiB0aGlzLmFjU2VydmljZSAmJiB0aGlzLnBsYWNlc0FQSTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBBdHRhY2hlZCB0aGUgZGF0YSBjYWxsYmFjayB0byBwb3B1bGF0ZSB2YWx1ZXMgZnJvbSB0aGUgUGxhY2VzIEFQSSBpbnRvXG4gICAgICogaW5wdXQgZWxlbWVudHMuIFRoaXMgYWxsb3dzIHVzIHRvIGZpbGwgaW4gdGhlIHZhbHVlcyB3aGVuIGNhbGxpbmdcbiAgICAgKiB0aGlzLnNlbGVjdCgpLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGtleVxuICAgICAqICAgVGhlIGRhdGEga2V5IGZvciB0aGUgbG9jYXRpb24gcHJvcGVydHkgdGhpcyBpbnB1dCBwb3B1bGF0ZXMuXG4gICAgICogQHBhcmFtIHtIVE1MSW5wdXRFbGVtZW50fSBpbnB1dFxuICAgICAqICAgVGhlIEhUTUwgaW5wdXQgZWxlbWVudCB0byBhdHRhY2hlZCB0aGUgZGF0YSBjYWxsYmFjayB0by5cbiAgICAgKi9cbiAgICBhdHRhY2hJbnB1dChrZXksIGlucHV0KSB7XG4gICAgICBjb25zdCB7IGZpZWxkLCBjYWxsYmFjayB9ID0gYnVpbGRJbnB1dENhbGxiYWNrKGtleSwgaW5wdXQpO1xuXG4gICAgICAvLyBUcmFuc2xhdGVzIHRvIGEgdmFsaWQgcGxhY2VzIHZhbHVlIGFuZCB2YWx1ZSB1cGRhdGUgY2FsbGJhY2suXG4gICAgICBpZiAoZmllbGQgJiYgY2FsbGJhY2spIHtcbiAgICAgICAgdGhpcy5pbnB1dHMucHVzaCh7XG4gICAgICAgICAgZWw6IGlucHV0LFxuICAgICAgICAgIG1ldGhvZDogY2FsbGJhY2ssXG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmICh0aGlzLnJlcUZpZWxkcy5pbmRleE9mKGZpZWxkKSA8IDApIHtcbiAgICAgICAgICB0aGlzLnJlcUZpZWxkcy5wdXNoKGZpZWxkKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENsZWFycyB0aGUgdmFsdWVzIGZyb20gdGhlIG1hbmFnZWQgaW5wdXQgZWxlbWVudHMuXG4gICAgICpcbiAgICAgKiBUaGlzIGVuc3VyZXMgdGhhdCBubyByZXNpZHVhbCBkYXRhIGlzIGtlcHQgaWYgdGhlIHVzZXIgY2hhbmdlcyB0aGUgdGV4dFxuICAgICAqIGVudHJ5IHdpdGhvdXQgc2VsZWN0aW5nIGEgcGxhY2VzIHN1Z2dlc3Rpb24uXG4gICAgICovXG4gICAgY2xlYXJJbnB1dFZhbHVlcygpIHtcbiAgICAgIHRoaXMuaW5wdXRzLmZvckVhY2goKHsgZWwgfSkgPT4ge1xuICAgICAgICBlbC52YWx1ZSA9ICcnO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogVXNlciBoYXMgc2VsZWN0ZWQgYW4gYXV0b2NvbXBsZXRlIHN1Z2dlc3Rpb24sIHVwZGF0ZSBpbnB1dCB2YWx1ZXMgYW5kXG4gICAgICogYXBwbHkgdGhlIHZhbHVlcyBmcm9tIHRoZSBQbGFjZXMgQVBJIGZyb20gdGhlIHNlbGVjdGVkIGl0ZW0uXG4gICAgICpcbiAgICAgKiBAcGFyYW0geyp9IGl0ZW1cbiAgICAgKiAgIFRoZSBhdXRvY29tcGxldGUgc3VnZ2VzdGVkIGl0ZW0gc2VsZWN0ZWQgYnkgdGhlIHVzZXIuXG4gICAgICovXG4gICAgc2VsZWN0KGl0ZW0pIHtcbiAgICAgIGlmICghdGhpcy5yZXFGaWVsZHMubGVuZ3RoKSB7XG4gICAgICAgIC8vIE5vIGZpZWxkcyBhcmUgcmVxdWVzdGVkIGZyb20gcGxhY2VzLCBubyBuZWVkIHRvIG1ha2UgdGhlIGNhbGwuXG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgY29uc3QgcmVxID0ge1xuICAgICAgICBwbGFjZUlkOiBpdGVtLnZhbHVlLmlkLFxuICAgICAgICBzZXNzaW9uVG9rZW46IHRoaXMuc2Vzc2lvblRva2VuLFxuICAgICAgICBmaWVsZHM6IHRoaXMucGxhY2VGaWVsZHMsXG4gICAgICB9O1xuXG4gICAgICAvLyBUb2tlbiBpcyBiZWluZyB1c2VkIGluIGEgcGxhY2UgZGV0YWlscyBjYWxsLCB3ZSBuZWVkIGEgbmV3IHRva2VuXG4gICAgICAvLyBmb3Igc3VjY2Vzc2l2ZSBhdXRvY29tcGxldGUgY2FsbHMuXG4gICAgICBkZWxldGUgdGhpcy5zZXNzaW9uVG9rZW47XG5cbiAgICAgIC8vIEdldCBhZGRpdGlvbmFsIHBsYWNlcyBkYXRhIGlmIGFkZGl0aW9uYWwgZGF0YSBmaWVsZHMgYXJlIGNvbmZpZ3VyZWQuXG4gICAgICB0aGlzLnBsYWNlc0FQSS5nZXREZXRhaWxzKHJlcSwgKHBsYWNlLCBzdGF0dXMpID0+IHtcbiAgICAgICAgaWYgKGdvb2dsZS5tYXBzLnBsYWNlcy5QbGFjZXNTZXJ2aWNlU3RhdHVzLk9LID09PSBzdGF0dXMpIHtcbiAgICAgICAgICB0aGlzLmlucHV0cy5mb3JFYWNoKChpbnB1dCkgPT4gaW5wdXQubWV0aG9kKHBsYWNlKSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEZvcm1hdCB0aGUgcmVzdWx0IGZyb20gdGhlIEFQSSByZXNwb25zZSBpbnRvIGF1dG9jb21wbGV0ZSBzdWdnZXN0IHRleHQuXG4gICAgICpcbiAgICAgKiBAcGFyYW0geyp9IGl0ZW1cbiAgICAgKiAgIFRoZSBkYXRhIGl0ZW0gcmV0dXJuZWQgYnkgdGhlIFBsYWNlcyBzZXJ2aWNlLlxuICAgICAqXG4gICAgICogQHJldHVybiB7c3RyaW5nfVxuICAgICAqICAgVGhlIHRleHQgdG8gZGlzcGxheSBhcyB0aGUgYXV0b2NvbXBsZXRlIHN1Z2dlc3Rpb24uXG4gICAgICovXG4gICAgZm9ybWF0U3VnZ2VzdFRleHQoaXRlbSkge1xuICAgICAgcmV0dXJuIGl0ZW0uZGVzY3JpcHRpb247XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTWFrZSByZXF1ZXN0IHRvIHRoZSBQbGFjZXMgQVBJIHRvIGdldCBsb2NhdGlvbiBzdWdnZXN0aW9ucyBmb3JcbiAgICAgKiBhdXRvY29tcGxldGUgc3VnZ2VzdGlvbnMgZGlzcGxheS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB0ZXh0XG4gICAgICogICBUaGUgdGV4dCB0byBxdWVyeSBmb3IgbWF0Y2hpbmcgcGxhY2VzIHN1Z2dlc3Rpb25zLlxuICAgICAqL1xuICAgIGZldGNoU3VnZ2VzdGlvbnModGV4dCkge1xuICAgICAgLy8gQ3JlYXRlIGFuZCBrZWVwIGEgc2Vzc2lvbiB0b2tlbiBmb3Igc2VhcmNoZXMgdG8gdHJ5IHRvIG9wdGltaXplXG4gICAgICAvLyBiaWxsaW5nLiBUaGlzIHNlc3Npb24gaXMgdmFsaWQgdW50aWwgd2UgbWFrZSBhIFBsYWNlcyBEZXRhaWwgQVBJIGNhbGxcbiAgICAgIC8vIGluIHRoZSBBdXRvY29tcGxldGUuc2VsZWN0KCkgbWV0aG9kIHRvIGdldCBmdWxsIHBsYWNlcyByZXN1bHRzLlxuICAgICAgaWYgKCF0aGlzLnNlc3Npb25Ub2tlbikge1xuICAgICAgICB0aGlzLnNlc3Npb25Ub2tlbiA9IG5ldyBnb29nbGUubWFwcy5wbGFjZXMuQXV0b2NvbXBsZXRlU2Vzc2lvblRva2VuKCk7XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHJlcSA9IHtcbiAgICAgICAgaW5wdXQ6IHRleHQsXG4gICAgICAgIHNlc3Npb25Ub2tlbjogdGhpcy5zZXNzaW9uVG9rZW4sXG4gICAgICAgIC4uLnRoaXMucmVxT3B0aW9ucyxcbiAgICAgIH07XG5cbiAgICAgIHRoaXMuYWNTZXJ2aWNlLmdldFBsYWNlUHJlZGljdGlvbnMocmVxLCAocHJlZGljdGlvbnMsIHN0YXR1cykgPT4ge1xuICAgICAgICB0aGlzLmFjLnBlbmRpbmcgPSBmYWxzZTtcblxuICAgICAgICBsZXQgc3VnZ2VzdCA9IG51bGw7XG4gICAgICAgIGlmIChzdGF0dXMgPT09IGdvb2dsZS5tYXBzLnBsYWNlcy5QbGFjZXNTZXJ2aWNlU3RhdHVzLk9LKSB7XG4gICAgICAgICAgY29uc3QgZGF0YSA9IFtdO1xuXG4gICAgICAgICAgcHJlZGljdGlvbnMuZm9yRWFjaCgoaXRlbSkgPT4ge1xuICAgICAgICAgICAgaWYgKCF0aGlzLmZpbHRlclJlZ0V4IHx8IHRoaXMuZmlsdGVyUmVnRXgudGVzdChpdGVtLmRlc2NyaXB0aW9uKSkge1xuICAgICAgICAgICAgICBkYXRhLnB1c2goe1xuICAgICAgICAgICAgICAgIHRleHQ6IHRoaXMuZm9ybWF0U3VnZ2VzdFRleHQoaXRlbSksXG4gICAgICAgICAgICAgICAgdmFsdWU6IHtcbiAgICAgICAgICAgICAgICAgIHR5cGVzOiBpdGVtLnR5cGVzLFxuICAgICAgICAgICAgICAgICAgaWQ6IGl0ZW0ucGxhY2VfaWQsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG5cbiAgICAgICAgICBzdWdnZXN0ID0geyBsaXN0OiBkYXRhIH07XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAoZ29vZ2xlLm1hcHMucGxhY2VzLlBsYWNlc1NlcnZpY2VTdGF0dXMuWkVST19SRVNVTFRTID09PSBzdGF0dXMpIHtcbiAgICAgICAgICBzdWdnZXN0ID0gbnVsbDtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIExldCB0aGUgYXV0b2NvbXBsZXRlIGVsZW1lbnRzIGtub3cgdGhhdCB3ZSBhcmUgZG9uZSBmZXRjaGluZyByZXN1bHRzLlxuICAgICAgICB0aGlzLmFjLnNldEZldGNoZWRTdWdnZXN0aW9ucyhzdWdnZXN0KTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENsZWFyIGFueSByZWZlcmVuY2VzIGFuZCBnZXQgcmVhZHkgdG8gZGV0YWNoIHRoaXMgYXV0b2NvbXBsZXRlIGhhbmRsZXIuXG4gICAgICovXG4gICAgZGVzdHJveSgpIHtcbiAgICAgIGRlbGV0ZSB0aGlzLnNlc3Npb25Ub2tlbjtcbiAgICB9XG4gIH07XG59KShEcnVwYWwsIE1hcGtpdCk7XG4iXSwibWFwcGluZ3MiOiI7O0FBQUEsQ0FBQyxDQUFDO0VBQUVBLFFBQVEsRUFBRUM7QUFBRyxDQUFDLEVBQUVDLE1BQU0sS0FBSztFQUM3QjtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNDLGtCQUFrQixDQUFDQyxHQUFHLEVBQUVDLEtBQUssRUFBRTtJQUN0QyxRQUFRRCxHQUFHO01BQ1QsS0FBSyxTQUFTO01BQ2QsS0FBSyxhQUFhO01BQ2xCLEtBQUssNkJBQTZCO01BQ2xDLEtBQUssNkJBQTZCO01BQ2xDLEtBQUssVUFBVTtNQUNmLEtBQUssZ0JBQWdCO1FBQ25CLE9BQU87VUFDTEUsS0FBSyxFQUFFLG1CQUFtQjtVQUMxQkMsUUFBUSxFQUFHQyxLQUFLLElBQUs7WUFDbkIsSUFBSUMsR0FBRyxHQUFHLEVBQUU7WUFDWkQsS0FBSyxDQUFDRSxrQkFBa0IsQ0FBQ0MsT0FBTyxDQUFDLENBQUM7Y0FBRUMsVUFBVSxFQUFFQyxJQUFJO2NBQUVDO1lBQU0sQ0FBQyxLQUFLO2NBQ2hFLElBQUlBLEtBQUssQ0FBQ0MsT0FBTyxDQUFDWCxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUVLLEdBQUcsR0FBR0ksSUFBSTtZQUN6QyxDQUFDLENBQUM7WUFFRlIsS0FBSyxDQUFDVyxLQUFLLEdBQUdQLEdBQUc7VUFDbkI7UUFDRixDQUFDO01BRUgsS0FBSyxRQUFRO1FBQ1gsT0FBTztVQUNMSCxLQUFLLEVBQUUsbUJBQW1CO1VBQzFCQyxRQUFRLEVBQUdDLEtBQUssSUFBSztZQUNuQixNQUFNUyxHQUFHLEdBQUcsQ0FBQ1QsS0FBSyxDQUFDVSxRQUFRLElBQUksQ0FBQyxDQUFDLEVBQUVDLFFBQVE7WUFDM0NkLEtBQUssQ0FBQ1csS0FBSyxHQUFHQyxHQUFHLEdBQUksR0FBRUEsR0FBRyxDQUFDRyxHQUFHLEVBQUUsQ0FBQ0MsT0FBTyxDQUFDLENBQUMsQ0FBRSxJQUFHSixHQUFHLENBQUNLLEdBQUcsRUFBRSxDQUFDRCxPQUFPLENBQUMsQ0FBQyxDQUFFLEVBQUMsR0FBRyxFQUFFO1VBQzVFO1FBQ0YsQ0FBQztNQUVIO1FBQ0UsT0FBTyxDQUFDLENBQUM7SUFBQztFQUVoQjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFbkIsTUFBTSxDQUFDcUIsRUFBRSxDQUFDQyxVQUFVLEdBQUcsTUFBTTtJQUMzQjtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSUMsV0FBVyxDQUFDRixFQUFFLEVBQUU7TUFDZCxJQUFJLENBQUNHLFFBQVEsR0FBRyxNQUFNO01BQ3RCLElBQUksQ0FBQ0gsRUFBRSxHQUFHQSxFQUFFOztNQUVaO01BQ0EsSUFBSSxDQUFDSSxTQUFTLEdBQUcsRUFBRTtNQUNuQixJQUFJLENBQUNDLE1BQU0sR0FBRyxFQUFFO0lBQ2xCOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJQyxJQUFJLEdBQUc7TUFDTCxNQUFNQyxXQUFXLEdBQUcsSUFBSTdCLEVBQUUsQ0FBQzhCLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDUixFQUFFLENBQUNTLFdBQVcsQ0FBQztNQUNyRUYsV0FBVyxDQUFDRyxRQUFRLENBQUMsQ0FBQyxjQUFjLEVBQUUsc0JBQXNCLENBQUMsQ0FBQzs7TUFFOUQ7TUFDQSxJQUFJLENBQUNDLFNBQVMsR0FBRyxJQUFJQyxNQUFNLENBQUNDLElBQUksQ0FBQ0MsTUFBTSxDQUFDQyxtQkFBbUIsRUFBRTtNQUM3RCxJQUFJLENBQUNDLFNBQVMsR0FBRyxJQUFJSixNQUFNLENBQUNDLElBQUksQ0FBQ0MsTUFBTSxDQUFDRyxhQUFhLENBQUNWLFdBQVcsQ0FBQ1csRUFBRSxDQUFDO01BRXJFLElBQUksQ0FBQ0MsVUFBVSxHQUFHLENBQUUsQ0FBQztNQUVyQixNQUFNO1FBQ0pDLFNBQVMsRUFBRUMsSUFBSTtRQUNmQyxTQUFTO1FBQ1RDLE1BQU07UUFDTkM7TUFDRixDQUFDLEdBQUcsSUFBSSxDQUFDeEIsRUFBRSxDQUFDeUIsTUFBTTtNQUVsQixJQUFJSixJQUFJLEtBQUssU0FBUyxFQUFFO1FBQ3RCLElBQUksQ0FBQ0YsVUFBVSxDQUFDNUIsS0FBSyxHQUFHLENBQUMsU0FBUyxDQUFDO01BQ3JDLENBQUMsTUFDSSxJQUFJOEIsSUFBSSxLQUFLLE1BQU0sRUFBRTtRQUN4QixJQUFJLENBQUNGLFVBQVUsQ0FBQzVCLEtBQUssR0FBRyxDQUFDLFVBQVUsQ0FBQztNQUN0QyxDQUFDLE1BQ0ksSUFBSThCLElBQUksS0FBSyxRQUFRLEVBQUU7UUFDMUIsSUFBSSxDQUFDRixVQUFVLENBQUM1QixLQUFLLEdBQUcsQ0FBQyxXQUFXLENBQUM7TUFDdkM7TUFFQSxJQUFJaUMsT0FBTyxFQUFFO1FBQ1gsSUFBSSxDQUFDRSxXQUFXLEdBQUcsSUFBSUMsTUFBTSxDQUFDSCxPQUFPLEVBQUUsR0FBRyxDQUFDO01BQzdDO01BRUEsSUFBSUYsU0FBUyxFQUFFO1FBQ2IsSUFBSSxDQUFDSCxVQUFVLENBQUNTLHFCQUFxQixHQUFHO1VBQUVDLE9BQU8sRUFBRVA7UUFBVSxDQUFDO01BQ2hFO01BRUEsSUFBSUMsTUFBTSxJQUFJQSxNQUFNLENBQUNPLE1BQU0sSUFBSVAsTUFBTSxDQUFDUSxHQUFHLEVBQUU7UUFDekMsTUFBTUMsSUFBSSxHQUFHVCxNQUFNLENBQUNGLElBQUksS0FBSyxhQUFhLEdBQUcscUJBQXFCLEdBQUcsY0FBYztRQUNuRixJQUFJLENBQUNGLFVBQVUsQ0FBQ2EsSUFBSSxDQUFDLEdBQUcsSUFBSXBCLE1BQU0sQ0FBQ0MsSUFBSSxDQUFDb0IsWUFBWSxDQUNsRCxJQUFJckIsTUFBTSxDQUFDQyxJQUFJLENBQUNxQixNQUFNLENBQUNYLE1BQU0sQ0FBQ08sTUFBTSxFQUFFUCxNQUFNLENBQUNZLEtBQUssQ0FBQyxFQUNuRCxJQUFJdkIsTUFBTSxDQUFDQyxJQUFJLENBQUNxQixNQUFNLENBQUNYLE1BQU0sQ0FBQ1EsR0FBRyxFQUFFUixNQUFNLENBQUNhLElBQUksQ0FBQyxDQUNoRDtNQUNIO0lBQ0Y7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lDLE9BQU8sR0FBRztNQUNSLE9BQU8sSUFBSSxDQUFDMUIsU0FBUyxJQUFJLElBQUksQ0FBQ0ssU0FBUztJQUN6Qzs7SUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJc0IsV0FBVyxDQUFDekQsR0FBRyxFQUFFQyxLQUFLLEVBQUU7TUFDdEIsTUFBTTtRQUFFQyxLQUFLO1FBQUVDO01BQVMsQ0FBQyxHQUFHSixrQkFBa0IsQ0FBQ0MsR0FBRyxFQUFFQyxLQUFLLENBQUM7O01BRTFEO01BQ0EsSUFBSUMsS0FBSyxJQUFJQyxRQUFRLEVBQUU7UUFDckIsSUFBSSxDQUFDcUIsTUFBTSxDQUFDa0MsSUFBSSxDQUFDO1VBQ2ZyQixFQUFFLEVBQUVwQyxLQUFLO1VBQ1QwRCxNQUFNLEVBQUV4RDtRQUNWLENBQUMsQ0FBQztRQUVGLElBQUksSUFBSSxDQUFDb0IsU0FBUyxDQUFDWixPQUFPLENBQUNULEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRTtVQUNyQyxJQUFJLENBQUNxQixTQUFTLENBQUNtQyxJQUFJLENBQUN4RCxLQUFLLENBQUM7UUFDNUI7TUFDRjtJQUNGOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJMEQsZ0JBQWdCLEdBQUc7TUFDakIsSUFBSSxDQUFDcEMsTUFBTSxDQUFDakIsT0FBTyxDQUFDLENBQUM7UUFBRThCO01BQUcsQ0FBQyxLQUFLO1FBQzlCQSxFQUFFLENBQUN6QixLQUFLLEdBQUcsRUFBRTtNQUNmLENBQUMsQ0FBQztJQUNKOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lpRCxNQUFNLENBQUNDLElBQUksRUFBRTtNQUNYLElBQUksQ0FBQyxJQUFJLENBQUN2QyxTQUFTLENBQUN3QyxNQUFNLEVBQUU7UUFDMUI7UUFDQTtNQUNGO01BRUEsTUFBTUMsR0FBRyxHQUFHO1FBQ1ZDLE9BQU8sRUFBRUgsSUFBSSxDQUFDbEQsS0FBSyxDQUFDc0QsRUFBRTtRQUN0QkMsWUFBWSxFQUFFLElBQUksQ0FBQ0EsWUFBWTtRQUMvQkMsTUFBTSxFQUFFLElBQUksQ0FBQ0M7TUFDZixDQUFDOztNQUVEO01BQ0E7TUFDQSxPQUFPLElBQUksQ0FBQ0YsWUFBWTs7TUFFeEI7TUFDQSxJQUFJLENBQUNoQyxTQUFTLENBQUNtQyxVQUFVLENBQUNOLEdBQUcsRUFBRSxDQUFDNUQsS0FBSyxFQUFFbUUsTUFBTSxLQUFLO1FBQ2hELElBQUl4QyxNQUFNLENBQUNDLElBQUksQ0FBQ0MsTUFBTSxDQUFDdUMsbUJBQW1CLENBQUNDLEVBQUUsS0FBS0YsTUFBTSxFQUFFO1VBQ3hELElBQUksQ0FBQy9DLE1BQU0sQ0FBQ2pCLE9BQU8sQ0FBRU4sS0FBSyxJQUFLQSxLQUFLLENBQUMwRCxNQUFNLENBQUN2RCxLQUFLLENBQUMsQ0FBQztRQUNyRDtNQUNGLENBQUMsQ0FBQztJQUNKOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJc0UsaUJBQWlCLENBQUNaLElBQUksRUFBRTtNQUN0QixPQUFPQSxJQUFJLENBQUNhLFdBQVc7SUFDekI7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSUMsZ0JBQWdCLENBQUNDLElBQUksRUFBRTtNQUNyQjtNQUNBO01BQ0E7TUFDQSxJQUFJLENBQUMsSUFBSSxDQUFDVixZQUFZLEVBQUU7UUFDdEIsSUFBSSxDQUFDQSxZQUFZLEdBQUcsSUFBSXBDLE1BQU0sQ0FBQ0MsSUFBSSxDQUFDQyxNQUFNLENBQUM2Qyx3QkFBd0IsRUFBRTtNQUN2RTtNQUVBLE1BQU1kLEdBQUcsR0FBRztRQUNWL0QsS0FBSyxFQUFFNEUsSUFBSTtRQUNYVixZQUFZLEVBQUUsSUFBSSxDQUFDQSxZQUFZO1FBQy9CLEdBQUcsSUFBSSxDQUFDN0I7TUFDVixDQUFDO01BRUQsSUFBSSxDQUFDUixTQUFTLENBQUNpRCxtQkFBbUIsQ0FBQ2YsR0FBRyxFQUFFLENBQUNnQixXQUFXLEVBQUVULE1BQU0sS0FBSztRQUMvRCxJQUFJLENBQUNwRCxFQUFFLENBQUM4RCxPQUFPLEdBQUcsS0FBSztRQUV2QixJQUFJQyxPQUFPLEdBQUcsSUFBSTtRQUNsQixJQUFJWCxNQUFNLEtBQUt4QyxNQUFNLENBQUNDLElBQUksQ0FBQ0MsTUFBTSxDQUFDdUMsbUJBQW1CLENBQUNDLEVBQUUsRUFBRTtVQUN4RCxNQUFNVSxJQUFJLEdBQUcsRUFBRTtVQUVmSCxXQUFXLENBQUN6RSxPQUFPLENBQUV1RCxJQUFJLElBQUs7WUFDNUIsSUFBSSxDQUFDLElBQUksQ0FBQ2pCLFdBQVcsSUFBSSxJQUFJLENBQUNBLFdBQVcsQ0FBQ3VDLElBQUksQ0FBQ3RCLElBQUksQ0FBQ2EsV0FBVyxDQUFDLEVBQUU7Y0FDaEVRLElBQUksQ0FBQ3pCLElBQUksQ0FBQztnQkFDUm1CLElBQUksRUFBRSxJQUFJLENBQUNILGlCQUFpQixDQUFDWixJQUFJLENBQUM7Z0JBQ2xDbEQsS0FBSyxFQUFFO2tCQUNMRixLQUFLLEVBQUVvRCxJQUFJLENBQUNwRCxLQUFLO2tCQUNqQndELEVBQUUsRUFBRUosSUFBSSxDQUFDdUI7Z0JBQ1g7Y0FDRixDQUFDLENBQUM7WUFDSjtVQUNGLENBQUMsQ0FBQztVQUVGSCxPQUFPLEdBQUc7WUFBRUksSUFBSSxFQUFFSDtVQUFLLENBQUM7UUFDMUIsQ0FBQyxNQUNJLElBQUlwRCxNQUFNLENBQUNDLElBQUksQ0FBQ0MsTUFBTSxDQUFDdUMsbUJBQW1CLENBQUNlLFlBQVksS0FBS2hCLE1BQU0sRUFBRTtVQUN2RVcsT0FBTyxHQUFHLElBQUk7UUFDaEI7O1FBRUE7UUFDQSxJQUFJLENBQUMvRCxFQUFFLENBQUNxRSxxQkFBcUIsQ0FBQ04sT0FBTyxDQUFDO01BQ3hDLENBQUMsQ0FBQztJQUNKOztJQUVBO0FBQ0o7QUFDQTtJQUNJTyxPQUFPLEdBQUc7TUFDUixPQUFPLElBQUksQ0FBQ3RCLFlBQVk7SUFDMUI7RUFDRixDQUFDO0FBQ0gsQ0FBQyxFQUFFdUIsTUFBTSxFQUFFNUYsTUFBTSxDQUFDIn0=
