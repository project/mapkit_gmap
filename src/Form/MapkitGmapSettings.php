<?php

namespace Drupal\mapkit_gmap\Form;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for setting Google maps provider settings.
 */
class MapkitGmapSettings extends ConfigFormBase {

  /**
   * The library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * Link generator service.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $link;

  /**
   * Creates a new form for configuring Mapkit settings.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery
   *   The library discovery service.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   Drupal service for resolving and generating links from URI or routes.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LibraryDiscoveryInterface $library_discovery, LinkGeneratorInterface $link_generator) {
    parent::__construct($config_factory);

    $this->libraryDiscovery = $library_discovery;
    $this->link = $link_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('library.discovery'),
      $container->get('link_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mapkit_gmap.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mapkit_gmap_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('mapkit_gmap.settings');
    $apiUrl = Url::fromUri('https://developers.google.com/maps/documentation/javascript/get-api-key', [
      'absolute' => TRUE,
      'attributes' => ['target' => '_blank'],
    ]);

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Maps API key (@link)', [
        '@link' => $this->link->generate($this->t('Get a key'), $apiUrl),
      ]),
      '#default_value' => $config->get('api_key'),
    ];

    $form['region'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Region code (optional)'),
      '#default_value' => $config->get('region'),
    ];

    $form['libraries'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select Google libraries to include in the Javascript APIs.'),
      '#options' => [
        'drawing' => $this->t('Drawing: polygons, circles and markers on the map'),
        'geometry' => $this->t('Geometry: calculate scalar values from geo-primatives'),
        'places' => $this->t('Places: establishments, points of interest or geographic locations'),
        'visualization' => $this->t('Visualization: heatmaps, and data representation'),
      ],
      '#default_value' => $config->get('libraries') ?: [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();

    // Apply our library key values from the form.
    $this->config('mapkit_gmap.settings')->setData([
      'api_key' => $values['api_key'],
      'region' => $values['region'],
      'libraries' => array_filter($values['libraries']),
    ])->save();

    $this->libraryDiscovery->clearCachedDefinitions();
  }

}
