<?php

namespace Drupal\mapkit_gmap\Plugin\Mapkit\MapProvider;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\mapkit\Plugin\MapProviderInterface;

/**
 * A map plugin for configuring and generating an interactive Google Map.
 *
 * @MapkitMapProvider(
 *   id = "gmap",
 *   label = @Translation("Google Maps"),
 *   config_route = {
 *      "route_name" = "mapkit_gmap.settings",
 *   }
 * )
 */
class GMap extends PluginBase implements MapProviderInterface, PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'fitBounds' => TRUE,
      'center' => [
        'lat' => 0.0,
        'lng' => 0.0,
      ],
      'zoom' => 14,
      'defaultLayer' => 'roadmap',
      'layers' => [
        'roadmap',
        'satellite',
        'hybrid',
        'terrain',
      ],
      'markerSpiderfy' => [
        'enabled' => FALSE,
        'keepSpiderfied' => FALSE,
        'markersWontMove' => FALSE,
        'markersWontHide' => FALSE,
        'basicFormatEvents' => TRUE,
        'nearbyDistance' => 20,
        'circleFootSeparation' => 23,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(): array {
    $config = $this->getConfiguration();
    $libraries = ['mapkit/gmap'];

    if (!empty($config['markerSpiderfy']['enabled'])) {
      $libraries[] = 'mapkit/gmap-marker-spiderfy';
    }

    return $libraries;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultMarkerLibraries(): array {
    return [
      'mapkit_gmap/gmap-marker',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getJsSettings(): array {
    $defaults = $this->defaultConfiguration();

    $config = $this->getConfiguration() + $defaults;
    $config['layers'] = array_values($config['layers']);

    // Was spiderify functionality enabled? If not clear out these settings
    // from the JS configurations.
    if (empty($config['markerSpiderfy']['enabled'])) {
      $config['markerSpiderfy'] = FALSE;
    }
    else {
      unset($config['markerSpiderfy']['enabled']);
    }

    return $config;
  }

  /**
   * Fetch a list of possible Google Maps display layer options.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]|string[]
   *   Available Google Maps layer options, keyed by layer identifier.
   */
  protected function getLayerOptions() {
    return [
      'roadmap' => $this->t('Roadmap'),
      'satellite' => $this->t('Satellite'),
      'hybrid' => $this->t('Hybrid'),
      'terrain' => $this->t('Terrain'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfiguration() + $this->defaultConfiguration();
    $layerOpts = $this->getLayerOptions();

    $form['fitBounds'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fit map to marker bounds'),
      '#default_value' => $config['fitBounds'],
      '#description' => $this->t('Scales and centers the map based on the markers placed on the map. Overrides default zoom and default center when there is more than one marker visible on the map.'),
    ];

    $form['center'] = [
      '#type' => 'latlng_point',
      '#title' => $this->t('Default center'),
      '#default_value' => $config['center'],
      '#required' => TRUE,
      '#attributes' => [
        'class' => ['container-inline'],
      ],
    ];

    $form['zoom'] = [
      '#type' => 'number',
      '#title' => $this->t('Default zoom'),
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 18,
      '#default_value' => $config['zoom'],
    ];

    $form['defaultLayer'] = [
      '#type' => 'select',
      '#title' => $this->t('Initial display layer'),
      '#options' => $layerOpts,
      '#default_value' => $config['defaultLayer'],
    ];

    $form['layers'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled layers'),
      '#options' => $layerOpts,
      '#default_value' => $config['layers'],
    ];

    $form['markerSpiderfy'] = [
      '#type' => 'details',
      '#title' => $this->t('Marker Spiderfy'),
      '#tree' => TRUE,
      '#open' => !empty($config['markerSpiderfy']['enabled']),

      'enabled' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable marker spiderfy'),
        '#default_value' => $config['markerSpiderfy']['enabled'],
      ],
      'keepSpiderfied' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Keep markers spiderfied after clicking'),
        '#default_value' => $config['markerSpiderfy']['keepSpiderfied'],
      ],
      'basicFormatEvents' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable spiderfy advanced marker formatting events'),
        '#default_value' => empty($config['markerSpiderfy']['basicFormatEvents']),
      ],
      'markersWontMove' => [
        '#type' => 'checkbox',
        '#title' => $this->t("Markers won't move (optimization)"),
        '#default_value' => $config['markerSpiderfy']['markersWontMove'],
      ],
      'markersWontHide' => [
        '#type' => 'checkbox',
        '#title' => $this->t("Markers won't hide (optimization)"),
        '#default_value' => $config['markerSpiderfy']['markersWontHide'],
      ],
      'nearbyDistance' => [
        '#type' => 'number',
        '#title' => $this->t('Distance max between markers to apply spiderfy to'),
        '#step' => 1,
        '#min' => 6,
        '#max' => 30,
        '#default_value' => $config['markerSpiderfy']['nearbyDistance'],
      ],
      'circleFootSeparation' => [
        '#type' => 'number',
        '#title' => $this->t('Distance in pixels to separate spiderfied markers'),
        '#step' => 1,
        '#min' => 11,
        '#max' => 100,
        '#default_value' => $config['markerSpiderfy']['circleFootSeparation'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // No validation beyond form element validation needed.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $defaults = $this->defaultConfiguration();

    // Ensure that the values are transformed to the correct types.
    $values = $form_state->getValues() + $defaults;
    $values['fitBounds'] = (bool) $values['fitBounds'];
    $values['layers'] = array_filter($values['layers']);
    $values['zoom'] = intval($values['zoom']);

    // Invert the value, as the settings form is inverted to make the option
    // less confusing and easier to default.
    $values['markerSpiderfy']['basicFormatEvents'] = !$values['markerSpiderfy']['basicFormatEvents'];
    $values['markerSpiderfy']['keepSpiderfied'] = (bool) $values['markerSpiderfy']['keepSpiderfied'];
    $values['markerSpiderfy']['nearbyDistance'] = intval($values['markerSpiderfy']['nearbyDistance']);
    $values['markerSpiderfy']['circleFootSeparation'] = intval($values['markerSpiderfy']['circleFootSeparation']);

    $this->configuration = array_intersect_key($values, $defaults);
  }

}
