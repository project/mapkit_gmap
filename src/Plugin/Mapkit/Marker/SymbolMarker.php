<?php

namespace Drupal\mapkit_gmap\Plugin\Mapkit\Marker;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\mapkit\Plugin\Mapkit\Marker\DefaultMarker;
use Drupal\mapkit\Plugin\MapProviderInterface;
use Drupal\mapkit\Plugin\MarkerPluginInterface;

/**
 * The Symbols Mapkit marker set.
 *
 * @MapkitMarker(
 *   id = "symbol",
 *   label = @Translation("Symbol (SVG path)"),
 *   map_types = {
 *     "gmap",
 *   }
 * )
 */
class SymbolMarker extends DefaultMarker implements MarkerPluginInterface, PluginFormInterface {

  /**
   * The current starting index to start the markers at.
   *
   * @var int
   */
  protected $startIndex = 1;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'symbol' => [
        'size' => ['width' => 22, 'height' => 22],
        'path' => 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
        'fillColor' => 'ff0000',
        'fillOpacity' => 1,
        'strokeColor' => 'ffffff',
        'strokeWeight' => 1,
        'scale' => 2,
        'anchor' => ['x' => 10, 'y' => 22],
        'labelOrigin' => ['x' => 10, 'y' => 10],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getJsSettings(MapProviderInterface $map) {
    $jsSettings = parent::getJsSettings($map);

    $jsSettings['symbol']['fillColor'] = '#' . $jsSettings['symbol']['fillColor'];
    $jsSettings['symbol']['strokeColor'] = '#' . $jsSettings['symbol']['strokeColor'];

    return $jsSettings;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $config = $this->getConfiguration() + $this->defaultConfiguration();

    $form['symbol'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Symbol Settings'),
      '#tree' => TRUE,

      'size' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Marker dimensions'),

        'width' => [
          '#type' => 'number',
          '#title' => $this->t('Width'),
          '#min' => 5,
          '#max' => 22,
          '#default_value' => $config['symbol']['size']['width'],
        ],
        'height' => [
          '#type' => 'number',
          '#title' => $this->t('Height'),
          '#min' => 5,
          '#max' => 22,
          '#default_value' => $config['symbol']['size']['height'],
        ],
      ],
      'path' => [
        '#type' => 'textarea',
        '#title' => $this->t('Path (in SVG format)'),
        '#required' => TRUE,
        '#rows' => 3,
        '#default_value' => $config['symbol']['path'],
        '#description' => $this->t('Paths are specified in SVG path format <a href="@url">documented here</a>, and can easily be taken from an SVG image. Paths should be specified to fit within of 22 x 22 pixels in size, and can be scaled.', [
          '@url' => 'https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths',
        ]),
      ],
      'fillColor' => [
        '#type' => 'textfield',
        '#title' => $this->t('Path fill color'),
        '#size' => 6,
        '#field_prefix' => '#',
        '#pattern' => '#?[a-fA-F0-9]{3}|[a-fA-F0-9]{6}',
        '#default_value' => $config['symbol']['fillColor'],
      ],
      'fillOpacity' => [
        '#type' => 'number',
        '#title' => $this->t('Opacity (0-1)'),
        '#min' => 0,
        '#max' => 1,
        '#step' => 0.01,
        '#default_value' => $config['symbol']['fillOpacity'],
      ],
      'strokeColor' => [
        '#type' => 'textfield',
        '#title' => $this->t('Stroke color'),
        '#size' => 6,
        '#field_prefix' => '#',
        '#pattern' => '#?[a-fA-F0-9]{3}|[a-fA-F0-9]{6}',
        '#default_value' => $config['symbol']['strokeColor'],
      ],
      'strokeWeight' => [
        '#type' => 'number',
        '#title' => $this->t('Stroke width'),
        '#min' => 1,
        '#max' => 10,
        '#default_value' => $config['symbol']['strokeWeight'],
      ],
      'scale' => [
        '#type' => 'number',
        '#title' => $this->t('Scale (multiplier)'),
        '#min' => 0.25,
        '#max' => 5,
        '#step' => 0.25,
        '#default_value' => $config['symbol']['scale'],
      ],
      'anchor' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Marker anchor point'),
        '#attributes' => [
          'class' => ['container-inline'],
        ],

        'x' => [
          '#type' => 'number',
          '#title' => 'x',
          '#step' => 1,
          '#size' => 3,
          '#default_value' => $config['symbol']['anchor']['x'],
        ],
        'y' => [
          '#type' => 'number',
          '#title' => 'y',
          '#step' => 1,
          '#size' => 3,
          '#default_value' => $config['symbol']['anchor']['y'],
        ],
      ],
      'labelOrigin' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Marker anchor point'),
        '#attributes' => [
          'class' => ['container-inline'],
        ],

        'x' => [
          '#type' => 'number',
          '#title' => 'x',
          '#step' => 1,
          '#size' => 3,
          '#default_value' => $config['symbol']['labelOrigin']['x'],
        ],
        'y' => [
          '#type' => 'number',
          '#title' => 'y',
          '#step' => 1,
          '#size' => 3,
          '#default_value' => $config['symbol']['labelOrigin']['y'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $svgPath = $form_state->getValue(['symbol', 'path']);

    // Number of arguments expected for each of the following SVG path commands.
    // Commands are indexed by the lower case version, though an uppercase
    // version for almost all of these are valid (but have same # arguments).
    $pathCmd = [
      'm' => 2,
      'l' => 2,
      'h' => 1,
      'v' => 1,
      'c' => 6,
      's' => 4,
      'q' => 4,
      't' => 2,
      'a' => 7,
      'z' => 0,
    ];

    // Check the provided SVG path to ensure that it looks like a proper
    // path definition. Check for known SVG commands and their expected number
    // of arguments.
    $offset = 0;
    while (preg_match('/\s*([mlhvcsqtaz])([ \-\,\d.]*)/i', $svgPath, $matches, PREG_OFFSET_CAPTURE, $offset)) {
      if ($matches[0][1] !== $offset) {
        $form_state->setError($form['symbol']['path'], $this->t('Invalid commands or information in SVG path at character offset @offset', [
          '@offset' => $offset,
        ]));
        break;
      }

      $cmd = strtolower($matches[1][0]);
      if (!isset($pathCmd[$cmd])) {
        $form_state->setError($form['symbol']['path'], $this->t('Invalid command "%cmd" in path at character offset @offset', [
          '%cmd' => $matches[1][0],
          '@offset' => $matches[1][1],
        ]));
        break;
      }

      $args = preg_split('/(\s*,\s*|\s+)/', $matches[2][0], -1, PREG_SPLIT_NO_EMPTY);
      if ($pathCmd[$cmd] !== count($args)) {
        $form_state->setError($form['symbol']['path'], $this->t('Wrong arguments for command "%cmd", expected %expected but got %count at offset @offset.', [
          '%cmd' => $matches[1][0],
          '%expected' => $pathCmd[$cmd],
          '%count' => count($args),
          '@offset' => $matches[1][1],
        ]));
        break;
      }

      // Move to the start position of the next path command.
      $offset = $matches[0][1] + strlen($matches[0][0]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $defaults = $this->defaultConfiguration();
    $this->configuration['symbol'] = [];
    $symConfig = &$this->configuration['symbol'];

    // Move the symbol definitions, with defaults applied.
    $symbolSettings = $form_state->getValue('symbol') + $defaults['symbol'];
    $symConfig['path'] = $symbolSettings['path'];
    $symConfig['fillColor'] = trim($symbolSettings['fillColor'], ' #');
    $symConfig['fillOpacity'] = floatval($symbolSettings['fillOpacity']);
    $symConfig['strokeColor'] = trim($symbolSettings['strokeColor'], ' #');
    $symConfig['strokeWeight'] = intval($symbolSettings['strokeWeight']);
    $symConfig['scale'] = floatval($symbolSettings['scale']);
    $symConfig['size'] = [
      'width' => intval($symbolSettings['size']['width']),
      'height' => intval($symbolSettings['size']['height']),
    ];
    $symConfig['anchor'] = [
      'x' => intval($symbolSettings['anchor']['x']),
      'y' => intval($symbolSettings['anchor']['y']),
    ];
    $symConfig['labelOrigin'] = [
      'x' => intval($symbolSettings['labelOrigin']['x']),
      'y' => intval($symbolSettings['labelOrigin']['y']),
    ];
  }

}
